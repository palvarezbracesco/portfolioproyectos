#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QtCore>
#include <QMessageBox>
#include <iostream>

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect((ui->pushButtonCargar),SIGNAL(void setSecundaria(bool)),this,SLOT(void secundaria(bool)));
    ui->spinBoxEdad->setRange(17,65);
    ui->spinBoxEdad->setValue(17);
    ui->progressBarCarrera->setValue(0);
    ui->tabWidget->setCurrentIndex(0);
    ui->tabSecundario->setDisabled(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::secundaria(bool valor)
{
    if(valor){
        ui->tabSecundario->setEnabled(true);

    }
}
void MainWindow::on_pushButtonCargar_clicked()
{
    bool valorSenial = false;
    if(ui->lineEditNombre->isModified() == false && ui->lineEditLegajo->isModified() == false){
        QMessageBox::warning(this,"ERROR","Debe ingresar nombre y edad");
    }else if(ui->lineEditNombre->text() == "" && ui->lineEditLegajo->text() == ""){
        QMessageBox::warning(this,"ERROR","Debe ingresar nombre y edad");
    }else{
        QString nombre = ui->lineEditNombre->text();
        QString legajo = ui->lineEditLegajo->text();
        QString edad= ui->spinBoxEdad->text();
        bool deudaFinales = ui->checkBoxDeudaFinales->isChecked();
        bool  materias = ui->checkBoxCantidadMaterias->isChecked();
        bool trabajo = ui->radioButtonSi->isChecked();

        QString data;
        data = QString::fromStdString("Nombre : ");
        data += nombre;
        data += "\n Legajo : ";
        data += legajo;
        data += "\n Edad : ";
        data += edad;
        data += "\n Adeuda Finales : ";
        if(deudaFinales){
            data += " Si ";
        }else{
            data += " No ";
        }
        data += "\n Cursa 3 o mas materias: ";
        if(materias){
            data += " Si ";
        }else{
            data += " No ";
        }
        data += "\n Trabaja: ";
        if(trabajo){
            data += "Si ";
        }else{
            data += " No ";
        }
        QMessageBox::information(this,"Datos del usuario",data);
        valorSenial = true;
    }
    emit setSecundaria(valorSenial);

}
