#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
signals:
    void setSecundaria(bool);

private slots:
    void on_pushButtonCargar_clicked();
    void secundaria(bool);

private:
    Ui::MainWindow *ui;

};

#endif // MAINWINDOW_H
