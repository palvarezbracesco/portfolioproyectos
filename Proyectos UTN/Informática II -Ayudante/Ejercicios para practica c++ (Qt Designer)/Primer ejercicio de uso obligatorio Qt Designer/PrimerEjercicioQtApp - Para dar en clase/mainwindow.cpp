#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>
#include <QtCore>
#include <QMessageBox>
#include <iostream>

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->spinBoxEdad->setRange(18,80);
    ui->spinBoxEdad->setValue(18);
    ui->radioButtonSi->setChecked(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonCargar_clicked()
{
    if(ui->lineEditNombre->text() == "" && ui->lineEditLegajo->text() == ""){
       //QMessageBox * mensaje = new QMessageBox();
       //mensaje->setText("ERROR en el ingreso de datos");
       //mensaje->show();
       QMessageBox::warning(this,"ERROR en el ingreso de datos", "Debe ingresar nombre y edad");
    }else{
        QString nombre = ui->lineEditNombre->text();
        QString legajo = ui->lineEditLegajo->text();
        bool finalesAdeudados = ui->checkBoxDeudaFinales->checkState();
        bool materias = ui->checkBoxCantidadMaterias->checkState();
        QString edad = ui->spinBoxEdad->text();
        bool trabaja = ui->radioButtonSi->isChecked();

        QString datos = "Nombre :" + nombre;
        datos += "\n";
        datos += "Legajo : " + legajo;
        datos += "\n";
        datos += "Edad : " + edad;
        datos += "\n";
        datos += "Adeuda finales: ";
        if(finalesAdeudados == true){
            datos += "si";
        }
        else{
            datos += "no";
        }
        datos += "\n";
        datos += "Trabaja: ";
        if(trabaja == true){
            datos += "si";
        }
        else{
            datos += "no";
        }
        datos += "\n";
        datos += "Cursa 3 o mas materias: ";
        if(materias == true){
            datos += "si";
        }
        else{
            datos += "no";
        }
        QMessageBox::information(this,"Datos ingresados",datos);


    }

}
