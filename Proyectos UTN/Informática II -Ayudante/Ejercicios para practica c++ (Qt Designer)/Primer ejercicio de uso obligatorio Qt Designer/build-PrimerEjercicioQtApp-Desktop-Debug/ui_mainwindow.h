/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *labelNombre;
    QLineEdit *lineEditNombre;
    QHBoxLayout *horizontalLayout_3;
    QLabel *Legajo;
    QLineEdit *lineEditLegajo;
    QHBoxLayout *horizontalLayout_2;
    QLabel *labelEdad;
    QSpinBox *spinBoxEdad;
    QHBoxLayout *horizontalLayout_4;
    QCheckBox *checkBoxDeudaFinales;
    QCheckBox *checkBoxCantidadMaterias;
    QHBoxLayout *horizontalLayout_5;
    QLabel *labelSiTrabaja;
    QRadioButton *radioButtonSi;
    QRadioButton *radioButtonNo;
    QSpacerItem *verticalSpacer_2;
    QPushButton *pushButtonCargar;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer_4;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(456, 343);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout->addItem(verticalSpacer);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelNombre = new QLabel(centralWidget);
        labelNombre->setObjectName(QString::fromUtf8("labelNombre"));

        horizontalLayout->addWidget(labelNombre);

        lineEditNombre = new QLineEdit(centralWidget);
        lineEditNombre->setObjectName(QString::fromUtf8("lineEditNombre"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(lineEditNombre->sizePolicy().hasHeightForWidth());
        lineEditNombre->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(lineEditNombre);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        Legajo = new QLabel(centralWidget);
        Legajo->setObjectName(QString::fromUtf8("Legajo"));

        horizontalLayout_3->addWidget(Legajo);

        lineEditLegajo = new QLineEdit(centralWidget);
        lineEditLegajo->setObjectName(QString::fromUtf8("lineEditLegajo"));
        sizePolicy.setHeightForWidth(lineEditLegajo->sizePolicy().hasHeightForWidth());
        lineEditLegajo->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(lineEditLegajo);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        labelEdad = new QLabel(centralWidget);
        labelEdad->setObjectName(QString::fromUtf8("labelEdad"));

        horizontalLayout_2->addWidget(labelEdad);

        spinBoxEdad = new QSpinBox(centralWidget);
        spinBoxEdad->setObjectName(QString::fromUtf8("spinBoxEdad"));

        horizontalLayout_2->addWidget(spinBoxEdad);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        checkBoxDeudaFinales = new QCheckBox(centralWidget);
        checkBoxDeudaFinales->setObjectName(QString::fromUtf8("checkBoxDeudaFinales"));

        horizontalLayout_4->addWidget(checkBoxDeudaFinales);

        checkBoxCantidadMaterias = new QCheckBox(centralWidget);
        checkBoxCantidadMaterias->setObjectName(QString::fromUtf8("checkBoxCantidadMaterias"));

        horizontalLayout_4->addWidget(checkBoxCantidadMaterias);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        labelSiTrabaja = new QLabel(centralWidget);
        labelSiTrabaja->setObjectName(QString::fromUtf8("labelSiTrabaja"));

        horizontalLayout_5->addWidget(labelSiTrabaja);

        radioButtonSi = new QRadioButton(centralWidget);
        radioButtonSi->setObjectName(QString::fromUtf8("radioButtonSi"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(radioButtonSi->sizePolicy().hasHeightForWidth());
        radioButtonSi->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(radioButtonSi);

        radioButtonNo = new QRadioButton(centralWidget);
        radioButtonNo->setObjectName(QString::fromUtf8("radioButtonNo"));
        sizePolicy1.setHeightForWidth(radioButtonNo->sizePolicy().hasHeightForWidth());
        radioButtonNo->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(radioButtonNo);


        verticalLayout->addLayout(horizontalLayout_5);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout->addItem(verticalSpacer_2);

        pushButtonCargar = new QPushButton(centralWidget);
        pushButtonCargar->setObjectName(QString::fromUtf8("pushButtonCargar"));

        verticalLayout->addWidget(pushButtonCargar);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_3);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        verticalLayout->addItem(horizontalSpacer);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout->addItem(verticalSpacer_4);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 456, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Ejemplo Qt Design", nullptr));
        labelNombre->setText(QApplication::translate("MainWindow", "Nombre", nullptr));
        Legajo->setText(QApplication::translate("MainWindow", "Legajo", nullptr));
        labelEdad->setText(QApplication::translate("MainWindow", "Edad", nullptr));
        checkBoxDeudaFinales->setText(QApplication::translate("MainWindow", "Adeuda m\303\241s de 2 finales", nullptr));
        checkBoxCantidadMaterias->setText(QApplication::translate("MainWindow", "Cursa 3 materias o m\303\241s", nullptr));
        labelSiTrabaja->setText(QApplication::translate("MainWindow", "Usted tiene trabajo?", nullptr));
        radioButtonSi->setText(QApplication::translate("MainWindow", "Si", nullptr));
        radioButtonNo->setText(QApplication::translate("MainWindow", "No", nullptr));
        pushButtonCargar->setText(QApplication::translate("MainWindow", "Cargar datos", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
