#include "alumno.h"

using namespace std;
unsigned long int Alumno::cantidadAlumnos = 0;

Alumno::Alumno(string nombre,unsigned int edad,unsigned long int legajo, string universidad)
    :Persona(nombre,edad)
{
    this->legajo = legajo;
    this->facultad = universidad;
    materias = nullptr;
    cantidadAlumnos++;
    cantidadMaterias = 0;
}
Alumno::~Alumno()
{
    cantidadAlumnos--;
}
unsigned long int Alumno::cantidadEstudiantes()
{
    return cantidadAlumnos;
}
bool Alumno::materiasIsEmpty()
{
   return(materias == nullptr ? true:false);
}
int Alumno::agregarMateria (Materia_t nuevaAsignatura)
{
    int return_value = -1;
    if(cantidadMaterias < MAX_MATERIAS){

        Materia * nuevaMateria = new Materia(nuevaAsignatura);
        nuevaMateria->setSiguiente(nullptr);
        cantidadMaterias++;

        if(materiasIsEmpty()){
            this->materias = nuevaMateria;
        }else{
         Materia * punteroAuxiliar = materias;
         while (punteroAuxiliar->siguiente() != nullptr)
         {
             punteroAuxiliar = punteroAuxiliar->siguiente();
         }
         punteroAuxiliar->setSiguiente(nuevaMateria);
        }
        return_value = 0;

    }else {
        cout<<"Error Alumno::agregarMateria : No se pueden agregar mas materias, alcanzo el maximo de "<<MAX_MATERIAS<<endl;
    }
    return return_value;
}
int Alumno::darBajaMateria(Materia_t asignaturaBaja)
{
    int return_value = ERROR;
    if(!materiasIsEmpty()){
        Materia *auxiliar = materias;
        bool flagNullptr = false;
        bool flagMateriaEncontrada = false;
        do
        {
            if(auxiliar->asignatura() == asignaturaBaja){
                flagMateriaEncontrada = true;
            }else{
                if(auxiliar->siguiente() != nullptr)
                {
                    auxiliar = auxiliar->siguiente();
                }else{
                    flagNullptr = true;
                }
            }

        }while(flagMateriaEncontrada == false && flagNullptr == false);

        if(flagMateriaEncontrada == true)
        {
            cout<<"Alumno::darBajaMateria: Se ha dado de baja correctamente de la materia"<< itoaMateria(auxiliar->asignatura())<<endl;
            delete(auxiliar);
            return_value = OK;
        }else{
            cout<<"Error en Alumno::darBajaMateria: No se ha encontrado la materia"<< itoaMateria(auxiliar->asignatura())<<endl;
        }
    }else{
        cout<<"Error en Alumno::darBajaMateria: No se encuentra anotado en ninguna materia"<<endl;
    }
    return return_value;
}

int Alumno::mostrarMaterias(void)
{
    int return_value = ERROR;
    if(!materiasIsEmpty()){
        Materia * auxiliar = materias;
        cout<<"------ Lista de materias de "<<nombrePersona()<<" ------"<<endl;
        do
        {
            cout<<itoaMateria(auxiliar->asignatura())<<endl;
            unsigned int calificacion = auxiliar->nota() ;
            if(calificacion == 0){
                cout<<"Nota = Todavia sin calificar"<<endl;
            }else{
                cout<<"Nota : "<<calificacion<<endl;
            }
            auxiliar = auxiliar->siguiente();
        }while(auxiliar != nullptr);
        cout<<"----------------------- Fin -----------------------"<<endl;
        return_value = OK;
    }else{
        cout<<"Error Alumno::mostrarMaterias : No tiene materias asignadas"<<endl;
    }
    return return_value;
}
