#ifndef DOCENTE_H
#define DOCENTE_H

#include "persona.h"
#include "alumno.h"
#include "materia.h"

class Docente : public Persona // Clase derivada
{
public:
    enum Rol_t{
        ProfesorTitular = 0,
        ProfesorAuxiliar,
        JefeDeTP,
        AyudanteAlumno,
        AyudanteRecibido
    };
    Docente(string nombre,unsigned int edad,Docente::Rol_t rol = ProfesorTitular,Materia_t materia = MATERIA_DEFAULT,string universidad = UNIVERSIDAD_DEFAULT);
    void corregirTarea(Alumno,string);

private:
    string facultad;
    string rol;
    Materia_t materia;
};

#endif // DOCENTE_H

