#ifndef MATERIA_H
#define MATERIA_H

#include "persona.h"

enum Materia_t
{
  Informatica2 = 0,
  ASyS,
  AnalisisMatematico2,
  Fisica2,
  FisicaElectronica,
  ProbabilidadyEstadistica
};

class Materia
{
    //agregar cout
public:
    friend class Docente;
    Materia(Materia_t asignatura);
    unsigned int nota();
    void setSiguiente(Materia*);
    Materia * siguiente();
    Materia_t asignatura();
private:
    void setNota(unsigned int);
    unsigned int calificacion;
    Materia_t nombreMateria;
    Materia * siguienteMateria;
};

string itoaMateria(Materia_t);

#endif // MATERIA_H
