#ifndef PERSONA_H
#define PERSONA_H

#include <QCoreApplication>
#include <iostream>

#define MAX_MATERIAS 10
#define UNIVERSIDAD_DEFAULT "UTN_BA"
#define MATERIA_DEFAULT Informatica2

using namespace std;

class Persona // Clase base
{
public:
    Persona( string nombre,unsigned int edad);
    string nombrePersona()const;
    unsigned int edadPersona()const;
private:
    string nombreCompleto;
    unsigned int edad;
};


#endif // PERSONA_H
