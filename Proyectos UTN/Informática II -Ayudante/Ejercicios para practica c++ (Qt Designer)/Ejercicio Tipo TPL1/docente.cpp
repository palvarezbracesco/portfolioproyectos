#include "docente.h"
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#define UNUSED(x) (void)(x)


Docente::Docente(string nombre,unsigned int edad,Docente::Rol_t rol,Materia_t materia ,string universidad)
    :Persona(nombre,edad)
{
    this->rol = rol;
    this->facultad = universidad;
    this->materia = materia;
}
void Docente::corregirTarea(Alumno alumno,string tarea)
{
    UNUSED(tarea);//en este caso no se corrije realmente la tarea y es por eso que no usamos el parametro tarea, esto es para evitar el warning
    if(!alumno.materiasIsEmpty()){
        bool flagMateriaEncontrada = false;
        bool flagNullptr = false;
        Materia *auxiliar = alumno.materias;
        do{

            if(auxiliar->asignatura() == this->materia){
                flagMateriaEncontrada = true;
            }else if (auxiliar->siguiente() == nullptr){
                flagNullptr = true;
            }
            else {
                auxiliar = auxiliar->siguiente();
            }
        }while(flagMateriaEncontrada == false && flagNullptr == false);
        if(flagMateriaEncontrada == true){
            unsigned int nota;
            srand(static_cast<unsigned int>(time(nullptr))); // para evitar warning (time devuelve un long int y nota es un unsigned int)
            nota = (rand()) % 11;
            if(nota == 0)
                nota = 1;
            auxiliar->setNota(nota);
        }else{
            cout<<"Error en Docente::setNota : el alumno"<<alumno.nombrePersona()<<"no pertenece a la materia"<< itoaMateria(auxiliar->asignatura())<<endl;
        }

    }else{
        cout<<"Error en Docente::setNota : el alumno no se encuentra anotado a ninguna materia"<<endl;
    }
}
