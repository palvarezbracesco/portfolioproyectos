#ifndef ALUMNO_H
#define ALUMNO_H

#include "persona.h"
#include "materia.h"


#define OK 0
#define ERROR 1

class Alumno : public Persona // Clase derivada
{

public:
    friend class Docente;
    Alumno(string nombre,unsigned int edad,unsigned long int legajo, string universidad = UNIVERSIDAD_DEFAULT);
    ~Alumno();
    int agregarMateria (Materia_t nuevaAsignatura);
    int darBajaMateria (Materia_t nuevaAsignatura);
    int mostrarMaterias(void);
    unsigned long int cantidadEstudiantes();

private:
    bool materiasIsEmpty();
    string facultad;
    Materia *materias;
    unsigned long int legajo;
    unsigned int cantidadMaterias;
    static unsigned long int cantidadAlumnos;
};

#endif // ALUMNO_H

