#include <QCoreApplication>
#include "persona.h"
#include "alumno.h"
#include "docente.h"
#include "materia.h"
using namespace std;

int main()
{
    Alumno Jorge("Jorge",20,1590669),Augusto("Augusto",20,1591338);
    Docente Daniel("Daniel",35),Mariano("Mariano",35,Docente::JefeDeTP),Pablo("Pablo",23,Docente::AyudanteAlumno);

    Jorge.agregarMateria(Informatica2);
    Jorge.agregarMateria(ASyS);
    Jorge.agregarMateria(AnalisisMatematico2);

    Augusto.darBajaMateria(Informatica2);
    Augusto.agregarMateria(Informatica2);
    cout<<"Cantidad de alumnos : "<<Augusto.cantidadEstudiantes()<<endl;

    cout<<Augusto.nombrePersona()<<" ingrese por favor la tarea de la materia "<<itoaMateria(Informatica2)<<endl;
    string tareaAugusto;
    cin>> tareaAugusto;
    Daniel.corregirTarea(Augusto,tareaAugusto);

    Augusto.mostrarMaterias();
    return 0;
}
