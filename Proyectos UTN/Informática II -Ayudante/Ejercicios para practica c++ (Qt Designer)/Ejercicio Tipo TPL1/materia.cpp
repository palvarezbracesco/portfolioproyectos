#include "materia.h"

string  itoaMateria(Materia_t nombreMateria)
{
    string return_value;
    switch(nombreMateria)
    {
    case Informatica2:
        return_value = "Informática II";
        break;
    case ASyS:
        return_value = "ASyS";
        break;
    case AnalisisMatematico2:
        return_value = "Análisis Matemático II";
        break;
    case Fisica2:
        return_value = "Física II";
        break;
    case FisicaElectronica:
        return_value = "Física electrónica";
        break;
    case ProbabilidadyEstadistica:
        return_value = "Probabilidad y estadística";
        break;
    }
    return return_value;
}
Materia_t Materia:: asignatura()
{
    return nombreMateria;
}
Materia::Materia(Materia_t asignatura)
{
    nombreMateria = asignatura;
    calificacion = 0;
    siguienteMateria = nullptr;
}
void Materia::setSiguiente(Materia* siguiente)
{
    siguienteMateria = siguiente;
}
Materia * Materia::siguiente()
{
    return siguienteMateria;
}
unsigned int Materia::nota()
{
    unsigned int return_value = 0;
    if(calificacion != 0)
        return_value = calificacion;
    return return_value;
}
void Materia::setNota(unsigned int notaNueva)
{
    if(notaNueva != 0)
    {
        calificacion = notaNueva;

    }else{
        cout<<"Error Materia::setNota : Valor de nota invalido "<<endl;
    }
}
