#include "empleado.h"
#include <iostream>
using namespace std;

Empleado::Empleado(std::string nombre, Cargo cargo, unsigned int edad, unsigned long int sueldo)
{
    setNombre(nombre);
    setCargo(cargo);
    setEdad(edad);
    setSueldo(sueldo);

}
void Empleado::setNombre(std::string nombre)
{
    this->nombreEmpleado = nombre;
}
void Empleado::setCargo(Cargo cargo)
{
    this->cargoEmpleado = cargo;
}
void Empleado::setEdad(unsigned int edad)
{
    this->edadEmpleado = edad;
}
void Empleado::setSueldo(unsigned long int sueldo)
{
    this->sueldoEmpleado = sueldo;
}
std::string Empleado::nombre(void)const
{
    return (this->nombreEmpleado);
}
Empleado::Cargo Empleado::cargo(void)const
{
    return (this->cargoEmpleado);
}
unsigned int Empleado::edad(void)const
{
    return (this->edadEmpleado);
}
unsigned long int Empleado::sueldo(void)const
{
    return (this->sueldoEmpleado);
}
