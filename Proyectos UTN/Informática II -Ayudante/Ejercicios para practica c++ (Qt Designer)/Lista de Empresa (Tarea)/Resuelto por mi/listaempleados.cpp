#include "listaempleados.h"
#include <string.h>
#include <iostream>
#define DOWN 0
#define UP 1

using namespace std;


ListaEmpleados::ListaEmpleados ()
{
    cantidadTrabajadores = 0;
    nodoInicio = nullptr;
}
Nodo * ListaEmpleados::primerNodo () const
{
    return nodoInicio;
}
Nodo* ListaEmpleados::nuevoNodo(std::string nombre,Empleado::Cargo cargo,unsigned int edad,unsigned long int sueldo)
{
    Nodo * nuevo = new Nodo(nombre,cargo,edad,sueldo);
    return nuevo;
}
bool ListaEmpleados::isEmpty()const
{
    return(nodoInicio == nullptr ? true : false);
}
void ListaEmpleados::agregarEmpleado (std::string nombre, Empleado::Cargo cargoNuevoEmpleado,unsigned int edad,unsigned long int sueldo)
{
    Nodo *nuevo = nuevoNodo(nombre, cargoNuevoEmpleado, edad, sueldo);

    if(isEmpty() == false){
        Nodo * prev = nullptr;
        Nodo * aux = nullptr;
        aux = nodoInicio;
        while(aux != nullptr && aux->empleado().cargo() >= cargoNuevoEmpleado) {
            prev = aux;
            aux = aux->siguiente();
        }
        if(prev == nullptr) {
            nuevo->setSiguiente(nodoInicio);
            nodoInicio = nuevo;
        }
        else {
            prev->setSiguiente(nuevo);
            nuevo->setSiguiente(aux);
        }
    }
    else {
        nodoInicio = nuevo;
        nodoInicio->setSiguiente(nullptr);
    }
    cantidadTrabajadores++;
}
void ListaEmpleados::borrarNodo (Nodo * nodo_delete)
{
    if(nodo_delete != nullptr)
        delete (nodo_delete);
}
void ListaEmpleados::quitarEmpleadoPrimero (void)
{
    if (false == isEmpty()){
        cantidadTrabajadores--;
        Nodo * auxiliar = nodoInicio;
        nodoInicio = nodoInicio->siguiente();
        borrarNodo (auxiliar);
        cout<< "Eliminando primer empleado..."<<endl;

    } else
        cout<< "Error en quitarEmpleadPrimero : Lista vacia" <<endl;
}
void ListaEmpleados::quitarEmpleadoUltimo (void)
{
    if (false == isEmpty()){
        cantidadTrabajadores--;
        Nodo * auxiliarAnterior;
        Nodo * auxiliarBorrar;
        auxiliarBorrar = nodoInicio;
        auxiliarAnterior = auxiliarBorrar;
        while (auxiliarBorrar->siguiente()!=nullptr){
            auxiliarAnterior = auxiliarBorrar;
            auxiliarBorrar = auxiliarBorrar->siguiente();
        }
        if (auxiliarBorrar != nodoInicio){
            auxiliarAnterior->setSiguiente(nullptr);
        } else
            nodoInicio = nullptr;
        borrarNodo(auxiliarBorrar);
        cout<< "Eliminando ultimo empleado..."<<endl;
     } else
        cout<< "Error en quitarEmpleadoUltimo: Lista vacia" <<endl;
}
void ListaEmpleados::quitarEmpleadoElegido (std::string nombre, Empleado::Cargo cargo)
{
    if (false == isEmpty()){
        cantidadTrabajadores--;
        int breakFlag = DOWN;
        Nodo * auxiliarAnterior;
        Nodo * auxiliarSiguiente;

        auxiliarSiguiente = nodoInicio;
        auxiliarAnterior = auxiliarSiguiente;
        do{
            if (cargo == auxiliarSiguiente->empleado().cargo() && nombre == auxiliarSiguiente->empleado().nombre()){
                    breakFlag = UP; // se encontro el empleado a borrar
            }else{
                auxiliarAnterior = auxiliarSiguiente;
                auxiliarSiguiente = auxiliarSiguiente->siguiente();
            }
        } while (auxiliarSiguiente != nullptr && breakFlag != UP);

        if (DOWN == breakFlag){

            cout<< "Error quitarEmpleadoElegido: No se ha encontrado el empleado" <<endl;

        } else {        // breakFlag=UP
            if (auxiliarSiguiente != nodoInicio){
                auxiliarAnterior->setSiguiente(auxiliarSiguiente->siguiente());
            } else {
                nodoInicio = auxiliarSiguiente->siguiente();
            }
            borrarNodo(auxiliarSiguiente);
        }
    }
    else
        cout<<"Error en quitarEmpleadoElegido: Lista vacia"<<endl;
}
int ListaEmpleados::cantidadEmpleados(void)const
{
    return(cantidadTrabajadores);
}
std::string ListaEmpleados::itoaCargo(Empleado::Cargo cargo_int)const
{
    std::string return_value = "ERROR";
    switch (cargo_int){
    case Empleado::Administrativo:
        return_value = "Administrativo";
        break;
    case Empleado::Tecnico:
        return_value = "Tecnico";
        break;
    case Empleado::JefeDeptoAdministrativo:
        return_value = "Jefe de Depto. Administrativo";
        break;
    case Empleado::JefeDeptoTecnico:
        return_value = "Jefe de Depto. Tecnico";
        break;
    case Empleado::Director:
        return_value = "Director";
        break;
    case Empleado::Jefe:
        return_value = "Jefe";
    }
    return return_value;
}

void ListaEmpleados::mostrarLista(void)const
{
    if (false == isEmpty()){
        Empleado::Cargo cargoInt;
        Nodo * auxiliar;
        auxiliar = nodoInicio;

        cout<< "-------------------- LISTA DE EMPLEADOS --------------------" <<endl;
        cout<< "Cantidad de empleados: ";
        cout<< cantidadTrabajadores<<endl;
        while (auxiliar != nullptr){

            cout<< endl << "- Nombre Empleado: ";
            cout<< (auxiliar->empleado().nombre()) <<endl;
            cout<< "- Cargo: ";
            cargoInt = auxiliar->empleado().cargo();
            cout<< (itoaCargo(cargoInt)) <<endl;
            cout<< "- Edad: ";
            cout<< (auxiliar->empleado().edad()) <<endl;
            cout<< "- Sueldo: $";
            cout<< (auxiliar->empleado().sueldo()) <<endl<<endl<<endl;
            auxiliar = auxiliar->siguiente();
        }
        cout<< "-------------------- FIN LISTA  ----------------------------" <<endl;
    }else
        puts("Error en mostrarLista: Lista vacia");
}

ListaEmpleados::~ListaEmpleados()
{
    if (false == isEmpty()){
        Nodo * auxiliar = nodoInicio;

        while (auxiliar != nullptr){
            nodoInicio = nodoInicio->siguiente();
            borrarNodo(auxiliar);
            auxiliar = nodoInicio;
        }
        borrarNodo(nodoInicio);
    }
}
