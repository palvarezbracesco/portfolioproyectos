#include "nodo.h"
#include <iostream>

Nodo::Nodo(std::string nombre,Empleado::Cargo cargo,unsigned int edad,unsigned long int sueldo)
    :trabajador(nombre,cargo,edad,sueldo)
{
    nodoSiguiente = nullptr;
}
Empleado  Nodo::empleado() const
{
    return trabajador;
}

void Nodo::setSiguiente(Nodo * siguiente)
{
    nodoSiguiente = siguiente;
}
Nodo * Nodo::siguiente() const
{
    return nodoSiguiente;
}


