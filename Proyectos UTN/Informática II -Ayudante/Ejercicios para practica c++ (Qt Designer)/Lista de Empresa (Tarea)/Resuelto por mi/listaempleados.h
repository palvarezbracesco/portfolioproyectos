#ifndef LISTAEMPLEADOS_H
#define LISTAEMPLEADOS_H
#include "nodo.h"

class ListaEmpleados
{
public:
    ListaEmpleados();
    ~ListaEmpleados();
    void agregarEmpleado(std::string nombre, Empleado::Cargo cargo,unsigned int edad,unsigned long int sueldo);
    void quitarEmpleadoPrimero(void);
    void quitarEmpleadoUltimo(void);
    void quitarEmpleadoElegido(std::string nombre, Empleado::Cargo cargo);
    int cantidadEmpleados(void)const;
    Nodo * primerNodo(void)const;
    void mostrarLista(void)const;
private:
    Nodo * nuevoNodo(std::string nombre,Empleado::Cargo cargo,unsigned int edad,unsigned long int sueldo);
    void borrarNodo(Nodo* nodo_delete);
    bool isEmpty()const;
    std::string itoaCargo(Empleado::Cargo)const;

    int cantidadTrabajadores;
    Nodo * nodoInicio;
};

#endif // LISTA_EMPLEADOS_H

