#ifndef EMPLEADO_H
#define EMPLEADO_H

#include <string>

class Empleado
{
public:
    enum Cargo{
       Administrativo=-5,
       Tecnico,
       JefeDeptoAdministrativo,
       JefeDeptoTecnico,
       Director,
       Jefe=0,
    };
    Empleado(std::string,Cargo,unsigned int,unsigned long int);
    std::string nombre(void)const; //Método constante
    Cargo cargo(void)const;
    unsigned int edad(void)const;
    unsigned long int sueldo(void)const;

    void setNombre(std::string);
    void setCargo(Cargo);
    void setEdad(unsigned int);
    void setSueldo(unsigned long int);
private:
    std::string nombreEmpleado;
    Cargo cargoEmpleado;
    unsigned int edadEmpleado;
    unsigned long int sueldoEmpleado;

};

#endif // EMPLEADO_H
