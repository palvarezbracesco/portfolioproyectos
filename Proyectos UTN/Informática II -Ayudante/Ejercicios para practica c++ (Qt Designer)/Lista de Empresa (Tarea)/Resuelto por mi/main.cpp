/* Una empresa de tecnología necesita cargar en una lista a todos sus empleados
 * y que contenga como datos: el nombre, la edad, el cargo y el sueldo
 * (+)Ademas necesita que la lista de empleados quede ordenada de la siguiente forma:
 *  - Jefe (0)
 *  - Director (-1)
 *  - Jefe del departamento técnico (-2)
 *  - Jefe del departamento administrativo (-3)
 *  - Técnicos (-4)
 *  - Administrativos (-5)
 *
 * Para esto es necesario crear una clase que permita cargar dichos datos
 * Se recomienda armar: una clase que sea la lista, otra que sea los nodos y otra que tenga los datos del empleado
 * Se da un main que muestra un uso basico de la clase y como se cargan los datos
 *
 *
 * Se pide:
 *          - crear la clase lista de empleados(con su correspondiente .cpp y su .h)
 *          - que los datos dentro de la lista queden ordenados por jerarquia(ver +)
 *          - que la clase del empleado contenga los datos pedidos
 *          - la clase de la lista de empleados debe tener funciones que : permitan agregar empleados, permita mostrar la lista (mostrar todos los empleados
 *          con todos los datos de cada uno), permita borrar un empleado (deben haber 3 funciones distintas que se encarguen de esto: borrar el primero de la lista (el de mas alto rango),
 *          borrar un empleado que se desee pasandole como argumentos el nombre y el cargo y otra funcion que se encargue de borrar a ultimo
 *          de la lista (el de menor rango) )
 *          - poder saber la cantidad de empleados
 *          - las funciones deben tener chequeos de seguridad (ejemplo: advertir si se esta queriendo borrar un empleado que no es parte de la lista
 *          o que se desea imprimir una lista que esta vacia)
 *Se recomiendan que los datos sean del tipo:
 *      std::string para el nombre
 *      Cargo para el cargo // para asegurarnos de que el cargo pasado por argumento es parte de los definidos dentro de enum
 *      unsigned int para la edad
 *      unsigned long int para el sueldo
*/
/*
 * recomendacion para los cargos:
typedef  enum
{
   Administrativo=-5,
   Tecnico,
   JefeDeptoAdministrativo,
   JefeDeptoTecnico,
   Director,
   Jefe=0,
}Cargo;
*/


#include <QCoreApplication>
#include "empleado.h"
#include "listaempleados.h"
#include "nodo.h"
#include <iostream>

int main(void)
{

   ListaEmpleados * listaTrabajo = new (ListaEmpleados);
   ListaEmpleados * lista2 = new ListaEmpleados;
   listaTrabajo->agregarEmpleado("Conrado",Empleado::Jefe,57,500000);
   listaTrabajo->agregarEmpleado("Carlos",Empleado::Director,50,350000);
   listaTrabajo->agregarEmpleado("Roberto",Empleado::JefeDeptoTecnico,45,150000);
   listaTrabajo->agregarEmpleado("Raul",Empleado::JefeDeptoAdministrativo,48,135000);
   listaTrabajo->agregarEmpleado("Pedro",Empleado::Tecnico,30,35000);
   listaTrabajo->agregarEmpleado("Gabriel",Empleado::Administrativo,27,27000);
   listaTrabajo->agregarEmpleado("Leo",Empleado::Administrativo,23,19000);

   //lista2->agregarEmpleado("Jorge",Empleado::Administrativo,55,55000);
   //lista2->mostrarLista();
   //std::cout<<lista2->cantidadEmpleados();
   listaTrabajo->mostrarLista();
   listaTrabajo->quitarEmpleadoUltimo();
   listaTrabajo->mostrarLista();
   delete listaTrabajo;
   return 0;
}
