#ifndef NODO_H
#define NODO_H

#include "empleado.h"

class Nodo
{
public:

    Nodo(std::string,Empleado::Cargo,unsigned int,unsigned long int);
    Empleado  empleado()const;
    Nodo * siguiente() const;
    void setSiguiente(Nodo*);
private:
 Empleado  trabajador;
 Nodo * nodoSiguiente;
};

#endif // NODO_H
