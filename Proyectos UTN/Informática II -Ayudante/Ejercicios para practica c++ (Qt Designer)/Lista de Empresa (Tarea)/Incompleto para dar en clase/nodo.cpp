#include "nodo.h"

Nodo::Nodo(std::string nombre,Empleado::Cargo cargo,unsigned int edad,unsigned long int sueldo)
    :trabajador(nombre,cargo,edad,sueldo)
{
    this->nodoSiguiente = nullptr;

}

Empleado Nodo:: empleado()const
{
    //Empleado auxiliar(this->nombre);
    return  this->trabajador;
}
Nodo * Nodo:: siguiente() const
{
    return this->nodoSiguiente;
}
void Nodo:: setSiguiente(Nodo* next)
{
    this->nodoSiguiente = next;
}

