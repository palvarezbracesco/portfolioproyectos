#ifndef NODO_H
#define NODO_H

#include "empleado.h"

class Nodo
{
public:

    //Constructor de la clase
    Nodo(std::string,Empleado::Cargo,unsigned int,unsigned long int);
    //Funciones get
    Empleado  empleado()const;
    Nodo * siguiente() const;
    //Funciones set
    void setSiguiente(Nodo*);
private:
 //Miembros privados
 Empleado  trabajador;
 Nodo * nodoSiguiente;
};


#endif // NODO_H
