#ifndef EMPLEADO_H
#define EMPLEADO_H

#include <string>

class Empleado
{
public:
    enum Cargo{
       Administrativo=-5,
       Tecnico,
       JefeDeptoAdministrativo,
       JefeDeptoTecnico,
       Director,
       Jefe=0,
    };

    //Constructor de la clase
    Empleado(std::string,Cargo,unsigned int,unsigned long int);

    //Funciones get (Métodos constantes)
    std::string nombre(void)const;
    Cargo cargo(void)const;
    unsigned int edad(void)const;
    unsigned long int sueldo(void)const;

    //Funciones set
    void setNombre(std::string);
    void setCargo(Cargo);
    void setEdad(unsigned int);
    void setSueldo(unsigned long int);
private:
    //Miembros privados
    std::string nombreEmpleado;
    Cargo cargoEmpleado;
    unsigned int edadEmpleado;
    unsigned long int sueldoEmpleado;

};

#endif // EMPLEADO_H
