#include "empleado.h"

Empleado::Empleado(std::string nombre,Cargo cargo,unsigned int edad ,unsigned long int sueldo)
{
    this->nombreEmpleado = nombre;
    this->cargoEmpleado = cargo;
    this->edadEmpleado = edad;
    this->sueldoEmpleado = sueldo;
}
std::string Empleado::nombre(void)const
{
    return this->nombreEmpleado;
}
Empleado::Cargo Empleado::cargo(void)const
{
    return this->cargoEmpleado;
}
unsigned int Empleado::edad(void)const
{
    return this->edadEmpleado;
}
unsigned long int Empleado::sueldo(void)const
{
    return this->sueldoEmpleado;
}
void Empleado::setNombre(std::string name)
{
    this->nombreEmpleado = name;
}

