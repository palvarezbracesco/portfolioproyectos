#ifndef LISTAEMPLEADOS_H
#define LISTAEMPLEADOS_H
#include "nodo.h"


class ListaEmpleados
{
public:
    //Constructor de la clase
    ListaEmpleados();
    //Destructor de la clase
    ~ListaEmpleados();
    //Funciones de listas
    void agregarEmpleado(std::string nombre, Empleado::Cargo cargo,unsigned int edad,unsigned long int sueldo);
    void quitarEmpleadoPrimero(void);
    void quitarEmpleadoUltimo(void);
    void quitarEmpleadoElegido(std::string nombre, Empleado::Cargo cargo);

    //Funciones get
    int cantidadEmpleados(void)const;
    Nodo * primerNodo(void)const;

    //Salida en pantalla
    void mostrarLista(void)const;
    //(si quieren lo hacemos sobrecargando cout)
    //friend std::ostream & operator << (std::ostream & out , ListaEmpleados& Lista);
private:
    //Funciones de lista pero privadas
    Nodo * nuevoNodo(std::string nombre,Empleado::Cargo cargo,unsigned int edad,unsigned long int sueldo);
    void borrarNodo(Nodo* nodo_delete);
    //Funcion para ver si esta vacia la lista
    bool isEmpty()const;
    //Funcion para pasar un dato de tipo Empleado::Cargo a std::string
    std::string itoaCargo(Empleado::Cargo)const;
    //Miembros privados
    Nodo * nodoInicio;
    int cantidadTrabajadores;
};

#endif // LISTA_EMPLEADOS_H
