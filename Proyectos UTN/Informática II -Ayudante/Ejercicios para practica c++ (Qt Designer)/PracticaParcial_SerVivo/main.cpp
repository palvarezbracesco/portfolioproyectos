#include <QCoreApplication>
#include "servivo.h"
#include "posicion.h"
#include "anfibios.h"
#include "aves.h"
#include "mamifero.h"
#include "peces.h"

int main(void)
{
    Posicion pos_serVivo1(-10,60,83);
    Posicion pos_anfibio1(-32,15,-9);
    Posicion pos_mamifero1(74,41,0);
    Posicion posicionNueva(100,-50,0);

    SerVivo serVivo1(pos_serVivo1,Cria);
    Anfibios anfibio1(pos_anfibio1,Adulto);
    Mamifero mamifero1(pos_mamifero1,Joven);

    cout<<anfibio1<<endl;

    Anfibios *anfibio2 = anfibio1.reproducirse();
    if(anfibio2 != nullptr){
        cout<<*(anfibio2)<<endl; //llamada en cascada
    }


    mamifero1.caminar(posicionNueva);
    mamifero1.alimentarse(Carne);
    serVivo1.respirar();
    cout<<mamifero1<<endl;

    //polimorfismo

    SerVivo *p = anfibio1.reproducirse();
    cout<<*p<<endl;

    delete p;
    delete anfibio2;

}
