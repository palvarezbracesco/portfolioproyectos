#include "aves.h"

unsigned int Aves::cantidadAves = 0;

Aves::Aves(Posicion posicion, NivelDeDesarrollo nivelDesarrollo)
    :SerVivo(posicion, nivelDesarrollo)
{
    cantidadAves++;
    identificadorAves = cantidadAves;
}
Aves * Aves::reproducirse()
{
    Aves * hijo = nullptr;
    if(getDesarrollo() != Cria){
        hijo = new Aves(this->getPosicion());
        cout<<"El ave numero "<<identificadorAves<<" se reprodujo"<<endl;
    }else{
        cout<<"ERROR: El ave numero "<<identificadorAves<<" no puede reproducirse debido a su nivel de desarrollo"<<endl;
    }

    return hijo;
}
ostream & operator << (ostream & out , Aves & ave)
{
    out<<(SerVivo &)ave;
    out<< "--------- Datos del ave ---------" <<std::endl;
    out<< "Hay "<<ave.cantidadAves<<" aves en total"<<endl;
    out<< "Este ave es el numero : "<<ave.identificadorAves<<endl;
    out<< "---------  Fin de los datos  ---------" <<std::endl;

    return out;
}
void Aves::caminar(Posicion posNueva)
{
    int x = posNueva.getX();
    int y = posNueva.getY();
    int z = posNueva.getZ();
    cout<<" El ave numero "<<identificadorAves<<" se encuentra caminando hacia la posicion ("<<x<<" , "<<y<<" , "<<z<<")"<<endl;
    this->moverse(posNueva);
}
void Aves::volar(Posicion posNueva)
{
    if(posNueva.getZ()>0){
        int x = posNueva.getX();
        int y = posNueva.getY();
        int z = posNueva.getZ();
        cout<<" El ave numero "<<identificadorAves<<" se encuentra volando hacia la posicion ("<<x<<" , "<<y<<" , "<<z<<")"<<endl;
        this->moverse(posNueva);
    }else{
        cout<<" ERROR : el ave numero "<<identificadorAves<<" no puede volar a una altura menor a 0"<<endl;
    }
}
