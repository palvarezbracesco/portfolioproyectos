#ifndef ANFIBIOS_H
#define ANFIBIOS_H

#include "servivo.h"

class Anfibios : public SerVivo
{
    friend ostream & operator << (ostream & out , Anfibios &);

public:
    Anfibios(Posicion posicion,NivelDeDesarrollo nivelDesarrollo = DESARROLLO_DEFAULT);
    Anfibios * reproducirse();
    void nadar(Posicion posNueva);
    void caminar(Posicion posNueva);

private:
    static unsigned int cantidadAnfibios;
    unsigned int identificadorAnfibio;
};

#endif // ANFIBIOS_H
