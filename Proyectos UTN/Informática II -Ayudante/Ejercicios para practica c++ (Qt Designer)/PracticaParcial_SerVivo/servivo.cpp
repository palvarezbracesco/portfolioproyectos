#include "servivo.h"

unsigned int SerVivo::cantidadSeresVivos = 0;

SerVivo:: SerVivo(Posicion posicion,NivelDeDesarrollo nivelDesarrollo)
    :ubicacion(posicion)
{
    cantidadSeresVivos++;
    identificador = cantidadSeresVivos;
    desarrollo = nivelDesarrollo;
    respirar();
}
void SerVivo::respirar(void)const
{
    cout<<"El ser vivo numero "<<identificador<<" esta respirando..."<<endl;
}
string SerVivo::itoaAlimento(Comida alimento)const
{
    string return_value;
    switch(alimento)
    {
    case Carne:
        return_value = "carne";
        break;
    case Vegetales:
        return_value = "vegetales";
        break;
    case Frutas:
        return_value = "frutas";
        break;
    case Insectos:
         return_value = "insectos";
         break;
    }
    return return_value;
}
string SerVivo::itoaDesarrollo()const
{
    string return_value;
    switch(desarrollo)
    {
    case Cria:
        return_value = "cria";
        break;
    case Joven:
        return_value = "joven";
        break;
    case Adulto:
        return_value = "adulto";
        break;
    }
    return return_value;
}
void SerVivo::alimentarse(Comida comida)const
{
    cout<<"El ser vivo numero "<<identificador<<" esta alimentandose de : "<<itoaAlimento(comida)<<endl;
}
Posicion SerVivo::getPosicion()
{
    return ubicacion;
}
NivelDeDesarrollo SerVivo::getDesarrollo()
{
    return desarrollo;
}
SerVivo * SerVivo::reproducirse()
{
    SerVivo * hijo = new SerVivo(ubicacion);
    return hijo;
}
void SerVivo::moverse(Posicion pos)
{
    ubicacion = pos;
}
ostream & operator << (ostream & out , SerVivo & ser)
{
    out<< "--------- Datos del ser vivo ---------" <<std::endl;
    out<< "Hay "<<ser.cantidadSeresVivos<<" seres vivos en total"<<endl;
    out<< "Este ser vivo es el numero : "<<ser.identificador<<endl;
    out<< "Su nivel de desarrollo es : "<<ser.itoaDesarrollo()<<endl;
    out<< "Se encuentra ubicado en la posicion : "<<endl;
    out<<"   - x = "<<ser.ubicacion.getX()<<endl;
    out<<"   - y = "<<ser.ubicacion.getY()<<endl;
    out<<"   - z = "<<ser.ubicacion.getZ()<<endl;
    return out;
}
