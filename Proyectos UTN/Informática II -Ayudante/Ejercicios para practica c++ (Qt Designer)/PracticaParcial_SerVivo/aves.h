#ifndef AVES_H
#define AVES_H

#include "servivo.h"

class Aves : public SerVivo
{
    friend ostream & operator << (ostream & out , Aves &);

public:
    Aves(Posicion posicion,NivelDeDesarrollo nivelDesarrollo = DESARROLLO_DEFAULT);
    Aves * reproducirse();
    void volar(Posicion posNueva);
    void caminar(Posicion posNueva);
private:
    static unsigned int cantidadAves;
    unsigned int identificadorAves;
};

#endif // AVES_H
