#include "anfibios.h"

unsigned int Anfibios::cantidadAnfibios = 0;

Anfibios::Anfibios(Posicion posicion, NivelDeDesarrollo nivelDesarrollo)
    :SerVivo(posicion, nivelDesarrollo)// lista inicializadora
{
    cantidadAnfibios++;
    identificadorAnfibio = cantidadAnfibios;
}
Anfibios * Anfibios::reproducirse()
{
    Anfibios * hijo = nullptr;
    if(getDesarrollo() != Cria){
        hijo = new Anfibios(this->getPosicion());
        cout<<"El anfibio numero "<<identificadorAnfibio<<" se reprodujo"<<endl;
    }else{
        cout<<"ERROR: El anfibio numero "<<identificadorAnfibio<<" no puede reproducirse debido a su nivel de desarrollo"<<endl;
    }

    return hijo;
}
ostream & operator << (ostream & out , Anfibios & anfibio)
{
    //out<<(SerVivo &)anfibio;
    out<<static_cast<SerVivo&>(anfibio);
    out<< "--------- Datos del anfibio ---------" <<std::endl;
    out<< "Hay "<<anfibio.cantidadAnfibios<<" anfibios en total"<<endl;
    out<< "Este anfibio es el numero : "<<anfibio.identificadorAnfibio<<endl;
    out<< "---------  Fin de los datos  ---------" <<std::endl;

    return out;
}
void Anfibios::nadar(Posicion posNueva)
{
    int x = posNueva.getX();
    int y = posNueva.getY();
    int z = posNueva.getZ();
    cout<<" El anfibio numero "<<identificadorAnfibio<<" se encuentra nadando hacia la posicion ("<<x<<" , "<<y<<" , "<<z<<")"<<endl;
    this->moverse(posNueva);
}
void Anfibios::caminar(Posicion posNueva)
{
    int x = posNueva.getX();
    int y = posNueva.getY();
    int z = posNueva.getZ();
    cout<<" El anfibio numero "<<identificadorAnfibio<<" se encuentra caminando hacia la posicion ("<<x<<" , "<<y<<" , "<<z<<")"<<endl;
    this->moverse(posNueva);
}
