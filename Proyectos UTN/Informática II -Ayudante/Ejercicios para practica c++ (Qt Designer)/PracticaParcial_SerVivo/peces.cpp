#include "peces.h"
unsigned int Peces::cantidadPeces = 0;

Peces::Peces(Posicion posicion, NivelDeDesarrollo nivelDesarrollo)
    :SerVivo(posicion, nivelDesarrollo)
{
    cantidadPeces++;
    identificadorPeces = cantidadPeces;
}
Peces * Peces::reproducirse()
{
    Peces * hijo = nullptr;
    if(getDesarrollo() != Cria){
        hijo = new Peces(this->getPosicion());
        cout<<"El pez numero "<<identificadorPeces<<" se reprodujo"<<endl;
    }else{
        cout<<"ERROR: El pez numero "<<identificadorPeces<<"no puede reproducirse debido a su nivel de desarrollo"<<endl;
    }

    return hijo;
}
ostream & operator << (ostream & out , Peces & pez)
{
    out<<(SerVivo &)pez;
    out<< "--------- Datos del pez---------" <<std::endl;
    out<< "Hay "<<pez.cantidadPeces<<" peces en total"<<endl;
    out<< "Este pez es el numero : "<<pez.identificadorPeces<<endl;
    out<< "---------  Fin de los datos  ---------" <<std::endl;

    return out;
}
void Peces::nadar(Posicion posNueva)
{
    int z = posNueva.getZ();
    if(z < 0){
        int x = posNueva.getX();
        int y = posNueva.getY();
        cout<<" El pez numero "<<identificadorPeces<<" se encuentra caminando hacia la posicion ("<<x<<" , "<<y<<" , "<<z<<")"<<endl;
        this->moverse(posNueva);
    }else{
        cout<<" ERROR : el pez numero "<<identificadorPeces<<" no puede ir a esa altura, ya que los peces solo se moveran por debajo del nivel del mar ( z < 0 )"<<endl;
    }
}

