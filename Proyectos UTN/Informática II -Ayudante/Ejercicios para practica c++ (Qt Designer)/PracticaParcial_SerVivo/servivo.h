#ifndef SERVIVO_H
#define SERVIVO_H

#include "posicion.h"
#include <iostream>
using namespace std;



enum Comida
{
  Carne = 0,
  Vegetales = 1,
  Frutas,
  Insectos
};
enum NivelDeDesarrollo
{
  Cria = 0,
  Joven = 1,
  Adulto
};

#define DESARROLLO_DEFAULT Cria

class SerVivo
{
    friend ostream & operator << (ostream & out , SerVivo &);

public:
    SerVivo(Posicion,NivelDeDesarrollo = DESARROLLO_DEFAULT);
    void respirar(void)const;
    void alimentarse(Comida)const;
    SerVivo * reproducirse();
    Posicion getPosicion();
    NivelDeDesarrollo getDesarrollo();
    void moverse(Posicion);
private:
    string itoaAlimento(Comida)const;
    string itoaDesarrollo()const;

    static unsigned int cantidadSeresVivos;
    unsigned int identificador;
    Posicion ubicacion;
    NivelDeDesarrollo desarrollo;

};

#endif // SERVIVO_H
