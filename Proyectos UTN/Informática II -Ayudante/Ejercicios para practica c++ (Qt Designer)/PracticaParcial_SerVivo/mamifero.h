#ifndef MAMIFERO_H
#define MAMIFERO_H

#include "servivo.h"

class Mamifero : public SerVivo
{
    friend ostream & operator << (ostream & out , Mamifero &);
public:
    Mamifero(Posicion posicion,NivelDeDesarrollo nivelDesarrollo = DESARROLLO_DEFAULT);
    Mamifero * reproducirse();

    void caminar(Posicion posNueva);
private:
    static unsigned int cantidadMamiferos;
    unsigned int identificadorMamiferos;
};

#endif // MAMIFERO_H

