#ifndef PECES_H
#define PECES_H

#include "servivo.h"

class Peces : public SerVivo
{
    friend ostream & operator << (ostream & out , Peces &);
public:
    Peces(Posicion posicion,NivelDeDesarrollo nivelDesarrollo = DESARROLLO_DEFAULT);
    void nadar(Posicion posNueva);
    Peces * reproducirse();
private:
    static unsigned int cantidadPeces;
    unsigned int identificadorPeces;
};

#endif // PECES_H




