#ifndef POSICION_H
#define POSICION_H


class Posicion
{
public:
    Posicion(int x = 0, int y = 0, int z = 0);
    int getX();
    int getY();
    int getZ();
private:
    int x,y,z;
};

#endif // POSICION_H
