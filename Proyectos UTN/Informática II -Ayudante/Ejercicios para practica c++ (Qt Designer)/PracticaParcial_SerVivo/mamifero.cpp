#include "mamifero.h"

unsigned int Mamifero::cantidadMamiferos = 0;

Mamifero::Mamifero(Posicion posicion, NivelDeDesarrollo nivelDesarrollo)
    :SerVivo(posicion, nivelDesarrollo)
{
    cantidadMamiferos++;
    identificadorMamiferos = cantidadMamiferos;
}
Mamifero * Mamifero::reproducirse()
{
    Mamifero * hijo = nullptr;
    if(getDesarrollo() != Cria){
        hijo = new Mamifero(this->getPosicion());
        cout<<"El mamifero numero"<<identificadorMamiferos<<"se reprodujo"<<endl;
    }else{
        cout<<"ERROR: El mamifero numero "<<identificadorMamiferos<<"no puede reproducirse debido a su nivel de desarrollo"<<endl;
    }

    return hijo;
}
ostream & operator << (ostream & out , Mamifero & mamifero)
{
    out<<(SerVivo &)mamifero;
    out<< "--------- Datos del mamifero---------" <<std::endl;
    out<< "Hay "<<mamifero.cantidadMamiferos<<" mamiferos en total"<<endl;
    out<< "Este mamifero es el numero : "<<mamifero.identificadorMamiferos<<endl;
    out<< "---------  Fin de los datos  ---------" <<std::endl;

    return out;
}
void Mamifero::caminar(Posicion posNueva)
{
    int x = posNueva.getX();
    int y = posNueva.getY();
    int z = posNueva.getZ();
    cout<<" El mamifero numero "<<identificadorMamiferos<<" se encuentra caminando hacia la posicion ("<<x<<" , "<<y<<" , "<<z<<")"<<endl;
    this->moverse(posNueva);
}
