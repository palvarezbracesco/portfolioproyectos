#include "producto.h"

Producto::Producto(Marca marca,Genero genero, unsigned int precio,unsigned int cantidad)
{
    Producto::marca = marca;
    this->genero = genero;
    this->precio = precio;
    this->cantidad = cantidad;
}
std::ostream & operator << (std::ostream & out , Producto & producto)
{
    std::string marcaString;
    std::string generoString;
    out<< "--------- Datos del producto ---------" <<std::endl;
    marcaString = producto.itoaMarca();
    generoString = producto.itoaGenero();
    out<< "Marca : " << marcaString <<std::endl;
    out<< "Genero : " << generoString <<std::endl;
    out<< "Precio : $" << producto.precio <<std::endl;
    out<< "Cantidad : " << producto.cantidad << " unidades" <<std::endl;
    return out;
}

std::string Producto::itoaMarca ()
{
    std::string returnValue;
    switch (this->marca){
        case Arcor:
            returnValue = "Arcor";
            break;
        case Matarazzo:
            returnValue = "Matarazzo";
            break;
        case Marolio:
            returnValue = "Marolio";
            break;
        case Ayudin:
            returnValue = "Ayudin";
            break;
        case Ivess:
            returnValue = "Ivess";
            break;
        case Fargo:
            returnValue = "Fargo";
            break;
        case BlancaFlor:
            returnValue = "BlancaFlor";
            break;
        case Hellmans:
            returnValue = "Hellmans";
            break;
        case Cocinero:
            returnValue = "Cocinero";
            break;
        case Villavicencio:
            returnValue = "Villavicencio";
            break;
   }
    return returnValue;
}

std::string Producto::itoaGenero()
{
    std::string returnValue;
    switch (this->genero){
        case Bebidas:
            returnValue = "Bebidas";
            break;
        case Golosinas:
            returnValue = "Golosinas";
            break;
    case Limpieza:
        returnValue = "Limpieza";
        break;
    case Aderezo:
        returnValue = "Aderezo";
        break;
    case Aceites:
        returnValue = "Aceites";
        break;
    case Panificados:
        returnValue = "Panificados";
        break;
    case AlimentosNoPerecederos:
        returnValue = "Alimentos no perecederos";
        break;
    case FideosHarinas:
        returnValue = "Fideos / Harinas";
        break;
    }
    return returnValue;
}

Producto Producto::operator++(void)
{
    this->cantidad++;
    return (*this);
}
Producto Producto::operator++(int)
{
    this->cantidad++;
    return (*this);
}
Producto  Producto::operator+(unsigned int i)
{
    this->cantidad += i;
    return (*this);
}
Producto Producto::operator--(void)
{
    this->cantidad--;
    return (*this);
}
Producto Producto::operator--(int)
{
    this->cantidad--;
    return (*this);
}
Producto Producto::operator-(unsigned int i)
{
    this->cantidad -= i;
    return (*this);
}
void Producto::operator+=(unsigned int i)
{
    this->cantidad += i;
}
void Producto::operator-=(unsigned int i)
{
    this->cantidad -= i;
}
bool Producto::operator==(const Producto& producto2)const
{
    bool returnValue = false;
    if (this->marca == producto2.marca){
        if (this->genero == producto2.genero){
            returnValue = true;
        }
    }
    return returnValue;
}
bool Producto::operator<(const Producto& producto2)const // en este caso lo usamos para el precio
{
    bool returnValue = false;
    if (this->genero != producto2.genero){
        std::cout<< "Error operator< : Los productos son de generos distintos"<<std::endl;
    }else if (this->precio < producto2.precio) {
        returnValue = true;
    }
    return returnValue;
}
bool Producto::operator>(const Producto& producto2)const // en este caso lo usamos para el precio
{
    bool returnValue = false;
    if (this->genero != producto2.genero){
        std::cout<< "Error operator< : Los productos son de generos distintos"<<std::endl;
    }else if (this->precio > producto2.precio) {
        returnValue = true;
    }
    return returnValue;
}
