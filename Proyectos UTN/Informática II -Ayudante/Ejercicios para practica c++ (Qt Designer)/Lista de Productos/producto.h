#ifndef PRODUCTO_H
#define PRODUCTO_H
#include <iostream>

class Producto
{
    friend std::ostream & operator << (std::ostream & out , Producto &);

public:
    enum Marca
    {
        Arcor=0,
        Matarazzo,
        Marolio,
        Ayudin,
        Ivess,
        Fargo,
        BlancaFlor,
        Hellmans,
        Cocinero,
        Villavicencio,
    };

    enum Genero
    {
        Bebidas = 0,
        Golosinas,
        Limpieza,
        Aderezo,
        Aceites,
        Panificados,
        AlimentosNoPerecederos,
        FideosHarinas
    } ;

    Producto(Marca,Genero, unsigned int ,unsigned int cantidad = 1);
    Producto operator++();
    Producto operator++(int);//posterior
    Producto operator+(unsigned int);
    Producto operator--();
    Producto operator--(int);//posterior
    Producto operator-(unsigned int i);
    void operator+=(unsigned int);
    void operator-=(unsigned int);
    bool operator==(const Producto&)const;
    bool operator<(const Producto&)const;
    bool operator>(const Producto&)const;
    std::string itoaMarca ();
    std::string itoaGenero();
private:
    Marca marca;
    Genero genero;
    unsigned int precio;
    unsigned int cantidad;
};

std::ostream & operator << (std::ostream & out , Producto & producto);

#endif // PRODUCTO_H
