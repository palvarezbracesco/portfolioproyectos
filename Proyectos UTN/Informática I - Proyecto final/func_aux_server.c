#include "includes.h"

extern int MAXCHILDS;

int contadormaxfd(int a, int b)
{
  if (a<b)
    return b;
  return a;
}
void config_load(char *argumento)
{
    int i,j,readed;
    char *buf,*extra;
    FILE *f;
    
    if((f=malloc(sizeof(FILE*)))==NULL)
    {
        perror("malloc file");
        exit(1);
    }
    if((f=fopen(argumento,"r+"))==NULL)
    {
        perror("open");
        exit(1);
    }
    if((buf=malloc(BUFFSIZE))==NULL)
    {
        perror("malloc buffer");
        exit(1);
    }
    do
    {
        if((readed= fread(buf,sizeof(char),BUFFSIZE,f))==0)
        {
            perror("read");
            exit(1);
        }
    } while(!feof(f));
    extra=malloc(strlen(buf)*sizeof(char));
    for(i=0,j=0;(*(buf+i))!='\n';i++)
    {
        if(isalpha((int)(*(buf+i)))==0 && (*(buf+i))!='=')
        {
            *(extra+j)=*(buf+i);
            j++;
        }
    }

    *(extra+j)='\0';
    MAXCHILDS=atoi(extra);
    printf("MAXCHILDS ES %d \n",MAXCHILDS);
        
    fclose(f);
    free(buf);
    free(extra);

return;
    
}

void set_audio_params (int fd)
{
	int 	arg;
	int	status;
	arg = SIZE;
	status = ioctl(fd, SOUND_PCM_WRITE_BITS, &arg); 
	if (status == -1) 
            perror("Error con comando SOUND_PCM_WRITE_BITS");
	if (arg != SIZE)
            fprintf (stderr,"Tamaño de muestras no soportado. Se programó %d\n",arg);
	arg = CHANNELS;  /* mono o stereo */
	status = ioctl(fd, SOUND_PCM_WRITE_CHANNELS, &arg);
	if (status == -1)
            perror("Error en comando SOUND_PCM_WRITE_CHANNELS");
	if (arg != CHANNELS)
            fprintf (stderr,"Cantidad de canales no soportado. Se programó %d\n",arg);
	arg = RATE;	   /* Velocidad de Muestreo */
	status = ioctl(fd, SOUND_PCM_WRITE_RATE, &arg);
	if (status == -1)
            perror("Error en comando SOUND_PCM_WRITE_RATE");
	if (arg != RATE)
            fprintf (stderr,"Velocidad de muestreo no soportada. Se programó %d\n",arg);

	return;
}



struct nodo* crear_nodo(char * d_name)
{
    nodo *p=malloc(sizeof(nodo));
    p->name_directory=malloc((strlen(d_name)+1));
    p->name_directory=memcpy(p->name_directory,d_name,(strlen(d_name)+1));
    return p;
    
}

void enlazar(nodo **head,nodo *agregar)
{
    nodo *recorrer=*head;
    while(recorrer->next!=NULL)
        recorrer=recorrer->next;
    recorrer->next=agregar;
    agregar->next=NULL;
    return;
}

void intercambiar_dato (nodo*recorrer,nodo*comparar)
{
    char*aux;
    aux=malloc((strlen(recorrer->name_directory)+1)*sizeof(char));
    strcpy(aux,recorrer->name_directory);
    recorrer->name_directory=realloc(recorrer->name_directory,(strlen(comparar->name_directory)+1)*sizeof(char));
    strcpy(recorrer->name_directory,comparar->name_directory);
    comparar->name_directory=realloc(comparar->name_directory,(strlen(aux)+1)*sizeof(char));
    strcpy(comparar->name_directory,aux);
    free(aux);
    return;
}
void ordenar (nodo **head)
{
    nodo * recorrer,*comparar;
    recorrer=*head;
    while(recorrer!=NULL)
    {
        comparar=recorrer->next;
        while(comparar!=NULL){
            if(strcmp(recorrer->name_directory,comparar->name_directory)>0 || strcmp(recorrer->name_directory,comparar->name_directory)==0 )
            {
                intercambiar_dato(recorrer,comparar);
            }
            comparar=comparar->next;
        }
        recorrer=recorrer->next;
    }
    
    return;    
}

int presentar_lista (nodo*auxiliar,int sockdup)
{
    int i=0;
    char start_msg[]="Empiezo transmision artistas";
    char end_msg[]="Fin transmision";
    while(auxiliar!=NULL)
    {
        
        if (!i)
        {
            if(write(sockdup,start_msg,100)==-1)
                return -1;
            i++;
        }
        
      if(write(sockdup,auxiliar->name_directory,100)==-1)
          return -1;
      auxiliar=auxiliar->next;
    }
    if(write(sockdup,end_msg,100)==-1)
        return -1;
return 0;
    
}
int borrar_lista(nodo **head){
    int i=0;
    nodo *extra=NULL,*extra2=NULL;
    if(*head==NULL)
      return -1;
    else{
      extra=*head;
      do{
	extra2=extra;
	extra=extra->next;
	free(extra2);
	i++;
      }while(extra!=NULL);
    }
free(*head);
*head=NULL;
return 0;
    
}

nodo* readdirectory (DIR *directory)
{
    int i=0;
    struct dirent *struct_directories;
    nodo*head,*auxiliar;
    
    while((struct_directories=readdir(directory))!=NULL)
    {
        if(strcmp(struct_directories->d_name,".")!=0 && strcmp(struct_directories->d_name,"..")!=0)
        {
            if(!i)
            {
                head=crear_nodo(struct_directories->d_name);
                head->next=NULL;
            }
            else
            {
                auxiliar=crear_nodo(struct_directories->d_name);
                enlazar(&head,auxiliar);
            }
            i++;
        }
    }
    ordenar(&head);
    
return head;
    
}

nodo* opendirectory(char *path)
{
    DIR *directory;
    nodo *head;
    if((directory=opendir(path))==NULL)
        return NULL;
    else
        head=readdirectory(directory);
    closedir(directory);
    
return head;
    
}

nodo* directories (int sockdup,char *path)
{
    char opendir_error[]="No se ha podido abrir el directorio";
    nodo *head;
    
    if((head=opendirectory(path))==NULL)
    {
        write(sockdup,opendir_error,100);
        return NULL;
    }
    if(presentar_lista(head,sockdup)==-1)
    {
        perror("presentar_lista");
        return NULL;
    }
    
return head;    
}
