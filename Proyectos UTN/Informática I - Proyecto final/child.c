#include "includes.h"

nodo* list_music (char * songs_path,int sockdup)
{
    char list_music_error[]="No se ha podido abrir el directorio elegido";
    nodo *head;
    
    if((head=opendirectory(songs_path))==NULL)
    {
        write(sockdup,list_music_error,size_readwrite);
        return NULL;
    }
return head;
    
}

int send_music(nodo*songs_head,char *songs_path ,int sockdup)
{
    int fdaudio,readed;
    nodo*aux=songs_head;
    char end_msg[]="Fin transmision";
    char*buffer_cancion=malloc(sizeof(char)*buffsize);
    char music_msg[]="Empiezo transmision musica";
    char *complete_path;
    if(songs_head!=NULL)
    {
        write(sockdup,music_msg,size_readwrite);
    }
    else
    {
        printf("songs_head vacio \n");
        return -1;
        
    }
    while(aux!=NULL)
    {
        complete_path=malloc(sizeof(char)*size_completepath);
        strcpy(complete_path,songs_path);
        strcat(complete_path,"/");
        strcat(complete_path,aux->name_directory);
        if((fdaudio=open(complete_path, O_RDONLY))==-1)
        {
            perror("open fdaudio");
            return -1;
        }
        do
        {
            if((readed=read(fdaudio,buffer_cancion,buffsize))==-1)
            {
                perror("read fdaudio");
                return -1;   
            }
            if(readed>0)
            {
                if(write(sockdup,buffer_cancion,buffsize)==-1)
                {
                    perror("write fdaudio");
                    return -1;
                }
            }
        }while(readed!=0);
        free(complete_path);
        aux=aux->next;
    }
    write(sockdup,end_msg,buffsize);
    free(buffer_cancion);
return 0;
    
}

int child_process (nodo*head,int sockdup,nodo** head_albums,char * artist_selected)
{
    int i,j,opcion;
    char *buffer_child=malloc(size_buffer_child*sizeof(char));
    char *extra,*artist_path;
    nodo *recorrer;
    artist_path=malloc(sizeof(char)*BUFFSIZE);
    strcpy(artist_path,music_path);
    strcat(artist_path,"/");
    if(read(sockdup,buffer_child,size_readwrite)==-1)
    {
        perror("read sockdup");
        return -1;
    }
     extra=malloc(strlen(buffer_child)*sizeof(char));
    for(i=0,j=0;(*(buffer_child+i))!='\0';i++)
    {
        if(isalpha((int)(*(buffer_child+i)))==0)
        {
            *(extra+j)=*(buffer_child+i);
            j++;
        }
    }
    *(extra+j)='\0';
    opcion=atoi(extra);
    if (opcion==-1)
        return 1;
    recorrer=head;
    for(i=0;i<opcion && recorrer!=NULL;i++)
        recorrer=recorrer->next;
    strcpy(artist_selected,recorrer->name_directory);
    strcat(artist_path,recorrer->name_directory);
    if(((*head_albums)=directories(sockdup,artist_path))==NULL)
        return -1;
    free(buffer_child);
    free(artist_path);
    free(extra);
return 0;
    
}
int process_subdirectory(nodo *head_sub, int sockdup,char *artist_selected)
{
    int i,j,opcion,return_send_music;
    char *buffer_child=malloc(size_buffer_child*sizeof(char));
    char *extra,*songs_path;
    nodo *recorrer,*songs_head;
    songs_path=malloc(sizeof(char)*BUFFSIZE);
    strcpy(songs_path,music_path);
    strcat(songs_path,"/");
    strcat(songs_path,artist_selected);
    if(read(sockdup,buffer_child,size_readwrite)==-1)
    {
        perror("read sockdup");
        return -1;
    }
     extra=malloc(strlen(buffer_child)*sizeof(char));
    for(i=0,j=0;(*(buffer_child+i))!='\0';i++)
    {
        if(isalpha((int)(*(buffer_child+i)))==0)
        {
            *(extra+j)=*(buffer_child+i);
            j++;
        }
    }
    *(extra+j)='\0';
    opcion=atoi(extra);
    if (opcion==-1)
        return 1;
    recorrer=head_sub;
    for(i=0;i<opcion && recorrer!=NULL;i++)
        recorrer=recorrer->next;
    strcat(songs_path,"/");
    strcat(songs_path,recorrer->name_directory);
    songs_head=list_music(songs_path,sockdup);
    return_send_music=send_music(songs_head,songs_path,sockdup);
    if(return_send_music==-1)
        return -1;
free(songs_path);
free(buffer_child);
free(extra);
return 0;
    
}
/*int send_image (int sockdup,char *artist_selected)
{
    int i,j,opcion,height,width,step,channels;
    char * complete_path = malloc(sizeof(char)*size_completepath);
    char opendir_error[]="No se ha podido abrir el directorio";
    char *buffer_child=malloc(size_buffer_child*sizeof(char));
    char *extra;
    nodo *head,recorrer;
    IplImage *img=0;
    uchar *data;
    
    strcpy(complete_path,portada_path);
    strcat(complete_path,"/");
    strcat(complete_path,artist_selected);
    free(artist_selected);
    if((head=opendirectory(path))==NULL)
    {
        write(sockdup,opendir_error,size_readwrite);
        return NULL;
    }
    if(read(sockdup,buffer_child,size_readwrite)==-1)
    {
        perror("read sockdup");
        return -1;
    }
     extra=malloc(strlen(buffer_child)*sizeof(char));
    for(i=0,j=0;(*(buffer_child+i))!='\0';i++)
    {
        if(isalpha((int)(*(buffer_child+i)))==0)
        {
            *(extra+j)=*(buffer_child+i);
            j++;
        }
    }
    *(extra+j)='\0';
    opcion=atoi(extra);
    printf("la opcion elegida es: %d \n",opcion);
    if (opcion==-1)
        return 1;
    recorrer=head;
    for(i=0;i<opcion && recorrer!=NULL;i++)
        recorrer=recorrer->next;
    strcat(complete_path,recorrer->name_directory);
    img=cvLoadImage(complete_path);
    if(!img)
        return -1;
    height= img->height;
    width= img->width;
    step= img->widthStep;
    channels= img->nChannels;
    data= (uchar*)img->imageData;
    send(sockdup,data,width*height*channels,0);
    cvReleaseImage(&img);
return 0;
    
}
*/
