#include "sock-lib.h"
//#include <cv.h>
//#include <highgui.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/soundcard.h>

#define BUFFSIZE 10000 //para archivo de configuracion
#define music_path "./musica"
#define portadas_path "./portadas"
#define size_readwrite 100 
#define size_buffer_child 100
#define size_completepath 500
/* segundos de audio a grabar */
#define MSEG 10
/* sampling rate = velocidad de muestreo del audio a la entrada*/
#define RATE 48000
/* sample size = Tamaño de muestra. Típicamente 8 o 16 bits */
#define SIZE 16
/* 1 = mono 2 = stereo */
#define CHANNELS 2
/*Calcula dinámicamente el tamaño del buffer de audio*/
#define buffsize MSEG*RATE*SIZE*CHANNELS/8/1000  //tamaño para enviar y recibir musica

typedef struct nodo
{
    char *name_directory;
    struct nodo *next;
} nodo;


int contadormaxfd(int a, int b);    //para encontrar el file descriptor mas grande 

nodo* crear_nodo(char*);     //crea un nodo y lo inicializa

void enlazar (nodo **,nodo *);  //enlaza un nodo a la lista

void ordenar (nodo **head);     //ordena la lista previamente armada por orden alfabetico (emplea intercambiar_dato)

void intercambiar_dato (nodo*recorrer,nodo*comparar); // compara los datos y los va intercambiando

int presentar_lista (nodo*,int);    //envia la lista a traves del sockdup

int borrar_lista(nodo **head);     //devuelve -1 si la lista estaba vacia

void set_audio_params (int fd);     //setea los parametros de audio

nodo* readdirectory (DIR *);        //emplea las funciones de listas para armar la lista con los directorios y ordenarlos alfabeticamente

nodo* opendirectory(char *);        //abre el directorio y llama a la funcion readdirectory (si falla imprime mensajes de errores)

nodo* directories (int,char*);      //llama a las funcion opendirectory;si falla imprime mensajes de errores,sino emplea presentar_lista 

nodo* list_music (char *,int);      //hace una lista con las canciones del album seleccionado

int send_music(nodo*,char*,int);    //envia la musica por socket 

int child_process (nodo*,int,nodo **,char*);//se encarga de encontrar el artista elegido por el cliente y enviar los albumes de dicho artista (retorna un puntero a la lista con los albumes)

int process_subdirectory(nodo *, int,char *);   //se encarga de encontrar el album elegido y emplea a list_music y send_music para enviar dicho album al cliente

void my_sigchild_h (int);   //handler de SIGCHLD donde empleo waitpid para que no queden hijos zombies

void my_sighup(int);        //handler de SIGHUP donde empleo config_load (es decir, si se quiere actualizar MAXCHILDS sin cerrar el server se envia dicha señal y se actualizara la cantidad maxima de hijos)

int sig_trap (void); //llama a ambos handler y retorna en el servidor que sucedio un error en caso de haber fallado alguno de ellos

void config_load(char *); //se encarga de tomar del archivo servidor.conf la cantidad maxima de hijos a emplear en el server

//int send_image (int,char *);
//int load_image(int);
