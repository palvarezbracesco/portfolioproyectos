Proyecto en lenguaje c en el cual se crea un servidor y un/unos clientes en el cual el objetivo es crear un simil "Spotify" donde se solicita al
servidor la lista de temas y se elije el que se desea reproducir.
Por cuestiones de tamaño de las canciones (que debían estar en formato crudo) es que las canciones de prueba no se encuentran,
además de que es un proyecto que se realizo hace varios años.
Este proyecto fue realizado en lenguaje c.
Este trabajo final me permitió aprender/afianzar los siguientes conceptos/herramientas:
	- sockets
	- memoria dinámica
	- empleo de array del argumento del main
	- fork para la creación de procesos hijo
	- utilización de señales
	- listas
	- files
Este proyecto fue un trabajo práctico final de la materia realizado de forma individual