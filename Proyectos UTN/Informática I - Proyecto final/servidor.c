#include "includes.h"

int nchilds=0;
int MAXCHILDS;

char * argv1_global;

int main (int argc, char*argv[])
{
    int maxfd = 0,first_use=1,return_child,return_sub,sockfd,sockdup;
    fd_set rfds,rfdsAux;
    pid_t pid;
    char err_msg[]="Server bussy";
    char * artist_selected=malloc(sizeof(char)*size_completepath);
    struct sockaddr_in my_addr; //contendra la direccion IP y el numero del puerto
    nodo *head_artists,*head_sub;
    //int return_image;
    if((sockfd=Open_conection (&my_addr))==-1)
    {
        perror("open conection");
        exit(1);
    }
    if(argc>1)
    {
        argv1_global=malloc(sizeof(char)*(strlen(argv[1])+1));
        argv1_global=(char*)strcpy(argv1_global,argv[1]);
        config_load(argv1_global);
        if(sig_trap()==-1)
        {
            perror("sig_trap");
            exit(1);
        }
        while(1)
        {
            sockdup=Aceptar_pedidos(sockfd);
            if(nchilds<MAXCHILDS)
            {
                pid=fork();
                if(pid==-1) //error fork
                {
                    perror("fork");
                    exit(1);
                }
                if(!pid) //proceso hijo
                {
                    close(sockfd);
                    FD_ZERO(&rfds);
                    FD_SET(0, &rfds);
                    FD_SET(sockdup, &rfds);
                    rfdsAux = rfds;
                    if((head_artists=directories(sockdup,music_path))==NULL)
                    {       
                        perror("music directory");
                        exit(1);
                    }
                    while(1)
                    {
                        rfds = rfdsAux;
                        maxfd=contadormaxfd(sockdup,0);
                        if (select(maxfd+1, &rfds, NULL, NULL, NULL)==-1)
                        {
                            printf("select()\n");
                            exit(1);
                        }
                        else
                        {
                            if(FD_ISSET(0,&rfds))
                            {   
	
                            }
                            if(FD_ISSET(sockdup,&rfds))
                            {
                                if(first_use)
                                {
                                    return_child=child_process(head_artists,sockdup,&head_sub,artist_selected);
                                    if(return_child==-1)
                                    {
                                        perror("child_process");
                                        exit(1);
                                    }
                                    if(return_child==1)
                                    {
                                        borrar_lista(&head_artists);
                                        exit(1);
                                    }
                                    first_use=0;
                                }
                                else
                                {
                                    //if((return_image=send_image(sockdup,artist_selected))==-1)
                                    //{
                                      //  perror("send_image:");
                                       // exit(1);
                                    //}
                                    
                                    if((return_sub=process_subdirectory(head_sub,sockdup,artist_selected))==-1)
                                    {
                                        perror("process_subdirectory");
                                        exit(1);
                                    }
                                    if(return_sub==1)
                                    {
                                        borrar_lista(&head_artists);
                                        exit(1);
                                    }
                                }
                            }                                                   // cierro llave FD_ISSET (sockdup,&rfds)
                        }                                                       //cierro llave del else (cuando select no fallo)
                    
                    }                                                           //cierro llave while(1)
                }                                                               //cierro llave if(!pid)
                else //proceso padre
                {
                    close(sockdup);
                    nchilds++;
                }
                    
            }                                                                   // llave if(nchilds<MAXCHILDS)
            else
            {
                if(write(sockfd,err_msg,(strlen(err_msg)+1)*sizeof(char))==-1)
                {
                    perror("write err_msg");
                    exit(1);
                }
                close(sockdup);
            }
        }
        
    }
    else
    {
        printf("No ha ingresado archivo de configuracion\n");
        exit(1);
    }
    
return 0;
    
}
