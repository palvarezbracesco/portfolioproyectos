#include "includes.h"

int contadormaxfd(int a, int b)
{
  if (a<b)
    return b;
  return a;
}
struct nodo* crear_nodo(char * d_name)
{
    nodo *p=malloc(sizeof(nodo));
    p->name_directory=malloc((strlen(d_name)+1));
    p->name_directory=memcpy(p->name_directory,d_name,(strlen(d_name)+1));
    return p;
    
}
void enlazar(nodo **head,nodo *agregar)
{
    nodo *recorrer=*head;
    while(recorrer->next!=NULL)
        recorrer=recorrer->next;
    recorrer->next=agregar;
    agregar->next=NULL;
    return;
}


int borrar_lista(nodo **head){
    int i=0;
    nodo *extra=NULL,*extra2=NULL;
    if(*head==NULL)
      return -1;
    else{
      extra=*head;
      do{
	extra2=extra;
	extra=extra->next;
	free(extra2);
	i++;
      }while(extra!=NULL);
    }
free(*head);
*head=NULL;
return 0;
    
}
/*int load_image(int sockfd)
{
    int height,width,step,channels;
    IplImage *img=0;
    uchar *data;
    
    img=cvCreateImage(cvSize(320, 240), 8, 3);
    if(!img)
        return -1;
    height= img->height;
    width= img->width;
    step= img->widthStep;
    channels= img->nChannels;
    data= (uchar*)img->imageData;
    recv(sockfd,img->imageData,width*height*channels,0);
    
    cvNamedWindow("Portada album",CV_WINDOW_AUTOSIZE);
  //Movemos la ventana al pixel 100 de la fila 100
  cvMoveWindow("Portada album",100,100);
  //Mostramos la imagen leída en la ventana creada
  cvShowImage("Portada album", img);
  //Esperamos que el usuario presione cualquier tecla...
  cvWaitKey(0);
  cvReleaseImage(&img);
    return 0;
}

*/

void set_audio_params (int fd)
{
    int arg;
    int	status;
    arg = SIZE;
    status = ioctl(fd, SOUND_PCM_WRITE_BITS, &arg); 
    if (status == -1) 
        perror("Error con comando SOUND_PCM_WRITE_BITS");
    if (arg != SIZE)
        fprintf (stderr,"Tamaño de muestras no soportado. Se programó %d\n",arg);
    
    arg = CHANNELS;  /* mono o stereo */
    status = ioctl(fd, SOUND_PCM_WRITE_CHANNELS, &arg);
    if (status == -1)
        perror("Error en comando SOUND_PCM_WRITE_CHANNELS");
    if (arg != CHANNELS)
        fprintf (stderr,"Cantidad de canales no soportado. Se programó %d\n",arg);
    
    arg = RATE;	   /* Velocidad de Muestreo */
    status = ioctl(fd, SOUND_PCM_WRITE_RATE, &arg);
    if (status == -1)
        perror("Error en comando SOUND_PCM_WRITE_RATE");
    if (arg != RATE)
        fprintf (stderr,"Velocidad de muestreo no soportada. Se programó %d\n",arg);

return;
}

