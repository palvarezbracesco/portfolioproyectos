#include "includes.h"
extern int nchilds;
extern char *argv1_global;


int sig_trap (void)
{
    if((signal(SIGCHLD,my_sigchild_h))==SIG_ERR)
    {
        perror("signal: ");
        return -1;
    } 

    if((signal(SIGHUP,my_sighup))==SIG_ERR)
    {
        perror("signal: ");
        return -1;
    }
    
    else
        return 0;
       
}      
void my_sigchild_h (int sig)
{
    while(waitpid(-1,NULL,WNOHANG)>0) //WNOHANG: no te cuelgues
        nchilds--;
    return;      
}
void my_sighup (int signal) //si enviamos la señal sighup se vuelve a cargar la cantidad maxima de childs
{
    
    config_load(argv1_global);
    return;
}
