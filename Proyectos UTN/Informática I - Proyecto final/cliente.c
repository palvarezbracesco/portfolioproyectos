#include "includes.h"

int main (int argc,char*argv[])
{
    int maxfd = 0,selectRetval=0,opcion_cont,flag_transmision,readed,opcion_int,flag_bienvenida=1;
    fd_set rfds,rfdsAux;
    char *buffer=malloc(sizeof(char)*size_readwrite); //buffer para lectura de opciones o errores
    char *buffer_cancion=malloc(sizeof(char)*buffsize); //buffer para cuando recibimos la musica
    char opcion[100]={}; //aca guardamos la opcion elegida
    int sockfd,fd_dev,i,status;
    
    char opendir_error[]="No se ha podido abrir el directorio";
    char start_msg[]="Empiezo transmision artistas";
    char music_msg[]="Empiezo transmision musica";
    char end_msg[]="Fin transmision";
    char list_music_error[]="No se ha podido abrir el directorio elegido";
    char nombre_dir[100];
    nodo * head,*nodo;
    
    if(argc<2)
    {
        fprintf(stderr,"uso: %s hostname [port]\n",argv [0]);
        exit(1);
    }
    if ((fd_dev = open("/dev/dsp", O_RDWR))<0)
    { 
        fprintf(stderr,"Error en función open de /dev/dsp, Código de error: %s\n",strerror (fd_dev)); 
        exit(1);
    }
    set_audio_params (fd_dev);
    sockfd=conectar(argc,argv);
    FD_ZERO(&rfds);
    FD_SET(0, &rfds); //Recordar que el 0 es stdin (teclado por default)
    FD_SET(sockfd, &rfds);
    rfdsAux = rfds;
    
    while(1)
    {
        rfds = rfdsAux;
        maxfd=contadormaxfd(sockfd,0);
        selectRetval = select(maxfd+1, &rfds, NULL, NULL, NULL);
        if (-1 == selectRetval)
        {
            perror("select() :");
            exit(1);
        }
        else
        {
            if(FD_ISSET(0,&rfds))
            {
                scanf("%[^\n]",opcion);  //espera escritura hasta el enter
                getchar(); //para que no se trabe
                opcion_int=atoi(opcion);
                
                if(opcion_int<-1 || opcion_int>opcion_cont)
                {
                    printf("---Opcion ingresada incorrecta---\n");
                    printf("\n>>>>>>>> Cerrando conexion <<<<<<<<<\n");
                    free(buffer);
                    free(buffer_cancion);
                    close(sockfd);
                    exit(1);
                }
                if(opcion_int==-1)
                {
                    printf("\n>>>>>>>> Cerrando conexion <<<<<<<<<\n");
                    free(buffer);
                    free(buffer_cancion);
                    close(sockfd);
                    exit(1);
                }
                nodo=head;
                for(i=0;i<opcion_int;i++)
                    nodo=nodo->next;
                printf("<<<<< Transmitiendo pedido de %s >>>>>\n",nodo->name_directory); 			
                write(sockfd,opcion,size_readwrite);
                        
            }
            if(FD_ISSET(sockfd,&rfds))
            {
                readed=read(sockfd,buffer,size_readwrite);
                if(readed==-1)
                {
                    printf("<<<<<< Ha surgido un error, disculpe las molestias >>>>>>>>\n");
                    free(buffer);
                    free(buffer_cancion);
                    close(sockfd);
                    exit(1);
                }
                if(strncmp(buffer,opendir_error,size_readwrite)==0)
                {
                    printf("<<<<<< Ha surgido un error, disculpe las molestias >>>>>>>>\n");
                    free(buffer);
                    free(buffer_cancion);
                    close(sockfd);
                    exit(1);
                }
                if(strncmp(buffer,start_msg,size_readwrite)==0)
                {
                    opcion_cont=0;
                    flag_transmision=1;
                    do
                    {
                        if(read(sockfd,buffer,size_readwrite)==-1)
                        {
                            printf("Ha fallado la lectura del socket\n");
                            printf("<<<<<< Ha surgido un error, disculpe las molestias >>>>>>>>\n");
                            free(buffer);
                            free(buffer_cancion);
                            close(sockfd);
                            exit(1);
                        }
                        if(strncmp(buffer,end_msg,size_readwrite)!=0)
                        {
                            strncpy(nombre_dir,buffer,size_readwrite);
                            if (!opcion_cont)
                            {
                                head=crear_nodo(nombre_dir);
                                head->next=NULL;
                                if(flag_bienvenida)
                                {
                                    printf("\n<<<<<<<<<<<<<<<<<< Bienvenido al proyecto de Alvarez >>>>>>>>>>>>>>>>>>\n");
                                    flag_bienvenida=0;
                                }
                                printf("\n------------Ingrese el numero de la opcion que desea seleccionar------------\n\n");
                            }
                            else
                            {
                                nodo=crear_nodo(nombre_dir);
                                enlazar(&head,nodo);
                            }
                            printf("---%d: %s\n",opcion_cont,nombre_dir);
                            opcion_cont++;
                        }
                        else
                        {
                            opcion_cont--;
                            printf("\n------------------------ -1 to exit ------------------------\n");
                            flag_transmision=0;
                        }
                    }while(flag_transmision);
                }
                if(strncmp(buffer,list_music_error,size_readwrite)==0)
                {
                    printf("Ha fallado list_music\n");
                    printf("<<<<<< Ha surgido un error, disculpe las molestias >>>>>>>>\n");
                    free(buffer);
                    free(buffer_cancion);
                    close(sockfd);
                    exit(1);
                }
                if(strncmp(buffer,music_msg,size_readwrite)==0)
                {
                    //if(load_image(sockfd)==-1)
                    //{
                        //  printf("error load_image\n ");
                        // exit(1);
                    //}
                    printf("\n<<<<<<<<< Reproduciendo pedido >>>>>>>>\n");
                    do
                    {
                        
                        if((readed=read(sockfd,buffer_cancion,buffsize))==-1)
                        {
                            printf("Ha fallado read fdaudio\n");
                            printf("<<<<<< Ha surgido un error, disculpe las molestias >>>>>>>>\n");
                            free(buffer);
                            free(buffer_cancion);
                            close(sockfd);
                            exit(1);   
                            
                        }
                        if(readed>0)
                        {
                            if(write(fd_dev,buffer_cancion,buffsize)==-1)
                            {
                                printf("Ha fallado write fdaudio\n");
                                printf("<<<<<< Ha surgido un error, disculpe las molestias >>>>>>>>\n");
                                free(buffer);
                                free(buffer_cancion);
                                close(sockfd);
                                exit(1);
                                
                            }
                            
                        }
                        
                    }while(strcmp(buffer_cancion,end_msg)!=0);
                    if(strcmp(buffer_cancion,end_msg)==0)
                    {
                        status = ioctl(fd_dev, SOUND_PCM_SYNC, 0);
                        
                        if (status == -1) 
                            perror("Error en comando SOUND_PCM_SYNC");
                        
                        printf("\n>>>>>>>> Fin de transmision <<<<<<<<<\n");
                        printf("\n>>>>>>>> Cerrando conexion <<<<<<<<<\n");
                        close(sockfd);
                        free(buffer);
                        free(buffer_cancion);
                        exit(1);
                    
                    }
                }
                
            } //llave isset sockfd
            
        }// llave else (no fallo select)
        
    }//llave while 
    return 0;
}// llave main
    

