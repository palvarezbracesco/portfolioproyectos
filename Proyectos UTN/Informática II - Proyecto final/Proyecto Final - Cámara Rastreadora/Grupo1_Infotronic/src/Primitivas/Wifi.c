/*
 * Wifi.c
 * Para ESP8266
 *  Created on: 31 oct. 2019
 *      Author: mr_nobody
 */
#include "Wifi.h"
#include "UART.h"
#include <string.h>
#include <stdio.h>

#define SIZE_OK 2
#define WIFI_SSID 		"Costa2.4"
#define WIFI_PASSWORD	"costasuarez"
#define SERVER_PORT	"9000"
#define SERVER_IP	"192.168.1.18"

typedef enum
{
	CONFIG_MODE_CLIENT,
	CONFIG_LENGTH
}WifiModule_state_t;
typedef enum
{
	START,
	CONFIGURATION_MODULE,
	CONNECTING_RED,
	CONNECTING_SERVER,
	CONNECTED,
	RESET,

}wifi_state_t;

static volatile uartn_t uartNum_g;
static volatile wifi_state_t WIFI_INIT_STATE=START;
static volatile WifiModule_state_t WIFI_MODULE_CONFIG_STATE=CONFIG_MODE_CLIENT;
static volatile bool flag_send=true;
static char buffer_read[SIZE_COMUNICATION];
static char buffer_write[SIZE_COMUNICATION];
static uint8_t i=0;

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/
void MdE_WifiModule_Configuration(uartn_t uart_number);

/***********************************************************************************************************************************/
void LimpiarBuffer(char* buffer, int16_t size);
void LimpiarBuffer(char* buffer, int16_t size)
{
	uint16_t i;
	for(i=0;i<size;i++)
	{
		buffer[i]='\0';
	}
}
void Wifi_Init(uartn_t uart_number,ULCR_t ulcr_data,PCLK_Function_t pclk_func)
{
	UART_Init(uart_number,ulcr_data,pclk_func);
}
void MdE_WifiModule_Configuration(uartn_t uart_number)
{
	(void*)uart_number;
//	switch(WIFI_MODULE_CONFIG_STATE)
//	{
//	case CONFIG_MODE_CLIENT:
//		if (flag_send)
//		{
//			Uart_SendString(uart_number,(uint8_t *)"AT+CWMODE_DEF=1\r\n");
//			flag_send=false;
//		}
//		if(Uart_IsByteAvailable(uart_number))
//		{
//			buffer_read[i]=Uart_GetByte(uart_number);
//			i++;
//			if(i>0 && strstr(buffer_read,"OK"))
//			{
//				WIFI_MODULE_CONFIG_STATE=CONFIG_LENGTH;
//				i=0;
//				LimpiarBuffer(buffer_read,SIZE_COMUNICATION);
//				flag_send=true;
//				//LCD_Display("           ",1,0);
//				//LCD_Display("CWMODE OK",1,0);
//			}
//
//		}
//		break;
//	case CONFIG_LENGTH:
//		if (flag_send)
//		{
//		//	sprintf(buffer_write,"AT+CIPSEND=%d\r\n",SIZE_COMUNICATION);
//		sprintf(buffer_write,"AT+CIPMUX=0\r\n");
//		Uart_SendString(uart_number,(uint8_t *)buffer_write);
//		flag_send=false;
//		}
//
//		if(Uart_IsByteAvailable(uart_number))
//		{
//			buffer_read[i]=Uart_GetByte(uart_number);
//			i++;
//			if(strstr(buffer_read,"OK"))
//			{
//				WIFI_INIT_STATE=CONNECTING_RED;
//				i=0;
//				LimpiarBuffer(buffer_read,SIZE_COMUNICATION);
//				flag_send=true;
//				//LCD_Display("           ",1,0);
//				//LCD_Display("CWMODE OK",1,0);
//			}
//
//		}
//		break;
//	}


}

void MdE_Wifi(uartn_t uart_number)
{
	switch (WIFI_INIT_STATE)
	{
	case START:
		if (flag_send==true)
		{
			uartNum_g=uart_number;
			Uart_SendString(uart_number,(uint8_t *)"ATE0\r\n");
			flag_send=false;
		}
		if(Uart_IsByteAvailable(uart_number))
		{
			buffer_read[i]=Uart_GetByte(uart_number);
			i++;
			if(i>0 && strstr(buffer_read,"OK")!=NULL)
			{
				WIFI_INIT_STATE=CONFIGURATION_MODULE;
				i=0;
				flag_send=true;
				LimpiarBuffer(buffer_read,SIZE_COMUNICATION);
				//LCD_Display("           ",1,0);
				//LCD_Display("AT OK",1,0);
			}
		}
		break;
	case CONFIGURATION_MODULE:
		if (flag_send)
				{
					Uart_SendString(uart_number,(uint8_t *)"AT+CWMODE_DEF=1\r\n");
					flag_send=false;
				}
				if(Uart_IsByteAvailable(uart_number))
				{
					buffer_read[i]=Uart_GetByte(uart_number);
					i++;
					if(i>0 && strstr(buffer_read,"OK"))
					{
						WIFI_INIT_STATE=CONNECTING_RED;
						i=0;
						LimpiarBuffer(buffer_read,SIZE_COMUNICATION);
						flag_send=true;
						//LCD_Display("           ",1,0);
						//LCD_Display("CWMODE OK",1,0);
					}

				}
				break;
		break;
	case CONNECTING_RED:
		if (flag_send)
		{
			sprintf(buffer_write, "AT+CWJAP=\"%s\",\"%s\"\r\n",WIFI_SSID,WIFI_PASSWORD);//ver si dejamos sin _DEF
			Uart_SendString(uart_number,(uint8_t*)buffer_write);
			flag_send=false;
		}
		if(Uart_IsByteAvailable(uart_number))
		{
			buffer_read[i]=Uart_GetByte(uart_number);
			i++;
			if(strstr(buffer_read,"IP"))
			{
				WIFI_INIT_STATE=CONNECTING_SERVER;
				i=0;
				LimpiarBuffer(buffer_read,SIZE_COMUNICATION);
				flag_send=true;
				//LCD_Display("           ",1,0);
				//LCD_Display("AT+CWJAP OK",1,0);
			}

		}
		break;
	case CONNECTING_SERVER:
		if (flag_send)
		{
			//sprintf(buffer_write,"AT+CIPSTART=\"TCP\",\"%s\",\"%s\"\r\n",SERVER_IP ,SERVER_PORT);
			sprintf(buffer_write,"AT+CIPSTART=\"TCP\",\"%s\",%s\r\n",SERVER_IP ,SERVER_PORT);
			Uart_SendString(uart_number,(uint8_t *)buffer_write);
			flag_send=false;
		}
		if(Uart_IsByteAvailable(uart_number))
		{
			buffer_read[i]=Uart_GetByte(uart_number);
			i++;
			if(strstr(buffer_read,"OK"))
			{
				WIFI_INIT_STATE=CONNECTED;
				i=0;
				LimpiarBuffer(buffer_read,SIZE_COMUNICATION);
				flag_send=true;
				//LCD_Display("           ",1,0);
				//LCD_Display("AT+CIPSTART OK",1,0);
			}


		}
		break;
	case CONNECTED://AGREGAR PARA VER COMO PASAR DE ACA A RESET
		if (flag_send)
				{
					//sprintf(buffer_write,"AT+CIPSTART=\"TCP\",\"%s\",\"%s\"\r\n",SERVER_IP ,SERVER_PORT);
					sprintf(buffer_write,"AT+CIPSEND=4\r\n");
					Uart_SendString(uart_number,(uint8_t *)buffer_write);
					flag_send=false;
			}
		if(Uart_IsByteAvailable(uart_number))
				{
					buffer_read[i]=Uart_GetByte(uart_number);
					i++;
					if(strstr(buffer_read,"OK"))
					{
						i=0;
						flag_send=true;
						sprintf(buffer_write,"hola\r\n");
						Uart_SendString(uart_number,(uint8_t *)buffer_write);
						//LCD_Display("           ",1,0);
						//LCD_Display("AT+CIPSTART OK",1,0);
					}
				}
			//LCD_Display("           ",1,0);

		//LCD_Display("CONNECTED OK",1,0);
		break;
	case RESET:
		if (flag_send)
		{
			Uart_SendString(uart_number,(uint8_t *)"AT+RST\r\n");
			flag_send=false;
		}
		if(Uart_IsByteAvailable(uart_number))
		{
			buffer_read[i]=Uart_GetByte(uart_number);
			i++;

			if(strstr(buffer_read,"OK"))
			{
				WIFI_INIT_STATE=START;
				WIFI_MODULE_CONFIG_STATE=CONFIG_MODE_CLIENT;
				i=0;
				//LCD_Display("           ",1,0);
				//LCD_Display("AT+RST OK",1,0);
			}
			flag_send=true;

		}
		break;

	}
}
bool Wifi_IsConnected(void)
{
	return (WIFI_INIT_STATE >= CONNECTED);
}
void Wifi_SendMsg(uint8_t const * const str,uint8_t size)
{
	bool ready=false;
	uint8_t read[256],i=0;
	//Uart_SendString(uartNum_g,((uint8_t *)"AT+CIPSEND=%d\r\n",size));
	do
	{
		if(Uart_IsByteAvailable(uartNum_g))
		{
			read[i]=Uart_GetByte(uartNum_g);
			i++;
			if(strstr(buffer_read,"OK"))
			{
				ready=true;
			}
		}
	}
	while(!ready);
	Uart_SendString(uartNum_g, str);
}
void Wifi_GetMsg(uint8_t * buffer)
{
	int i;
	for(i=0;Uart_IsByteAvailable(uartNum_g)==true && i<SIZE_COMUNICATION;i++)
		buffer[i]=Uart_GetByte(uartNum_g);
}
