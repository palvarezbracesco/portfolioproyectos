/*
 * Peripheral_Control.c
 *
 *  Created on: 5 oct. 2019
 *      Author: mr_nobody
 */
#include <stdint.h>
#include "PeripheralControl.h"

void PeripheralControl_SetPowerControl(PCONP_t shift,ON_OFF_t func)//PCONP
{
	if(func==ON)
		(*PCONP)|=(1UL<<shift);
	else //func==OFF
		(*PCONP)&=~(1UL<<shift);
}
ON_OFF_t PeripheralControl_GetPowerControl(PCONP_t shift)
{
	return(((*PCONP) & (1UL<<shift)) ? ON : OFF);
}

void PeripheralControl_SelectClock(PCLK_t pclk, PCLK_Function_t func)
{
	uint32_t sel=pclk/16;
	uint32_t shift=(pclk%16)*2;
	PCLKSEL[sel]&=~(3UL<<shift);
	PCLKSEL[sel]|=(func<<shift);
}
