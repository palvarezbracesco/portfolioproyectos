/*
 * Extern_Interrupt.c
 *
 *  Created on: 3 oct. 2019
 *      Author: mr_nobody
 */

#include "Extern_Interrupt.h"
#include "LPC17xx.h"
#include <stdint.h>

void EXTERN_INTERRUPT_SelectMode(EINT_Type eint,EXT_MODE_Type mode)
{
	if(mode==LEVEL_SENSITIVITY)
	{
		(*LPC_EXTMODE)&=~(1UL<<eint);
	}
	else //EDGE_SENSITIVITY
		(*LPC_EXTMODE)|=(1UL<<eint);

}
void EXTERN_INTERRUPT_SelectPolar(EINT_Type eint,EXT_POLAR_Type polar)
{
	if(polar==EXT_LOW)
	{
		(*LPC_EXTPOLAR)&=~(1UL<<eint);
	}
	else
		(*LPC_EXTPOLAR)|=(1UL<<eint);
}
void EXTERN_INTERRUPT_ClearPending(EINT_Type eint)
{
	(*LPC_EXTINT)|=(1UL<<eint);
}
