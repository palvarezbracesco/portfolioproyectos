/*******************************************************************************************************************************//**
 *
 * @file		Pin.c
 * @brief		Módulo para configuración de un pin de puerto
 * @date		21/05/2016
 * @author		Daniel J. López Amado <dlopezamado@hotmail.com>
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "LPC17xx.h"
#include "Pin.h"

/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/

//#define PINMODE 	(volatile (uint_32_t *)0x4002C040)
//#define PINMODE_OD 	(volatile (uint_32_t *)0x4002C068)
/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/
/**
 * @brief IOCON register block
 */
typedef struct {
	uint32_t RESERVED0[5];
} LPC_IOCON_T;

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBLES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/*--------------------------------------------------------------------------------------------------------------------------------*/

/***********************************************************************************************************************************
 *** IMPLEMENTACION DE FUNCIONES PUBLICAS
 **********************************************************************************************************************************/

/*******************************************************************************************************************************//**
 * @fn			void Pin_SetFunction(uint8_t port, uint8_t pin, PinFunction_e func)
 * @brief		Setea la Función del Pin pasado como parámetro
 * @param[in]	port:	Número de Puerto. Debe ser uno de los siguientes: PORT_0, PORT_1, PORT_2, PORT_3, PORT_4.
 * @param[in]	pin:		Número de Pin. Debe ser uno de los siguientes: Desde PIN_0 hasta PIN_31.
 * @param[in] 	func 		Funcion. Debe ser uno de los tipos definidos en la enumeración PinFunction_e
 * @return 		void
 **********************************************************************************************************************************/
void Pin_SetFunction(uint8_t port, uint8_t pin, PinFunction_e func)
{
	port = port*2+pin/16;
	pin = (pin%16)*2;
	LPC_PINSEL[port] &= ~(3UL << pin);//limpia escribiendo 0 en los dos bits correspondientes a dicho pin
	LPC_PINSEL[port] |= func << pin; //escribe la funcion que debe cumplir el pin
}

/*******************************************************************************************************************************//**
 * @fn			void Pin_SetResistorMode ( uint8_t port, uint8_t pin, PinResistorMode_e resMode)
 * @brief		Configura la resistencia asociada a un pin de un puerto
 * @param[in]	port:	Número de Puerto. Debe ser uno de los siguientes: PORT_0, PORT_1, PORT_2, PORT_3, PORT_4.
 * @param[in]	pin:		Número de Pin. Debe ser uno de los siguientes: Desde PIN_0 hasta PIN_31.
 * @param[in] 	resMode		Modo de la Resistencia: Debe ser uno de los tipos definidos en la enumeración PinResistorMode_e
 * @return 		void
 **********************************************************************************************************************************/
void Pin_SetResistorMode(uint8_t port, uint8_t pin, PinResistorMode_e resMode)
{
	port = port*2+pin/16;
	pin = (pin%16)*2;
	LPC_PINMODE[port] &= ~(3UL << pin);//limpia escribiendo 0 en los dos bits correspondientes a dicho pin
	LPC_PINMODE[port] |= resMode<< pin; //escribe la funcion que debe cumplir el pin
}

/*******************************************************************************************************************************//**
 * @fn			void Pin_SetOpenDrainMode(uint8_t port, uint8_t pin, PinOpenDrainMode_e odMode)
 * @brief		Configura el modo de la salida de un pin de un puerto
 * @param[in]	port:	Número de Puerto. Debe ser uno de los siguientes: PORT_0, PORT_1, PORT_2, PORT_3, PORT_4.
 * @param[in]	pin:		Número de Pin. Debe ser uno de los siguientes: Desde PIN_0 hasta PIN_31.
 * @param[in] 	odMode 	    Modo de la Salida: Debe ser uno de los tipos definidos en la enumeración PinOpenDrainMode_e
 * @return 		void
 **********************************************************************************************************************************/
void Pin_SetOpenDrainMode(uint8_t port, uint8_t pin, PinOpenDrainMode_e odMode)
{
	if(odMode==OPEN_DRAIN)
	{
		LPC_PINMODE_OD[port]|=(1UL<<pin);
	}
	else //NOT_OPEN_DRAIN
	{
		LPC_PINMODE_OD[port]&=~(1UL<<pin);
	}
}

/***********************************************************************************************************************************
 *** IMPLEMNTACION DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/*--------------------------------------------------------------------------------------------------------------------------------*/

