/*
 * nvic.c
 *
 *  Created on: 25 sep. 2019
 *      Author: mr_nobody
 */
#include "nvic.h"
#include "LPC17xx.h"


void NVIC_EnableIRQ(IRQn_Type IRQn)
{
	uint8_t numberIser=IRQn/32;
	uint32_t shift=IRQn%32;
	ISER[numberIser]|=(1UL<<shift);
}
void NVIC_SetPriority(IRQn_Type IRQn, uint32_t priority)
{
	uint8_t numberIpr=IRQn/4;
	uint32_t pin=3,interrupt_number=IRQn%4;
	if(interrupt_number>0)
	{
		pin+=JUMP_IPR*interrupt_number;
	}
	IPR[numberIpr]=(priority & 0xff);
}
void NVIC_DisableIRQ(IRQn_Type IRQn)
{
	uint8_t numberIser=IRQn/32;
	uint32_t shift=IRQn%32;
	ICER[numberIser]|=(1UL<<shift);
}

