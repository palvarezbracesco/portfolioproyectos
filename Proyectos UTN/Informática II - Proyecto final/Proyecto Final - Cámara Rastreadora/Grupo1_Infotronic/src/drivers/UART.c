/*
 * UART.c
 *
 *  Created on: 5 oct. 2019
 *      Author: mr_nobody
 */
#include "UART.h"
#include "LPC17xx.h"
#include "PeripheralControl.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "Pin.h"
#include "nvic.h"
#include <string.h>


// Flags para indicar comunicacion en curso
static volatile bool Uart_Sending[4]={false,false,false,false};



#define IIR_RBR			0x02		// Identificador de dato disponible
#define IIR_THR 		0x01		// Identificador de registro de transmision vacio
#define SIZE	256		//

typedef struct
{
	volatile uint8_t in;
	volatile uint8_t out;
	volatile uint16_t count;
	volatile uint8_t buffer[SIZE];
}RingBuffer_s;

// Buffers de rx/tx de la UART_n

static RingBuffer_s Uart0Rx;
static RingBuffer_s Uart0Tx;

static RingBuffer_s Uart1Rx;
static RingBuffer_s Uart1Tx;

static RingBuffer_s Uart2Rx;
static RingBuffer_s Uart2Tx;

static RingBuffer_s Uart3Rx;
static RingBuffer_s Uart3Tx;


/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/
bool IsEmpty(RingBuffer_s const * const rb);
uint8_t Pop(RingBuffer_s * const rb);
void Push(RingBuffer_s * const rb, uint8_t data);

void UART_Init(uartn_t uart_number,ULCR_t ulcr_data,PCLK_Function_t pclk_func)
{
	volatile UARTn_t* uart_register;
	PCONP_t shift;
	PCLK_t pclk_select;
	uint8_t port,pin_tx,pin_rx;
	PinFunction_e func;
	IRQn_Type irq_number;

	if (uart_number<=UART_1)
	{
		shift=uart_number+3;//PCUART0=3,PCUART1=4
	}
	else
		shift=uart_number+22;//PCUART2=24,PCUART3=25 revisar si esta bien
	pclk_select=shift; //coinciden numericamente
	PeripheralControl_SetPowerControl(shift,ON);//PCONP
	PeripheralControl_SelectClock(pclk_select, pclk_func);


		switch(uart_number)
		{
		case 0:
			uart_register=UART0;
			port = PORT_0;
			pin_tx = PIN_2;
			pin_rx = PIN_3;
			func =FIRST_ALT_FUNC;
			//Para habilitar interrupcion (NVIC)
			irq_number=UART0_IRQn;
			break;
		case 1:
			uart_register=UART1;
			port = PORT_0;
			pin_tx = PIN_15;
			pin_rx = PIN_16;
			func = FIRST_ALT_FUNC;
			//Para habilitar interrupcion (NVIC)
			irq_number=UART1_IRQn;
			break;
		case 2:
			uart_register=UART2;
			port = PORT_0;
			pin_tx = PIN_10;
			pin_rx = PIN_11;
			func =FIRST_ALT_FUNC;
			//Para habilitar interrupcion (NVIC)
			irq_number=UART2_IRQn;
			break;
		case 3:
			uart_register=UART3;
			port = PORT_0;
			pin_tx = PIN_0;
			pin_rx = PIN_1;
			func = SECOND_ALT_FUNC;
			//Para habilitar interrupcion (NVIC)
			irq_number=UART3_IRQn;
			break;
		}

		uart_register->LCR &=~(3UL<<0); // Limpio bits (abarca bit 0 y bit 1)
		uart_register->LCR |=(ulcr_data.wordLength<<0); // Escribo bits (abarca bit 0 y bit 1)

		if(!ulcr_data.stopBit)
			uart_register->LCR &=~(1UL<<2);
		else
			uart_register->LCR |=(ulcr_data.stopBit<<2);

		if(!ulcr_data.parityEnable)
			uart_register->LCR &=~(1UL<<3);
		else
			uart_register->LCR |=(ulcr_data.parityEnable<<3);
		if(ulcr_data.parityEnable==ON)
		{
			uart_register->LCR &=~(3UL<<4); //Limpio bits (abarca bit 4 y bit 5)
			uart_register->LCR |=(ulcr_data.paritySelect<<4); // Escribo bits (abarca bit 4 y bit 5)
		}
		if(!ulcr_data.breakControl)
			uart_register->LCR &=~(1UL<<6);
		else
			uart_register->LCR |=(ulcr_data.breakControl<<6);

		//Para configurar Baudrate en 9600
		uart_register->LCR |=(1UL<<7); //Escribo un 1 en DLAB para configurar Baudrate
		uart_register->DLM= 0; //DLM
		uart_register->DLL= 73; //DLL
		uart_register->FDR |= ( 1 << 0); //preguntar
		uart_register->FDR |= ( 9 << 4);//preguntar
		uart_register->LCR &= ~(1UL<<7); //Escribo un 0 en DLAB

		Pin_SetFunction(port, pin_tx,func);
		Pin_SetFunction(port, pin_rx,func);

		NVIC_EnableIRQ(irq_number);
		uart_register->IER=(1UL << 0) | (1UL << 1); //Preguntar


}
void UART0_IRQHandler(void)
{
	uint32_t iir;	// Se utiliza para la lectura del regitro IIR (que se limpia al leerlo)
	uint32_t intId;	// Se utiliza para identificar la causa de la interrupcion
	uint8_t data;	// Se utiliza para la lectura/escritura de los datos recividos/transmitidos

	do
	{
		iir = (UART0)->IIR;
		intId = ((iir >> 1UL) & 7UL);	// Obtiene el "Interrupt identification", que son los bits 3:1 del registro IIR

		switch (intId)
		{
			case IIR_RBR:	// Se recibio un dato y esta disponible para ser leido
				data = (UART0)->RBR;		// Lee el dato recibido
				Push(&Uart0Rx, data);			// Guarda el dato en el buffer circular
				break;

			case IIR_THR:	// Se finalizo de transmitir un dato
				if (IsEmpty(&Uart0Tx) == false)
				{
					(UART0)->THR = Pop(&Uart0Tx);
				}
				else
				{
					Uart_Sending[UART_0] = false;
				}
				break;
		}

	} while(!(iir & 1UL));	// Si el bit 0 es igual a 0, entonces al menos queda una interrupcion pendiente
}


void UART1_IRQHandler(void)
{
	uint32_t iir;	// Se utiliza para la lectura del regitro IIR (que se limpia al leerlo)
	uint32_t intId;	// Se utiliza para identificar la causa de la interrupcion
	uint8_t data;	// Se utiliza para la lectura/escritura de los datos recividos/transmitidos

	do
	{
		iir = (UART1)->IIR;
		intId = ((iir >> 1UL) & 7UL);	// Obtiene el "Interrupt identification", que son los bits 3:1 del registro IIR

		switch (intId)
		{
			case IIR_RBR:	// Se recibio un dato y esta disponible para ser leido
				data = (UART1)->RBR;		// Lee el dato recibido
				Push(&Uart1Rx, data);			// Guarda el dato en el buffer circular
				break;

			case IIR_THR:	// Se finalizo de transmitir un dato
				if (IsEmpty(&Uart1Tx) == false)
				{
					(UART1)->THR = Pop(&Uart1Tx);
				}
				else
				{
					Uart_Sending[UART_1]= false;
				}
				break;
		}

	} while(!(iir & 1UL));	// Si el bit 0 es igual a 0, entonces al menos queda una interrupcion pendiente
}

void UART2_IRQHandler(void)
{
	uint32_t iir;	// Se utiliza para la lectura del regitro IIR (que se limpia al leerlo)
	uint32_t intId;	// Se utiliza para identificar la causa de la interrupcion
	uint8_t data;	// Se utiliza para la lectura/escritura de los datos recividos/transmitidos

	do
	{
		iir = (UART2)->IIR;
		intId = ((iir >> 1UL) & 7UL);	// Obtiene el "Interrupt identification", que son los bits 3:1 del registro IIR

		switch (intId)
		{
			case IIR_RBR:	// Se recibio un dato y esta disponible para ser leido
				data = (UART2)->RBR;		// Lee el dato recibido
				Push(&Uart2Rx, data);			// Guarda el dato en el buffer circular
				break;

			case IIR_THR:	// Se finalizo de transmitir un dato
				if (IsEmpty(&Uart2Tx) == false)
				{
					(UART2)->THR = Pop(&Uart2Tx);
				}
				else
				{
					Uart_Sending[UART_2]= false;
				}
				break;
		}

	} while(!(iir & 1UL));	// Si el bit 0 es igual a 0, entonces al menos queda una interrupcion pendiente
}
void UART3_IRQHandler(void)
{
	uint32_t iir;	// Se utiliza para la lectura del regitro IIR (que se limpia al leerlo)
	uint32_t intId;	// Se utiliza para identificar la causa de la interrupcion
	uint8_t data;	// Se utiliza para la lectura/escritura de los datos recividos/transmitidos

	do
	{
		iir = (UART3)->IIR;
		intId = ((iir >> 1UL) & 7UL);	// Obtiene el "Interrupt identification", que son los bits 3:1 del registro IIR

		switch (intId)
		{
			case IIR_RBR:	// Se recibio un dato y esta disponible para ser leido
				data = (UART3)->RBR;		// Lee el dato recibido
				Push(&Uart3Rx, data);			// Guarda el dato en el buffer circular
				break;

			case IIR_THR:	// Se finalizo de transmitir un dato
				if (IsEmpty(&Uart3Tx) == false)
				{
					(UART3)->THR = Pop(&Uart3Tx);
				}
				else
				{
					Uart_Sending[UART_3]= false;
				}
				break;
		}

	} while(!(iir & 1UL));	// Si el bit 0 es igual a 0, entonces al menos queda una interrupcion pendiente
}

void Push(RingBuffer_s * const rb, uint8_t data)
{
	if (rb->count < SIZE)
	{
		rb->buffer[rb->in] = data;
		rb->in++;
		rb->count++;
	}
}

uint8_t Pop(RingBuffer_s * const rb)
{
	uint8_t data = 0;

	if (rb->count > 0)
	{
		data = rb->buffer[rb->out];
		rb->out++;
		rb->count--;
	}

	return data;
}

bool IsEmpty(RingBuffer_s const * const rb)
{
	return ((rb->count == 0) ? true : false);
}

// Envio de datos

void Uart_SendByte(uartn_t portNum, uint8_t data)
{
	RingBuffer_s*uart_tx;
	volatile UARTn_t* uart_register;
	switch(portNum)
	{
		case UART_0:
			uart_register=UART0;
			uart_tx=&Uart0Tx;
			break;
		case UART_1:
			uart_register=UART1;
			uart_tx=&Uart1Tx;
			break;
		case UART_2:
			uart_register=UART2;
			uart_tx=&Uart2Tx;
			break;
		case UART_3:
			uart_register=UART3;
			uart_tx=&Uart3Tx;
			break;
	}
	uart_register->IER &= ~(1UL << 1);	// Deshabilito interrupcion por transmision
	if (Uart_Sending[portNum] == true)
	{
		Push(uart_tx, data);
	}
	else
	{
		uart_register->THR =data;
		Uart_Sending[portNum] = true;
	}

	uart_register->IER |= (1UL << 1);


}

void Uart_Send(uartn_t portNum, const uint8_t *bufferPtr, uint32_t length)
{
	for (uint32_t i = 0; i < length; i++)
	{
		Uart_SendByte(portNum, bufferPtr[i]);
	}

}

void Uart_SendString(uartn_t portNum, uint8_t const * const str)
{
	uint8_t const *data = str;
	uint8_t i=0;
	while (data[i] != '\0')
	{
		Uart_SendByte(portNum, data[i]);
		i++;
	}
}


// Recepcion de datos
bool Uart_IsByteAvailable(uartn_t portNum)
{
	bool isByteAvailable = false;
	RingBuffer_s*uart_rx;
	switch(portNum)
	{
	case UART_0:
		uart_rx=&Uart0Rx;
		break;
	case UART_1:
		uart_rx=&Uart1Rx;
		break;
	case UART_2:
		uart_rx=&Uart2Rx;
		break;
	case UART_3:
		uart_rx=&Uart3Rx;
		break;
	}
	isByteAvailable = !IsEmpty(uart_rx);
	return isByteAvailable;
}

uint8_t Uart_GetByte(uartn_t portNum)
{
	uint8_t data = 0;
	RingBuffer_s*uart_rx;
	switch(portNum)
	{
	case UART_0:
		uart_rx=&Uart0Rx;
		break;
	case UART_1:
		uart_rx=&Uart1Rx;
		break;
	case UART_2:
		uart_rx=&Uart2Rx;
		break;
	case UART_3:
		uart_rx=&Uart3Rx;
		break;
	}
	data=Pop(uart_rx);
	return data;
}

