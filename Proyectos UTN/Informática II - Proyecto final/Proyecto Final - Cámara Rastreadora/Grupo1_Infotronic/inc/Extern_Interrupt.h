/*
 * Extern_Interrupt.h
 *
 *  Created on: 3 oct. 2019
 *      Author: mr_nobody
 */

#ifndef EXTERN_INTERRUPT_H_
#define EXTERN_INTERRUPT_H_


typedef enum
{
	EINT0=0,
	EINT1,
	EINT2,
	EINT3
}EINT_Type;

typedef enum
{
	LEVEL_SENSITIVITY=0,
	EDGE_SENSITIVITY

}EXT_MODE_Type;

typedef enum EXT_POLAR
{
	EXT_LOW=0,
	EXT_HIGH

}EXT_POLAR_Type;

void EXTERN_INTERRUPT_SelectMode(EINT_Type eint,EXT_MODE_Type mode);
void EXTERN_INTERRUPT_SelectPolar(EINT_Type eint,EXT_POLAR_Type polar);
void EXTERN_INTERRUPT_ClearPending(EINT_Type eint);




#endif /* EXTERN_INTERRUPT_H_ */
