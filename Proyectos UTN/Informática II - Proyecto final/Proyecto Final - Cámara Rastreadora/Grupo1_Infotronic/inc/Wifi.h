/*
 * Wifi.h
 *
 *  Created on: 31 oct. 2019
 *      Author: mr_nobody
 */

#ifndef WIFI_H_
#define WIFI_H_

#include "LPC17xx.h"
#include "UART.h"
#include <stdbool.h>
#include <stdint.h>

#define SIZE_COMUNICATION 500 //esto es la cantidad de bytes de comunicacion, NO SE EN CUANTO NOS CONVIENE DEFINIR

void Wifi_Init(uartn_t uart_number,ULCR_t ulcr_data,PCLK_Function_t pclk_func);
void MdE_Wifi(uartn_t uart_number);
bool Wifi_IsConnected(void);
void Wifi_SendMsg(uint8_t const * const str,uint8_t size);
void Wifi_GetMsg(uint8_t *);
bool Wifi_IsMsgAvailable();

#endif /* WIFI_H_ */
