/*
===============================================================================
 Name        : Uart.h
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

#ifndef UART_H_
#define UART_H_

#include "LPC1769.h"
#include "PeripheralControl.h"
#include <stdbool.h>

typedef enum
{
	UART_0,
	UART_1,
	UART_2,
	UART_3
}uartn_t;

typedef enum
{
	Length_5Bit=0x00,
	Length_6Bit=0x01,
	Length_7Bit=0x02,
	Length_8Bit=0x03
}wordLenght_t;

typedef enum
{
	OddParity,
	EvenParity,
	Forced_0,
	Forced_1,
	OFF_paritySelect
}parity_t;

typedef struct
{
	wordLenght_t wordLength;
	ON_OFF_t stopBit;
	ON_OFF_t parityEnable;
	parity_t paritySelect;
	ON_OFF_t breakControl;
}ULCR_t;

//--------- Baudrate fijo a 9600 ---------
void UART_Init(uartn_t uart_number,ULCR_t ulcr_data,PCLK_Function_t pclk_func);
void UART0_IRQHandler(void);
void UART1_IRQHandler(void);
void UART2_IRQHandler(void);
void UART3_IRQHandler(void);
void Uart_SendByte(uartn_t portNum, uint8_t data);
void Uart_Send(uartn_t portNum, const uint8_t *bufferPtr, uint32_t length);
void Uart_SendString(uartn_t portNum, char const * const str);
bool Uart_IsByteAvailable(uartn_t portNum);
uint8_t Uart_GetByte(uartn_t portNum);

//void UART_Baudrate(baudrate_t);


#endif /* UART_H_ */
