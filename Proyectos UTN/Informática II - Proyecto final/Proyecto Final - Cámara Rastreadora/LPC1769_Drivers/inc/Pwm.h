/*
===============================================================================
 Name        : Pwm.h
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

#include "LPC1769.h"

#define SBIT_CNTEN     0
#define SBIT_PWMEN     3

#define SBIT_PWMMR0R   1
#define SBIT_PWMMR1R   2

#define SBIT_LEN0      0
#define SBIT_LEN1      1
#define SBIT_LEN2      2
#define SBIT_LEN3      3
#define SBIT_LEN4      4
#define SBIT_LEN5      5
#define SBIT_LEN6      6

#define SBIT_PWMENA1   9
#define SBIT_PWMENA2   10
#define SBIT_PWMENA3   11
#define SBIT_PWMENA4   12


void PWM_Init(void);

typedef enum {
	PWM_0 = 0,
	PWM_1 = 1,
	PWM_2,
	PWM_3,
	PWM_4,
	PWM_5,
	PWM_6,
	MAX_PWM = 6
}PWM_t;

void Pwm_Init_Single_Edge(PWM_t, uint32_t);

void Pwm_Set_DutyCycle(PWM_t pwm_id, uint32_t anchoDePulso);

uint32_t Pwm_Get_DutyCycle(PWM_t pwm_id);
