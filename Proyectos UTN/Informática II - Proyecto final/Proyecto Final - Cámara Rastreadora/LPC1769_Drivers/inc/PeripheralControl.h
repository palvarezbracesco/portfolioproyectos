/*
===============================================================================
 Name        : PeripheralControl.h
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

#ifndef PERIPHERALCONTROL_H_
#define PERIPHERALCONTROL_H_

#include "LPC1769.h"

typedef enum
{
	PCTIM0=1,
	PCTIM1 ,
	PCUART0,
	PCUART1,
	PCPWM1 =6,
	PCI2C0,
	PCSPI,
	PCRTC,
	PCSSP1,
	PCADC=12,
	PCCAN1,
	PCCAN2,
	PCGPIO,
	PCRIT,
	PCMCPWM,
	PCQEI,
	PCI2C1,
	PCSSP0=21,
	PCTIM2,
	PCTIM3,
	PCUART2,
	PCUART3,
	PCI2C2,
	PCI2S,
	PCGPDMA=29,
	PCENET,
	PCUSB
}PCONP_t;

typedef enum
{
	PCLK_WDT,
	PCLK_TIMER0,
	PCLK_TIMER1,
	PCLK_UART0,
	PCLK_UART1,
	PCLK_PWM1=6,
	PCLK_I2C0,
	PCLK_SPI,
	PCLK_SSP1=10,
	PCLK_DAC,
	PCLK_ADC,
	PCLK_CAN1,
	PCLK_CAN2,
	PCLK_ACF,
	PCLK_QEI, //ESTE SERIA EL 1ERO DEL PCLKSEL1
	PCLK_GPIOINT,
	PCLK_PCB,
	PCLK_I2C1,
	PCLK_SSP0=21,
	PCLK_TIMER2,
	PCLK_TIMER3,
	PCLK_UART2,
	PCLK_UART3,
	PCLK_I2C2,
	PCLK_I2S,
    PCLK_RIT=29,
	PCLK_SYSCON,
	PCLK_MC
}PCLK_t;


typedef enum
{
	CCLK_break_4=0,
	CCLK,
	CCLK_break_2,
	CCLK_break_special //except for CAN1, CAN2, and CAN filtering when “11” selects = CCLK/6.
}PCLK_Function_t;

void PeripheralControl_SelectClock(PCLK_t ,PCLK_Function_t);

ON_OFF_t PeripheralControl_GetPowerControl(PCONP_t );

void PeripheralControl_SetPowerControl(PCONP_t,ON_OFF_t);

#endif /* PERIPHERALCONTROL_H_ */
