/*
===============================================================================
 Name        : LPC1769.h
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/
/***********************************************************************************************************************************
 *** MODULO
 **********************************************************************************************************************************/
#ifndef __LPC17xx_H__
#define __LPC17xx_H__

#include <stdint.h>
/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/
/* IO definitions (access restrictions to peripheral registers) */
#define   __I     volatile const       /*!< Defines 'read only' permissions                 */
#define   __O     volatile             /*!< Defines 'write only' permissions                */
#define   __IO    volatile             /*!< Defines 'read / write' permissions              */

/* Memory mapping of Cortex-M3 Hardware */
#define SCS_BASE            (0xE000E000UL)                            /*!< System Control Space Base Address */

/* Memory mapping of Cortex-M3 Hardware */
#define SCS_BASE            (0xE000E000UL)                            /*!< System Control Space Base Address */
#define SysTick_BASE        (SCS_BASE +  0x0010UL)                    /*!< SysTick Base Address              */

/* SysTick configuration struct */
#define SysTick             ((SysTick_Type *)       SysTick_BASE)     /*!< SysTick configuration struct      */

typedef enum
{
	OFF,
	ON
}ON_OFF_t;

/* Definición de puertos */
#define PORT_0		 0
#define PORT_1		 1
#define PORT_2		 2
#define PORT_3		 3
#define PORT_4		 4

/* Definición de pines */
#define PIN_0		 0
#define PIN_1		 1
#define PIN_2		 2
#define PIN_3		 3
#define PIN_4		 4
#define PIN_5		 5
#define PIN_6		 6
#define PIN_7		 7
#define PIN_8		 8
#define PIN_9		 9
#define PIN_10		10
#define PIN_11		11
#define PIN_12		12
#define PIN_13		13
#define PIN_14		14
#define PIN_15		15
#define PIN_16		16
#define PIN_17		17
#define PIN_18		18
#define PIN_19		19
#define PIN_20		20
#define PIN_21		21
#define PIN_22		22
#define PIN_23		23
#define PIN_24		24
#define PIN_25		25
#define PIN_26		26
#define PIN_27		27
#define PIN_28		28
#define PIN_29		29
#define PIN_30		30
#define PIN_31		31

/** LPC175x/6x Peripheral addresses and register set declarations */
#define LPC_GPIO0_BASE            0x2009C000
#define LPC_GPIO1_BASE            0x2009C020
#define LPC_GPIO2_BASE            0x2009C040
#define LPC_GPIO3_BASE            0x2009C060
#define LPC_GPIO4_BASE            0x2009C080
#define LPC_IOCON_BASE            0x4002C000

/* Assign LPC_* names to structures mapped to addresses */
#define LPC_GPIO                  ((LPC_GPIO_T             *) LPC_GPIO0_BASE)//LPC_GPIO0
#define LPC_GPIO1                 ((LPC_GPIO_T             *) LPC_GPIO1_BASE)
#define LPC_GPIO2                 ((LPC_GPIO_T             *) LPC_GPIO2_BASE)
#define LPC_GPIO3                 ((LPC_GPIO_T             *) LPC_GPIO3_BASE)
#define LPC_GPIO4                 ((LPC_GPIO_T             *) LPC_GPIO4_BASE)
#define LPC_GPIOINT               ((LPC_GPIOINT_T          *) LPC_GPIOINT_BASE)
#define LPC_IOCON                 ((LPC_IOCON_T            *) LPC_IOCON_BASE)

/** LPC175x/6x PINSEL addresses and register set declarations */
#define LPC_PINSEL0_BASE            0x4002C000
#define LPC_PINSEL1_BASE            0x4002C004
#define LPC_PINSEL2_BASE            0x4002C008
#define LPC_PINSEL3_BASE            0x4002C00C
#define LPC_PINSEL4_BASE            0x4002C010
#define LPC_PINSEL7_BASE            0x4002C01C
#define LPC_PINSEL8_BASE            0x4002C020
#define LPC_PINSEL9_BASE            0x4002C024
#define LPC_PINSEL10_BASE           0x4002C028

/* Assign LPC_* names to structures mapped to addresses */
#define LPC_PINSEL                  ((__IO uint32_t             *) LPC_PINSEL0_BASE)//PINSEL0
#define LPC_PINSEL1                 ((__IO uint32_t             *) LPC_PINSEL1_BASE)
#define LPC_PINSEL2                 ((__IO uint32_t             *) LPC_PINSEL2_BASE)
#define LPC_PINSEL3                 ((__IO uint32_t             *) LPC_PINSEL3_BASE)
#define LPC_PINSEL4                 ((__IO uint32_t             *) LPC_PINSEL4_BASE)
#define LPC_PINSEL7                 ((__IO uint32_t             *) LPC_PINSEL7_BASE)
#define LPC_PINSEL8                 ((__IO uint32_t             *) LPC_PINSEL8_BASE)
#define LPC_PINSEL9                 ((__IO uint32_t             *) LPC_PINSEL9_BASE)
#define LPC_PINSEL10                ((__IO uint32_t             *) LPC_PINSEL10_BASE)

/** LPC175x/6x PINMODE addresses and register set declarations */
#define LPC_PINMODE0_BASE            0x4002C040
#define LPC_PINMODE1_BASE            0x4002C044
#define LPC_PINMODE2_BASE            0x4002C048
#define LPC_PINMODE3_BASE            0x4002C04C
#define LPC_PINMODE4_BASE            0x4002C050
#define LPC_PINMODE7_BASE            0x4002C05C
#define LPC_PINMODE9_BASE            0x4002C064

/* Assign LPC_* names to structures mapped to addresses */
#define LPC_PINMODE                  ((__IO uint32_t             *) LPC_PINMODE0_BASE)//PINMODE0
#define LPC_PINMODE1                 ((__IO uint32_t             *) LPC_PINMODE1_BASE)
#define LPC_PINMODE2                 ((__IO uint32_t             *) LPC_PINMODE2_BASE)
#define LPC_PINMODE3                 ((__IO uint32_t             *) LPC_PINMODE3_BASE)
#define LPC_PINMODE4                 ((__IO uint32_t             *) LPC_PINMODE4_BASE)
#define LPC_PINMODE7                 ((__IO uint32_t             *) LPC_PINMODE7_BASE)
#define LPC_PINMODE9                 ((__IO uint32_t             *) LPC_PINMODE9_BASE)

/** LPC175x/6x PINMODE_OD addresses and register set declarations */
#define LPC_PINMODE_OD0_BASE            0x4002C068
#define LPC_PINMODE_OD1_BASE            0x4002C06C
#define LPC_PINMODE_OD2_BASE            0x4002C070
#define LPC_PINMODE_OD3_BASE            0x4002C074
#define LPC_PINMODE_OD4_BASE            0x4002C078

/* Assign LPC_* names to structures mapped to addresses */
#define LPC_PINMODE_OD                  ((__IO uint32_t             *) LPC_PINMODE_OD0_BASE)//PINMODE_OD0
#define LPC_PINMODE_OD1                 ((__IO uint32_t             *) LPC_PINMODE_OD1_BASE)
#define LPC_PINMODE_OD2                 ((__IO uint32_t             *) LPC_PINMODE_OD2_BASE)
#define LPC_PINMODE_OD3                 ((__IO uint32_t             *) LPC_PINMODE_OD3_BASE)
#define LPC_PINMODE_OD4                 ((__IO uint32_t             *) LPC_PINMODE_OD4_BASE)


/** LPC175x/6x EXTINT address and register set declaration */
#define LPC_EXTINT_BASE				0x400FC140
/* Assign LPC_* names to structures mapped to addresses */
#define LPC_EXTINT                  ((__IO uint32_t             *) LPC_EXTINT_BASE)

/** LPC175x/6x EXTMODE address and register set declaration */
#define LPC_EXTMODE_BASE			0x400FC148

/* Assign LPC_* names to structures mapped to addresses */
#define LPC_EXTMODE                  ((__IO uint32_t             *) LPC_EXTMODE_BASE)


/** LPC175x/6x EXTPOLAR address and register set declaration */
#define LPC_EXTPOLAR_BASE			0x400FC14C
/* Assign LPC_* names to structures mapped to addresses */
#define LPC_EXTPOLAR                  ((__IO uint32_t             *) LPC_EXTPOLAR_BASE)

/* NVIC register map */
#define ISER						((__IO uint32_t             *) 0xE000E100)//ISER0
#define ISER1						((__IO uint32_t             *) 0xE000E104)
#define ICER						((__IO uint32_t             *) 0xE000E180)//ICER0
#define ICER1						((__IO uint32_t             *) 0xE000E184)
#define ISPR						((__IO uint32_t             *) 0xE000E200)//ISPR0
#define ISPR1						((__IO uint32_t             *) 0xE000E204)
#define ICPR						((__IO uint32_t             *) 0xE000E280)//ICPR0
#define ICPR1						((__IO uint32_t             *) 0xE000E284)
#define IABR						((__O uint32_t             *) 0xE000E300)//IABR0
#define IABR1						((__O uint32_t             *) 0xE000E304)
#define IPR						    ((__IO uint32_t             *) 0xE000E400)//IPR0
#define IPR1						((__IO uint32_t             *) 0xE000E404)
#define IPR2						((__IO uint32_t             *) 0xE000E408)
#define IPR3						((__IO uint32_t             *) 0xE000E40C)
#define IPR4						((__IO uint32_t             *) 0xE000E410)
#define IPR5						((__IO uint32_t             *) 0xE000E414)
#define IPR6						((__IO uint32_t             *) 0xE000E418)
#define IPR7						((__IO uint32_t             *) 0xE000E41C)
#define IPR8						((__IO uint32_t             *) 0xE000E420)
#define STIR						((__I uint32_t             *) 0xE000EF00)

//-------------- Peripheral Registers -------------------------------
#define PCONP_BASE 				0x400FC0C4
#define PCLKSEL0_BASE			0x400FC1A8
#define PCLKSEL1_BASE			0x400FC1AC
#define PCONP						((__IO uint32_t             *) PCONP_BASE)
#define PCLKSEL						((__IO uint32_t             *) PCLKSEL0_BASE)
#define PCLKSEL1					((__IO uint32_t             *) PCLKSEL1_BASE)


#define UART0_BASE				0x4000C000
#define UART1_BASE				0x40010000
#define UART2_BASE				0x40098000
#define UART3_BASE				0x4009C000


/*
typedef struct
{
	union
	{
		__I  uint32_t RBR;
		__O  uint32_t THR;
		__IO uint32_t DLL;
	}RBR_THR_DLL;
	union
	{
		__IO uint32_t DLM;
		__IO uint32_t IER;

	}DLM_IER;
	union
	{
		__I  uint32_t IIR;
		__O  uint32_t FCR;
	}IIR_FCR;
	__IO uint32_t LCR;
	__IO uint32_t MCR;
	__I uint32_t LSR;
	__I uint32_t MSR;
	__IO uint32_t SCR;
	__IO uint32_t ACR;
		 uint32_t reserved_0;
	__IO uint32_t FDR;
		 uint32_t reserved_1;
	__IO uint32_t TER;
		 uint32_t reserved_2;
		 uint32_t reserved_3;
		 uint32_t reserved_4;
		 uint32_t reserved_5;
		 uint32_t reserved_6;
		 uint32_t reserved_7;
	__IO uint32_t RS485CTRL;
	__IO uint32_t ADRMATCH;
	__IO uint32_t RS485DLY;
}UART1_t;

*/
typedef struct {					/*!< UARTn Structure       */

	union {
		__I  uint32_t  RBR;			/*!< Receiver Buffer Register. Contains the next received character to be read (DLAB = 0). */
		__O  uint32_t  THR;			/*!< Transmit Holding Register. The next character to be transmitted is written here (DLAB = 0). */
		__IO uint32_t  DLL;			/*!< Divisor Latch LSB. Least significant byte of the baud rate divisor value. The full divisor is used to generate a baud rate from the fractional rate divider (DLAB = 1). */
	};

	union {
		__IO uint32_t IER;			/*!< Interrupt Enable Register. Contains individual interrupt enable bits for the 7 potential UART interrupts (DLAB = 0). */
		__IO uint32_t DLM;			/*!< Divisor Latch MSB. Most significant byte of the baud rate divisor value. The full divisor is used to generate a baud rate from the fractional rate divider (DLAB = 1). */
	};

	union {
		__O  uint32_t FCR;			/*!< FIFO Control Register. Controls UART FIFO usage and modes. */
		__I  uint32_t IIR;			/*!< Interrupt ID Register. Identifies which interrupt(s) are pending. */
	};

	__IO uint32_t LCR;				/*!< Line Control Register. Contains controls for frame formatting and break generation. */
	__IO uint32_t MCR;				/*!< Modem Control Register. Only present on UART1 */
	__I  uint32_t LSR;				/*!< Line Status Register. Contains flags for transmit and receive status, including line errors. */
	__I  uint32_t MSR;				/*!< Modem Status Register. Only present on UART1 */
	__IO uint32_t SCR;				/*!< Scratch Pad Register. Eight-bit temporary storage for software. */
	__IO uint32_t ACR;				/*!< Auto-baud Control Register. Contains controls for the auto-baud feature. */
	__IO uint32_t ICR;				/*!< IrDA control register. Only present on UART0/2/3 */
	__IO uint32_t FDR;				/*!< Fractional Divider Register. Generates a clock input for the baud rate divider. */
	uint32_t RESERVED0;
	__IO uint32_t TER;				/*!< Transmit Enable Register. Turns off UART transmitter for use with software flow control. */
	uint32_t  RESERVED1[6];
	__IO uint32_t RS485CTRL;		/*!< RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes. Only present on UART1*/
	__IO uint32_t RS485ADRMATCH;	/*!< RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode. Only present on UART1*/
	__IO uint32_t RS485DLY;			/*!< RS-485/EIA-485 direction control delay. Only present on UART1*/

}UARTn_t;

#define UART0 (__IO UARTn_t*)UART0_BASE
#define UART1 (__IO UARTn_t*)UART1_BASE//(__IO UART1_t*)UART1_BASE
#define UART2 (__IO UARTn_t*)UART2_BASE
#define UART3 (__IO UARTn_t*)UART3_BASE

/***********************************************************************************************************************************
 *** FIN DEL MODULO*/

#endif// __LPC17xx_H__
