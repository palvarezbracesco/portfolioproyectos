/*
===============================================================================
 Name        : ExtInt.h
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

#ifndef EXTINT_H_
#define EXTINT_H_

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include <stdint.h>


/***********************************************************************************************************************************
 *** TIPOS
 **********************************************************************************************************************************/

/* Definiciones con numero de las lineas de Interrupcion Externa (No modificar) */
typedef enum {
	EINT0 = 0,
	EINT1,
	EINT2,
	EINT3
}eint_t;

/* Definiciones con numero de las opciones para la polaridad */
typedef enum {
	EINT_LOW = 0,
	EXT_LOW = 0,
	EINT_HIGH = 1,
	EXT_HIGH = 1
}polarity_t;

/* Definiciones con numero de los modos de funcionamiento */
typedef enum {
	LEVEL_SENSITIVITY =0,
	EINT_LEVEL = 0,
	EINT_FLANK = 1,
	EDGE_SENSITIVITY = 1,
}int_mode_t;

/**
 * @brief Función para activar las interrupciones externas en el registro NVIC
 * @param eint : Linea de interrupcion externa a activar
 */
void EInt_Enable(eint_t eint);

/**
 * @brief Función para desactivar las interrupciones externas en el registro NVIC
 * @param eint : Linea de interrupcion externa a desactivar
 */
void EInt_Disable(eint_t eint);

/**
 * @brief Configuracion de la interrupcion externa
 * @param eint : Linea de interrupcion externa a configurar
 * @param mode : Modo de trabajo de la interrupcion (EINT_LEVEL, EINT_FLANK)
 * @param polarity : Polaridad para el modo de trabajo indicado anteriormente
 */
void EInt_Config(eint_t eint, int_mode_t mode, polarity_t polarity);

/**
 * @brief Limpieza del flag, responsable de la interrupción
 * @param eint : Linea de interrupcion externa a limpiar
 */
void EInt_CleanFlag(eint_t eint);

#endif /* EXTINT_H_ */
