/*
===============================================================================
 Name        : Pwm.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

#include "Pin.h"
#include "Pwm.h"
#include "PeripheralControl.h"
#include "LPC1769.h"

#define PWMPRESCALE (25-1) //25 ciclos del reloj cada un micro segundo

#define SINGLE_EDGE_MODE 0

#define LPC_PWM1_REGISTER 0x40018000UL

#define LPC_PWM 				((PWM_T 			*) LPC_PWM1_REGISTER)

typedef struct {
	__IO uint32_t IR;
	__IO uint32_t TCR;
	__IO uint32_t TC;
	__IO uint32_t PR;
	__IO uint32_t PC;
	__IO uint32_t MCR;
	__IO uint32_t MR0;
	__IO uint32_t MR1;
	__IO uint32_t MR2;
	__IO uint32_t MR3;
	__IO uint32_t CCR;
	__O uint32_t CR[4];
	uint32_t RESERVERD0;
	__IO uint32_t MR4;
	__IO uint32_t MR5;
	__IO uint32_t MR6;
	__IO uint32_t PCR;
	__IO uint32_t LER;
	uint32_t RESERVERD[7];
	__IO uint32_t CTCR;
}PWM_T;

void Pwm_Init_Single_Edge(PWM_t pwm_id, uint32_t Pwm_period)
{
	//PeripheralControl_SetPowerControl(PCPWM1, ON);

	if(pwm_id > MAX_PWM)	//Solo existen 6 pwm
		return;
	/* Power: En el registro PCONP, configurar el bit PCPWM1.
Dato: En el reseteo el pwm está activo (PCPWM1 = 1).*/

	/*Peripheral clock: In the PCLKSEL0 register (Table 40)
Dato: El seteo por default es pclk/4 = 30 MHz.*/


	/*Pins: Seleccionar el modo de funcionamiento del pin*/

	LPC_Pin_SetFunction(PORT_2, pwm_id ,FIRST_ALT_FUNC);

	/*4. Interrupciones: Por el momento fuera de uso*/

	LPC_PWM->PCR &= ~(1UL << (pwm_id)); /* Setea el PWM como Single Edge
														Modificar el modo del pwm0 y el pwm1
														no tiene ningun efecto */

	LPC_PWM->PR = PWMPRESCALE; //1 us de resolucion

	LPC_PWM->MR0 = Pwm_period; // Setea el ciclo del PWM (Cycle Time -> Ton + Toff)

	LPC_PWM->MCR = (1UL << SBIT_PWMMR0R);  // En el match, resetea PWMMR0, y resetea TC si matchea con MR0

    LPC_PWM->LER = (1UL << pwm_id) | (1UL<< 0); ; // Actualiza los valores de MR0

	LPC_PWM->PCR |= (1UL << (8+pwm_id) );// Habilita salida PWM

	LPC_PWM->TCR = (1UL << 1); // Resetea PWM TC y PR

	LPC_PWM->TCR = (1UL << 0) | (1<<3); // Habilita contadores y el PWM mode
}

/**
 * @brief Actualiza el periodo en el que el pwm 1 está en alto
 * @param anchoDePulso 	Este es el valor en microsegundos en el que
 * 						el pwm estará en alto, si se coloca un valor
 * 						mayor que un periodo completo de funcionamiento
 * 						el pwm permanecerá en 1 permanente, si se coloca 0
 * 						el pwm permanecerá en 0 permanente.
 */
void Pwm_Set_DutyCycle(PWM_t pwm_id, uint32_t anchoDePulso)
{
	switch (pwm_id)
	{
	case PWM_0:
		break;

	case PWM_1:

		LPC_PWM->MR1 = anchoDePulso; // Actualiza MR con el nuevo ancho de pulso
		LPC_PWM->LER = (1UL << SBIT_LEN1);

		break;

	case PWM_2:

		LPC_PWM->MR2 = anchoDePulso; // Actualiza MR con el nuevo ancho de pulso
		LPC_PWM->LER |= (1UL << SBIT_LEN2);
		break;

	case PWM_3:

		LPC_PWM->MR3 = anchoDePulso; // Actualiza MR1 con el nuevo ancho de pulso
		LPC_PWM->LER |= (1UL << SBIT_LEN3);

		break;

	case PWM_4:

		LPC_PWM->MR4 = anchoDePulso; // Actualiza MR con el nuevo ancho de pulso
		LPC_PWM->LER |= (1UL << SBIT_LEN4);

		break;

	case PWM_5:

		LPC_PWM->MR5 = anchoDePulso; // Actualiza MR con el nuevo ancho de pulso
		LPC_PWM->LER |= (1UL << SBIT_LEN5);

		break;

	case PWM_6:

		LPC_PWM->MR6 = anchoDePulso; // Actualiza MR con el nuevo ancho de pulso
		LPC_PWM->LER |= (1UL << SBIT_LEN6);

		break;
	default:
		break;
	}
}

uint32_t Pwm_Get_DutyCycle(PWM_t pwm_id)
{
	uint32_t ancho_de_pulso;

	switch (pwm_id)
	{
	case PWM_0:
		break;

	case PWM_1:

		ancho_de_pulso = LPC_PWM->MR1; // Actualiza MR con el nuevo ancho de pulso

		break;

	case PWM_2:

		ancho_de_pulso = LPC_PWM->MR2; // Actualiza MR con el nuevo ancho de pulso

		break;

	case PWM_3:

		ancho_de_pulso = LPC_PWM->MR3; // Actualiza MR con el nuevo ancho de pulso


		break;

	case PWM_4:

		ancho_de_pulso = LPC_PWM->MR4; // Actualiza MR con el nuevo ancho de pulso


		break;

	case PWM_5:

		ancho_de_pulso = LPC_PWM->MR5; // Actualiza MR con el nuevo ancho de pulso


		break;

	case PWM_6:

		ancho_de_pulso = LPC_PWM->MR6; // Actualiza MR con el nuevo ancho de pulso


		break;

	default:
		break;
	}

	return ancho_de_pulso;
}
