/*
===============================================================================
 Name        : Teclado.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/


/***********************************************************************************************************************************
 *** MODULO Keyboard
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include <Teclado.h>
#include "Pin.h"
#include "Gpio.h"

/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

#define BUFFER_SIZE 256
/***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

bool Bounce_Ended(void);
void Anti_Bounce_Update(void);
void Teclado_Buffer_Update(const keyboard_t);
void Teclado_Buffer_Push(key_t);
key_t Teclado_Buffer_Pull(void);

/***********************************************************************************************************************************
 *** TIPOS PRIVADOS
 **********************************************************************************************************************************/

typedef struct {
	uint8_t in;
	uint8_t out;
	uint8_t size;
	key_t data [BUFFER_SIZE];
} Buffer_Teclado_t;

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS
 **********************************************************************************************************************************/

static Buffer_Teclado_t buffer;
static keyboard_t anti_bounce [2];
static keyboard_t matrix_keyboard_state = 0;
static keyboard_t extern_keyboard_state = 0;
static keyboard_t keyboard_state = 0;
static size_t bounce_count = 0;

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/
/*
 * @brief Función que inicializa el teclado matricial de la expansión 3
 */
void Teclado_Init(void)
{
	/*FILAS*/

	LPC_Pin_SetFunction			(PORT_1, PIN_25, DEFAULT_FUNC);
	LPC_Pin_SetResistorMode		(PORT_1, PIN_25, RES_PULL_UP);
	LPC_Gpio_SetDir				(PORT_1, PIN_25, DIR_IN);

	LPC_Pin_SetFunction			(PORT_1, PIN_22, DEFAULT_FUNC);
	LPC_Pin_SetResistorMode		(PORT_1, PIN_22, RES_PULL_UP);
	LPC_Gpio_SetDir				(PORT_1, PIN_22, DIR_IN);

	LPC_Pin_SetFunction			(PORT_1, PIN_19, DEFAULT_FUNC);
	LPC_Pin_SetResistorMode		(PORT_1, PIN_19, RES_PULL_UP);
	LPC_Gpio_SetDir				(PORT_1, PIN_19, DIR_IN);

	LPC_Pin_SetFunction			(PORT_0, PIN_20, DEFAULT_FUNC);
	LPC_Pin_SetResistorMode		(PORT_0, PIN_20, RES_PULL_UP);
	LPC_Gpio_SetDir				(PORT_0, PIN_20, DIR_IN);

	/*COLUMNAS*/

	LPC_Pin_SetFunction			(PORT_3, PIN_25, DEFAULT_FUNC);
	LPC_Pin_SetOpenDrainMode	(PORT_3, PIN_25,NOT_OPEN_DRAIN);
	LPC_Gpio_SetDir				(PORT_3, PIN_25, DIR_OUT);
	LPC_Gpio_SetPin				(PORT_3, PIN_25);

	LPC_Pin_SetFunction			(PORT_1, PIN_27, DEFAULT_FUNC);
	LPC_Pin_SetOpenDrainMode	(PORT_1, PIN_27,NOT_OPEN_DRAIN);
	LPC_Gpio_SetDir				(PORT_1, PIN_27, DIR_OUT);
	LPC_Gpio_SetPin				(PORT_1, PIN_27);

	return;

}

/*
 * Función que actualiza el estado del teclado
 */
void Teclado_Update(void)
{
	if(Bounce_Ended())
	{
		matrix_keyboard_state = Teclado_Read();
	}

	Teclado_Buffer_Update(matrix_keyboard_state | extern_keyboard_state);

	//ACTUALIZO EL ESTADO ACTUAL

	keyboard_state = matrix_keyboard_state | extern_keyboard_state;
}

key_t Teclado_GetKey(void)
{
	return Teclado_Buffer_Pull();
}

bool Teclado_GetKeyState(key_t keyNum)
{
	return (keyboard_state&((keyboard_t)1<<keyNum))? true : false;
}

/**
 * @brief Funcion que configura el estado del teclado externo
 * @param newstate Nuevo estado del teclado (one hot)
 */
void Teclado_Extern_Set(keyboard_t newstate)
{
	extern_keyboard_state = newstate;
}

keyboard_t Teclado_Internal_Get(void)
{
	return matrix_keyboard_state;
}

keyboard_t Teclado_Read(void)
{
	//ESTADO DEL KEYBOARD INTERNO //

	keyboard_t teclado = 0;
	//Activo la primera columna

	LPC_Gpio_ClrPin(PORT_3, PIN_25);
	LPC_Gpio_SetPin(PORT_1, PIN_27);

	//Leo las filas
	if(LPC_Gpio_GetPinValue(PORT_1, PIN_25)==LOW)
	{
		teclado |= (1UL << KEY_UP);

	}

	if(LPC_Gpio_GetPinValue(PORT_1, PIN_22)==LOW)
	{
		teclado |= (1UL << KEY_LEFT);

	}

	if(LPC_Gpio_GetPinValue(PORT_1, PIN_19)==LOW)
	{
		teclado |= (1UL << KEY_DOWN);

	}

	if(LPC_Gpio_GetPinValue(PORT_0, PIN_20)==LOW)
	{
		teclado |= (1UL << KEY_MODE);

	}

	//Activo la segunda columna

	LPC_Gpio_SetPin(PORT_3, PIN_25);
	LPC_Gpio_ClrPin(PORT_1, PIN_27);

	//Leo las filas

	if(LPC_Gpio_GetPinValue(PORT_1, PIN_25)==LOW)
	{
		teclado |= (1UL << KEY_MIC);

	}

	if(LPC_Gpio_GetPinValue(PORT_1, PIN_22)==LOW)
	{
		teclado |= (1UL << KEY_RIGHT);

	}

	if(LPC_Gpio_GetPinValue(PORT_1, PIN_19)==LOW)
	{
		teclado |= (1UL << KEY_CAMERA);

	}

	if(LPC_Gpio_GetPinValue(PORT_0, PIN_20)==LOW)
	{
		teclado |= (1UL << KEY_UNDEFINED);

	}

	return teclado;
}

//Funcion para filtrar los rebotes de las teclas
bool Bounce_Ended(void)
{
	anti_bounce[1] = anti_bounce[0];

	anti_bounce[0] = Teclado_Read();

	if( anti_bounce[0] == anti_bounce[1] )
	{
		if(bounce_count == ANTI_BOUNCE_CHECKS)
		{
			return true;
		}
		else
		{
			bounce_count++;
		}
	}
	else
	{
		bounce_count = 1;
	}

	return false;
}

void Teclado_Buffer_Update(const keyboard_t new_buffer)
{
	if(new_buffer > keyboard_state)
	{
		keyboard_t aux = new_buffer & (keyboard_t)(~keyboard_state);

		for(key_t key = 0 ; key < MAX_TECLAS ; key++)
		{
			if((aux >> key) & (keyboard_t)1)
			{
				Teclado_Buffer_Push(key);
			}
		}
	}
}

void Teclado_Buffer_Push(key_t key)
{
	if(!(buffer.size == BUFFER_SIZE-1))
	{
		buffer.data[buffer.in] = key;
		buffer.size++;
		buffer.in++;
	}
	return;
}

key_t Teclado_Buffer_Pull(void)
{
	key_t key = NO_KEY;

	if(buffer.size)
	{
		key = buffer.data[buffer.out];

		buffer.size--;
		buffer.out++;
	}

	return key;
}

/***********************************************************************************************************************************
 *** FIN DEL MODULO Keyboard
 **********************************************************************************************************************************/
