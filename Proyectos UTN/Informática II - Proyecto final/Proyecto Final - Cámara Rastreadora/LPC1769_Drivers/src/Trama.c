/*
===============================================================================
 Name        : Trama.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

/***********************************************************************************************************************************
 *** MODULO trama
 **********************************************************************************************************************************/



/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include <stdbool.h>
#include <stdint.h>
#include <Teclado.h>
#include "Trama.h"
#include "Motor.h"

/***********************************************************************************************************************************
 *** TIPOS
 **********************************************************************************************************************************/


/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

#define ANGULO_MAX 50
#define ANGULO_MIN -50

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/

bool Procesar_Trama(void);

static uint8_t trama [MAX_ELEMENTOS];

void Analizar_Elemento(char elemento)
{
	static int i = 0;


	if(elemento == BYTE_INICIO)
	{
		trama[INICIO] = BYTE_INICIO;
		i = 1;
	}

	else if(i > 0)
	{
		trama[i] = elemento;

		if(i == MAX_ELEMENTOS)
		{
			i = 0;
		}

		else if(trama[i] == BYTE_FIN )
		{

			Procesar_Trama();
			i = 0;
		}
		else
		{
			i++;

		}
	}


	return;

}

/**
 * @brief Función interna al modulo
 * @return 	true si la trama fue válida y ha sido procesada correctamente
 * 			false si la trama no fue válida
 */
bool Procesar_Trama(void)
{
	motor_pos_s pos = Motor_Posicion();
	keyboard_t keyboard;

	switch(trama[FUNCION])
	{
	case PAN:

		switch (trama[PARAM_DIRECC])
		{
		case IZQUIERDA:

			pos.angulo_pan = (uint8_t)(pos.angulo_pan + trama[PARAM_ANGULO]);

			Motor_Buffer_Push(pos);

			break;

		case DERECHA:

			pos.angulo_pan = (uint8_t)(pos.angulo_pan - trama[PARAM_ANGULO]);

			Motor_Buffer_Push(pos);

			break;
		}

		break;

	case TILT:

		switch (trama[PARAM_DIRECC])
		{
		case ARRIBA:

			pos.angulo_tilt = (uint8_t)(pos.angulo_tilt - trama[PARAM_ANGULO]);

			Motor_Buffer_Push(pos);

			break;

		case ABAJO:

			pos.angulo_tilt = (uint8_t)(pos.angulo_tilt + trama[PARAM_ANGULO]);

			Motor_Buffer_Push(pos);

			break;
		}

		break;

	case KEYBOARD:

		keyboard = (keyboard_t)trama[PARAM_KEYBOARD];

		Teclado_Extern_Set(keyboard);

		return true;

		break;

	default:
		break;
	}

	return false;
}

/***********************************************************************************************************************************
 *** FIN DEL MODULO trama
 **********************************************************************************************************************************/
