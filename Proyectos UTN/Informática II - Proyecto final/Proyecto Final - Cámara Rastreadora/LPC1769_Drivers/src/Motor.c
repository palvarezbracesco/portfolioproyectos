/*
===============================================================================
 Name        : Motor.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

/***********************************************************************************************************************************
 *** MODULO Motor
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include <stdio.h>
#include <math.h>

#include "Motor.h"
#include "Pwm.h"

/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

#define MOTOR_BUFFER_SIZE	256				// NO SE PUEDE CAMBIAR (info en la estructura “MotorRingBuffer_s“)
#define PI 		3.14159265359	// NO SE PUEDE CAMBIAR
#define MODULO_MAXIMO 90
#define FASE_MAXIMA 360

/***********************************************************************************************************************************
 *** ESTRUCTURAS LOCALES
 **********************************************************************************************************************************/
typedef struct
{
	volatile uint8_t in;
	volatile uint8_t out;
	volatile uint16_t count;
	volatile motor_pos_s buffer[MOTOR_BUFFER_SIZE]; 	//Largo forzado debido al tipo
											//de in y out
}MotorRingBuffer_s;

/***********************************************************************************************************************************
 *** VARIABLES LOCALES
 **********************************************************************************************************************************/
static MotorRingBuffer_s 	motor_rb; //Ring buffer del motor
static motor_pos_s actual_position;
/***********************************************************************************************************************************
 *** PROTOTIPOS LOCALES
 **********************************************************************************************************************************/

uint32_t Grados_A_PWM(uint8_t grados);
uint8_t PWM_A_Grados(uint32_t Pwm_Dc);

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/


void Motor_Inicializar(void)
{
	Pwm_Init_Single_Edge(PWM_2, SERVO_PWM_PERIOD);
	Pwm_Init_Single_Edge(MOTOR_TILT, SERVO_PWM_PERIOD);
	Pwm_Init_Single_Edge(MOTOR_PAN, SERVO_PWM_PERIOD);
	actual_position.angulo_pan = 90;
	actual_position.angulo_tilt = 90;
	Motor_Buffer_Push(actual_position);
}

/**
 * @brief Si existe un elemento en buffer lo lee y lo transforma en un movimiento del servo.
 * RECOMENDACIÓN : Llamar esta funcion cada 10 milisegundos o mas debido a las limitaciones físicas
 * 					de los servomotores.
 * IMPORTANTE : Esta función se debe llamar en la misma frecuencia que se usa el push.
 * 				Esto es para no sosbrecargar el buffer
 */
void Motor_Actualizar (void)
{
	if(!Motor_Buffer_Vacio())
	{
		motor_pos_s posicion_motor;

		posicion_motor = Motor_Buffer_Pop();

		Motor_Mover(MOTOR_PAN, posicion_motor.angulo_pan);

		Motor_Mover(MOTOR_TILT, posicion_motor.angulo_tilt);

		actual_position = posicion_motor;
	}
	return;
}

/**
 * @brief Funcion para mover un servomotor
 * Funcion para mover un servomotor
 * @param motor El identificador del motor
 * @param grados La posición final en grados
 */
void Motor_Mover(motor_t motor, uint8_t grados)
{
	Pwm_Set_DutyCycle(motor, Grados_A_PWM(grados));
	return;
}


motor_pos_s Motor_Posicion(void)
{
	return actual_position;
}

/**
 * @brief Introduce un nuevo elemento al buffer del motor.
 * @param nuevo_modulo Modulo del nuevo elemento en buffer
 * @param nueva_fase Fase del nuevo elemento en buffer
 * IMPORTANTE: El uso de esta función esta limitada por un buffer de 256 elementos.
 * Debe llamarse a la funcion actualizar para ir sacando elementos del buffer
 */
void Motor_Buffer_Push(motor_pos_s nueva_pos)
{

	if(nueva_pos.angulo_pan > SERVO_MAX_GRADOS || nueva_pos.angulo_tilt >SERVO_MAX_GRADOS)
	{
		return;
	}

	else if (motor_rb.count < MOTOR_BUFFER_SIZE)
	{
		motor_rb.buffer[motor_rb.in].angulo_pan = nueva_pos.angulo_pan;
		motor_rb.buffer[motor_rb.in].angulo_tilt = nueva_pos.angulo_tilt;
		motor_rb.in++;
		motor_rb.count++;
	}

	return;
}

/**
 * @brief Saca un elemento del buffer
 * 			Saca el primer elemento agregado.
 * @param modulo_actual Modulo del elemento del buffer que se saca
 * @param fase_actual Fase del elemento del buffer que se saca
 */
motor_pos_s Motor_Buffer_Pop(void)
{
	motor_pos_s posicion_motor_buffer;

	if (motor_rb.count > 0)
	{
		posicion_motor_buffer.angulo_pan = (motor_rb.buffer[motor_rb.out]).angulo_pan;
		posicion_motor_buffer.angulo_tilt = (motor_rb.buffer[motor_rb.out]).angulo_tilt;

		motor_rb.out++;
		motor_rb.count--;
	}

	return posicion_motor_buffer;
}

/**
 * @brief Funcion booleana que te informa si el buffer está vacío o no
 * @return 1(TRUE) si el buffer está vacío o 0(FALSE) si el buffer no está vacío
 */
bool Motor_Buffer_Vacio(void)
{
	return ((motor_rb.count == 0) ? true : false);
}

/**
 * @brief Función que convierte de grados a un determinado tiempo
 * @param Grados Grados que se requieren convertir a pulso
 * @return Devuelve el tiempo en microsegundos del pulso
 */
uint32_t Grados_A_PWM(uint8_t grados)
{
	return (((uint32_t)grados *(SERVO_PWM_MAX_DC - SERVO_PWM_MIN_DC)/ SERVO_MAX_GRADOS)+SERVO_PWM_MIN_DC);
}

uint8_t PWM_A_Grados(uint32_t Pwm_Dc)
{
	return (Pwm_Dc - SERVO_PWM_MIN_DC)*(SERVO_MAX_GRADOS/(SERVO_PWM_MAX_DC - SERVO_PWM_MIN_DC));
}

/***********************************************************************************************************************************
 *** FIN DEL MODULO Motor
 **********************************************************************************************************************************/

