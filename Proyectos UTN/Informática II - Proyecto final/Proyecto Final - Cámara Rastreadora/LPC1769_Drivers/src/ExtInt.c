/*
===============================================================================
 Name        : ExtInt.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

/***********************************************************************************************************************************
 *** MODULO DE INTERRUPCIONES EXTERNAS
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include "LPC1769.h"
#include "ExtInt.h"
#include "Nvic.h"

/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MÓDULO
 **********************************************************************************************************************************/

/* Definicion de registros de las interrupciones externas */
#define EXTINT		(*((__IO uint32_t *)0x400FC140UL))	/* Registro de Flags de Interrupciones Externas */
#define EXTMODE		(*((__IO uint32_t *)0x400FC148UL))	/* Registro de Modo de Interrupciones Externas */
#define EXTPOLAR	(*((__IO uint32_t *)0x400FC14CUL))	/* Registro de Polaridad de Interrupciones Externas */


/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/

/**
 * @brief Función para activar las interrupciones externas en el registro NVIC
 * @param eint : Linea de interrupcion externa a activar
 */
void EInt_Enable(eint_t eint)
{
	//Habilito la interrupcion en el NVIC (Nested Vectored Interrupt Controller)
	NVIC_EnableIRQ(EINT0_IRQn + eint);
}

/**
 * @brief Función para desactivar las interrupciones externas en el registro NVIC
 * @param eint : Linea de interrupcion externa a desactivar
 */
void EInt_Disable(eint_t eint)
{
	//Desabilito la interrupcion en el NVIC (Nested Vectored Interrupt Controller)
	NVIC_DisableIRQ(EINT0_IRQn + eint);
}

/**
 * @brief Configuracion de la interrupcion externa
 * @param eint : Linea de interrupcion externa a configurar
 * @param mode : Modo de trabajo de la interrupcion (EINT_LEVEL, EINT_FLANK)
 * @param polarity : Polaridad para el modo de trabajo indicado anteriormente
 */
void EInt_Config(eint_t eint, int_mode_t mode, polarity_t polarity)
{
	//Configuración del Modo (Nivel o Flanco).
	uint32_t temp;

	temp = EXTMODE & ~(3UL << eint); // Guardo temporalmente el registro con ceros en el lugar a modificar
	EXTMODE = temp  | ((uint32_t)mode << eint); // Escribo uno en los lugares indicados por parametro

	//Configuración de la Polaridad (Alto/Bajo para Nivel o Flanco Ascedente/Descente para Flanco)

	temp = EXTPOLAR & ~(3UL << eint); // Guardo temporalmente el registro con ceros en el lugar a modificar
	EXTPOLAR = temp  | ((uint32_t)polarity << eint); // Escribo uno en los lugares indicados por parametro
}

/**
 * @brief Limpieza del flag, responsable de la interrupción
 * @param eint : Linea de interrupcion externa a limpiar
 */
void EInt_CleanFlag(eint_t eint)
{
	EXTINT |= (1UL << eint); 	// Limpio el flag escribiendo un UNO en el registro de interrupciones externas
}


/***********************************************************************************************************************************
 *** FIN MODULO DE INTERRUPCIONES EXTERNAS
 **********************************************************************************************************************************/
