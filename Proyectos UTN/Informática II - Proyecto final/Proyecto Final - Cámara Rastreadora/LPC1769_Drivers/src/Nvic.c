/*
===============================================================================
 Name        : Nvic.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

/***********************************************************************************************************************************
 *** MODULO Nvic
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "Nvic.h"
#include "LPC1769.h"

/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/
#define NVIC_ISER				((__IO uint32_t *)0xE000E100UL)
#define NVIC_ISER0				NVIC_ISER[0]
#define NVIC_ISER1				NVIC_ISER[1]

#define NVIC_ICER				((__IO uint32_t *)0xE000E180UL)
#define NVIC_ICER0				NVIC_ICER[0]
#define NVIC_ICER1				NVIC_ICER[1]

#define NVIC_ISPR				((__IO uint32_t *)0xE000E200UL)
#define NVIC_ISPR0				NVIC_ISPR[0]
#define NVIC_ISPR1				NVIC_ISPR[1]

#define NVIC_ICPR				((__IO uint32_t *)0xE000E280UL)
#define NVIC_ICPR0				NVIC_ICPR[0]
#define NVIC_ICPR1				NVIC_ICPR[1]

#define NVIC_IABR				((__I uint32_t *)0xE000E300UL)
#define NVIC_IABR0				NVIC_IABR[0]
#define NVIC_IABR1				NVIC_IABR[1]

#define NVIC_IPR				((__IO uint32_t *)0xE000E400UL)
#define NVIC_IPR0				NVIC_IPR[0]
#define NVIC_IPR1				NVIC_IPR[1]
#define NVIC_IPR2				NVIC_IPR[2]
#define NVIC_IPR3				NVIC_IPR[3]
#define NVIC_IPR4				NVIC_IPR[4]
#define NVIC_IPR5				NVIC_IPR[5]
#define NVIC_IPR6				NVIC_IPR[6]
#define NVIC_IPR7				NVIC_IPR[7]
#define NVIC_IPR8				NVIC_IPR[8]

#define NVIC_STIR				(*(__IO uint32_t *)0xE000EF00UL)

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/

/**
 * @brief Esta función habilita las interrupciones de los perifericos
 * @param peripheral Nombre del periférico
 */
void NVIC_EnableIRQ(IRQn_Type peripheral)
{
	NVIC_ISER[peripheral/32] |= (1UL << peripheral%32);	// Habilito escribiendo un 1 en el bit correspondiente

	/* La expresion "ise_index/32" se indica debido a la estrucutura de los registros.
	 * Estos registros estan divididos en dos*/
}

/**
 * @brief Esta función deshabilita las interrupciones de los periféricos
 * @param peripheral : Nombre del periférico
 */
void NVIC_DisableIRQ(IRQn_Type peripheral)
{
	NVIC_ICER[peripheral/32] |= (1UL << peripheral%32);	// Deshabilito escribiendo un 1 en el bit correspondiente

	/* La expresion "ise_index/32" se indica debido a la estrucutura de los registros.
	 * Estos registros estan divididos en dos*/
}

/**
 * @brief Función booleana para comprobar si una interrupcion está activa
 * @param peripheral : Nombre del periférico
 * @return Devuelve true(1) si la interrupcion está activa.
 * 					false(0) si la interrupcion esta desactivada.
 */
bool NVIC_IsEnabled(IRQn_Type peripheral)
{
	uint32_t temp = NVIC_ISER[peripheral/32];
	return ( !(temp & ~(1UL << peripheral%32)) )? false : true;
}

void NVIC_SetPriority(IRQn_Type peripheral, uint32_t priority)
{
	uint8_t numberIpr=(uint8_t)peripheral/4;
	uint32_t pin=3;
	uint32_t interrupt_number=(uint8_t)peripheral%4;

	if(interrupt_number>0)
	{
		pin+=JUMP_IPR*interrupt_number;
	}
	IPR[numberIpr]=(priority & 0xff);
}

/** TO DO **/
/*
 * NVIC_ISPR
 * NVIC_ICPR
 * NVIC_IABR
 * NVIC_IPR
 * NVIC_STIR
*/
/** FIN TO DO **/

/***********************************************************************************************************************************
 *** FIN DEL MODULO Nvic
 **********************************************************************************************************************************/
