/*
===============================================================================
 Name        : PeripheralControl.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

#include <stdint.h>
#include "PeripheralControl.h"

/**
 * @brief 				Esta funcion enciende o apaga los periféricos
 * @param peripheral 	Idenficador del periférico
 * @param func 			Estado en el cual se quiere configurar el periférico
 */
void PeripheralControl_SetPowerControl(PCONP_t peripheral,ON_OFF_t func)//PCONP
{
	if(func==ON)
		(*PCONP)|=(1UL<<peripheral);
	else //func==OFF
		(*PCONP)&=~(1UL<<peripheral);
}

/**
 * @brief				Esta función devuelve el estado del los periféricos
 * @param peripheral	Idenficador del periférico
 * @return				Estado en el cual se encuentra el periférico
 */
ON_OFF_t PeripheralControl_GetPowerControl(PCONP_t peripheral)
{
	return(((*PCONP) & (1UL<<peripheral)) ? ON : OFF);
}

/**
 * @brief 					Esta funcion selecciona la función del clock del periférico
 * @param peripheral_clock	Identificador del relój del periférico
 * @param func				Función del clock al cual se quiere configurar
 */
void PeripheralControl_SelectClock(PCLK_t peripheral_clock, PCLK_Function_t func)
{
	uint32_t sel=peripheral_clock/16;
	uint32_t shift=((uint32_t)peripheral_clock%16)*2;
	PCLKSEL[sel]&=~(3UL<<shift);
	PCLKSEL[sel]|=((uint32_t)func<<shift);
}
