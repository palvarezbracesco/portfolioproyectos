/****************************************************************************
** Meta object code from reading C++ file 'detector.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../QtTracker/detector.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'detector.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Detector_t {
    QByteArrayData data[15];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Detector_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Detector_t qt_meta_stringdata_Detector = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Detector"
QT_MOC_LITERAL(1, 9, 14), // "DistanciaLista"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 14), // "DetectionReady"
QT_MOC_LITERAL(4, 40, 13), // "DetectedImage"
QT_MOC_LITERAL(5, 54, 18), // "ImprimirDistancias"
QT_MOC_LITERAL(6, 73, 10), // "MoverMotor"
QT_MOC_LITERAL(7, 84, 6), // "byte_t"
QT_MOC_LITERAL(8, 91, 5), // "motor"
QT_MOC_LITERAL(9, 97, 7), // "uint8_t"
QT_MOC_LITERAL(10, 105, 5), // "angle"
QT_MOC_LITERAL(11, 111, 5), // "direc"
QT_MOC_LITERAL(12, 117, 11), // "MoverCamara"
QT_MOC_LITERAL(13, 129, 14), // "ProcesarImagen"
QT_MOC_LITERAL(14, 144, 4) // "Draw"

    },
    "Detector\0DistanciaLista\0\0DetectionReady\0"
    "DetectedImage\0ImprimirDistancias\0"
    "MoverMotor\0byte_t\0motor\0uint8_t\0angle\0"
    "direc\0MoverCamara\0ProcesarImagen\0Draw"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Detector[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   54,    2, 0x06 /* Public */,
       3,    0,   55,    2, 0x06 /* Public */,
       4,    1,   56,    2, 0x06 /* Public */,
       5,    2,   59,    2, 0x06 /* Public */,
       6,    3,   64,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
      12,    0,   71,    2, 0x0a /* Public */,
      13,    1,   72,    2, 0x0a /* Public */,
      14,    0,   75,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QImage,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, 0x80000000 | 7, 0x80000000 | 9, 0x80000000 | 7,    8,   10,   11,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QImage,    2,
    QMetaType::Void,

       0        // eod
};

void Detector::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Detector *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->DistanciaLista(); break;
        case 1: _t->DetectionReady(); break;
        case 2: _t->DetectedImage((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 3: _t->ImprimirDistancias((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->MoverMotor((*reinterpret_cast< byte_t(*)>(_a[1])),(*reinterpret_cast< uint8_t(*)>(_a[2])),(*reinterpret_cast< byte_t(*)>(_a[3]))); break;
        case 5: _t->MoverCamara(); break;
        case 6: _t->ProcesarImagen((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 7: _t->Draw(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (Detector::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Detector::DistanciaLista)) {
                *result = 0;
                return;
            }
        }
        {
            using _t = void (Detector::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Detector::DetectionReady)) {
                *result = 1;
                return;
            }
        }
        {
            using _t = void (Detector::*)(QImage );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Detector::DetectedImage)) {
                *result = 2;
                return;
            }
        }
        {
            using _t = void (Detector::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Detector::ImprimirDistancias)) {
                *result = 3;
                return;
            }
        }
        {
            using _t = void (Detector::*)(byte_t , uint8_t , byte_t );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&Detector::MoverMotor)) {
                *result = 4;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject Detector::staticMetaObject = { {
    &QObject::staticMetaObject,
    qt_meta_stringdata_Detector.data,
    qt_meta_data_Detector,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Detector::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Detector::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Detector.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int Detector::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void Detector::DistanciaLista()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void Detector::DetectionReady()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void Detector::DetectedImage(QImage _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void Detector::ImprimirDistancias(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void Detector::MoverMotor(byte_t _t1, uint8_t _t2, byte_t _t3)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t3))) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
