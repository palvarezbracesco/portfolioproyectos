/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.5.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../QtTracker/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.5.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[37];
    char stringdata0[650];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 14), // "CamaraNewFrame"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 25), // "on_pushButtonModo_pressed"
QT_MOC_LITERAL(4, 53, 27), // "on_pushButtonCamara_pressed"
QT_MOC_LITERAL(5, 81, 30), // "on_pushButtonMicrofono_pressed"
QT_MOC_LITERAL(6, 112, 14), // "ImprimirImagen"
QT_MOC_LITERAL(7, 127, 15), // "ImprimirConsola"
QT_MOC_LITERAL(8, 143, 6), // "string"
QT_MOC_LITERAL(9, 150, 23), // "on_UpdateButton_pressed"
QT_MOC_LITERAL(10, 174, 13), // "error_message"
QT_MOC_LITERAL(11, 188, 5), // "title"
QT_MOC_LITERAL(12, 194, 11), // "description"
QT_MOC_LITERAL(13, 206, 27), // "on_pushButtonCargar_clicked"
QT_MOC_LITERAL(14, 234, 13), // "newConnection"
QT_MOC_LITERAL(15, 248, 12), // "disconnected"
QT_MOC_LITERAL(16, 261, 9), // "readyRead"
QT_MOC_LITERAL(17, 271, 12), // "EnviarAngulo"
QT_MOC_LITERAL(18, 284, 6), // "byte_t"
QT_MOC_LITERAL(19, 291, 5), // "motor"
QT_MOC_LITERAL(20, 297, 7), // "uint8_t"
QT_MOC_LITERAL(21, 305, 5), // "angle"
QT_MOC_LITERAL(22, 311, 5), // "direc"
QT_MOC_LITERAL(23, 317, 14), // "EnviarKeyboard"
QT_MOC_LITERAL(24, 332, 10), // "keyboard_t"
QT_MOC_LITERAL(25, 343, 29), // "on_pushButtonMoverIzq_pressed"
QT_MOC_LITERAL(26, 373, 30), // "on_pushButtonMoverIzq_released"
QT_MOC_LITERAL(27, 404, 32), // "on_pushButtonMoverArriba_pressed"
QT_MOC_LITERAL(28, 437, 33), // "on_pushButtonMoverArriba_rele..."
QT_MOC_LITERAL(29, 471, 29), // "on_pushButtonMoverDer_pressed"
QT_MOC_LITERAL(30, 501, 30), // "on_pushButtonMoverDer_released"
QT_MOC_LITERAL(31, 532, 31), // "on_pushButtonMoverAbajo_pressed"
QT_MOC_LITERAL(32, 564, 32), // "on_pushButtonMoverAbajo_released"
QT_MOC_LITERAL(33, 597, 13), // "keyPressEvent"
QT_MOC_LITERAL(34, 611, 10), // "QKeyEvent*"
QT_MOC_LITERAL(35, 622, 15), // "keyReleaseEvent"
QT_MOC_LITERAL(36, 638, 11) // "CargarBarra"

    },
    "MainWindow\0CamaraNewFrame\0\0"
    "on_pushButtonModo_pressed\0"
    "on_pushButtonCamara_pressed\0"
    "on_pushButtonMicrofono_pressed\0"
    "ImprimirImagen\0ImprimirConsola\0string\0"
    "on_UpdateButton_pressed\0error_message\0"
    "title\0description\0on_pushButtonCargar_clicked\0"
    "newConnection\0disconnected\0readyRead\0"
    "EnviarAngulo\0byte_t\0motor\0uint8_t\0"
    "angle\0direc\0EnviarKeyboard\0keyboard_t\0"
    "on_pushButtonMoverIzq_pressed\0"
    "on_pushButtonMoverIzq_released\0"
    "on_pushButtonMoverArriba_pressed\0"
    "on_pushButtonMoverArriba_released\0"
    "on_pushButtonMoverDer_pressed\0"
    "on_pushButtonMoverDer_released\0"
    "on_pushButtonMoverAbajo_pressed\0"
    "on_pushButtonMoverAbajo_released\0"
    "keyPressEvent\0QKeyEvent*\0keyReleaseEvent\0"
    "CargarBarra"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      26,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  144,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,  145,    2, 0x08 /* Private */,
       4,    0,  146,    2, 0x08 /* Private */,
       5,    0,  147,    2, 0x08 /* Private */,
       6,    1,  148,    2, 0x08 /* Private */,
       7,    2,  151,    2, 0x08 /* Private */,
       7,    1,  156,    2, 0x08 /* Private */,
       9,    0,  159,    2, 0x08 /* Private */,
      10,    2,  160,    2, 0x08 /* Private */,
      13,    0,  165,    2, 0x08 /* Private */,
      14,    0,  166,    2, 0x08 /* Private */,
      15,    0,  167,    2, 0x08 /* Private */,
      16,    0,  168,    2, 0x08 /* Private */,
      17,    3,  169,    2, 0x08 /* Private */,
      23,    1,  176,    2, 0x08 /* Private */,
      25,    0,  179,    2, 0x08 /* Private */,
      26,    0,  180,    2, 0x08 /* Private */,
      27,    0,  181,    2, 0x08 /* Private */,
      28,    0,  182,    2, 0x08 /* Private */,
      29,    0,  183,    2, 0x08 /* Private */,
      30,    0,  184,    2, 0x08 /* Private */,
      31,    0,  185,    2, 0x08 /* Private */,
      32,    0,  186,    2, 0x08 /* Private */,
      33,    1,  187,    2, 0x08 /* Private */,
      35,    1,  190,    2, 0x08 /* Private */,
      36,    0,  193,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QImage,    2,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::QString,    8,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   11,   12,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 18, 0x80000000 | 20, 0x80000000 | 18,   19,   21,   22,
    QMetaType::Void, 0x80000000 | 24,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 34,    2,
    QMetaType::Void, 0x80000000 | 34,    2,
    QMetaType::Void,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->CamaraNewFrame(); break;
        case 1: _t->on_pushButtonModo_pressed(); break;
        case 2: _t->on_pushButtonCamara_pressed(); break;
        case 3: _t->on_pushButtonMicrofono_pressed(); break;
        case 4: _t->ImprimirImagen((*reinterpret_cast< QImage(*)>(_a[1]))); break;
        case 5: _t->ImprimirConsola((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->ImprimirConsola((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->on_UpdateButton_pressed(); break;
        case 8: _t->error_message((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2]))); break;
        case 9: _t->on_pushButtonCargar_clicked(); break;
        case 10: _t->newConnection(); break;
        case 11: _t->disconnected(); break;
        case 12: _t->readyRead(); break;
        case 13: _t->EnviarAngulo((*reinterpret_cast< byte_t(*)>(_a[1])),(*reinterpret_cast< uint8_t(*)>(_a[2])),(*reinterpret_cast< byte_t(*)>(_a[3]))); break;
        case 14: _t->EnviarKeyboard((*reinterpret_cast< keyboard_t(*)>(_a[1]))); break;
        case 15: _t->on_pushButtonMoverIzq_pressed(); break;
        case 16: _t->on_pushButtonMoverIzq_released(); break;
        case 17: _t->on_pushButtonMoverArriba_pressed(); break;
        case 18: _t->on_pushButtonMoverArriba_released(); break;
        case 19: _t->on_pushButtonMoverDer_pressed(); break;
        case 20: _t->on_pushButtonMoverDer_released(); break;
        case 21: _t->on_pushButtonMoverAbajo_pressed(); break;
        case 22: _t->on_pushButtonMoverAbajo_released(); break;
        case 23: _t->keyPressEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        case 24: _t->keyReleaseEvent((*reinterpret_cast< QKeyEvent*(*)>(_a[1]))); break;
        case 25: _t->CargarBarra(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (MainWindow::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&MainWindow::CamaraNewFrame)) {
                *result = 0;
            }
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 26)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 26;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 26)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 26;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::CamaraNewFrame()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}
QT_END_MOC_NAMESPACE
