/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tabConfiguracion;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *UpdateButton;
    QSpacerItem *verticalSpacer_3;
    QGridLayout *gridLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QComboBox *TypeOfDetection;
    QLabel *labelDetectar;
    QComboBox *MicIndex;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_4;
    QSpacerItem *verticalSpacer_5;
    QGridLayout *gridLayout_4;
    QRadioButton *CamaraIpButton;
    QLabel *label;
    QRadioButton *CamaraWebButton;
    QComboBox *CamaraWebIndex;
    QSpacerItem *horizontalSpacer_5;
    QLineEdit *CamaraIpName;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *verticalSpacer_4;
    QGridLayout *gridLayout_5;
    QLineEdit *PortLineEdit;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_7;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_8;
    QSpacerItem *verticalSpacer_2;
    QGridLayout *gridLayout_2;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *pushButtonCargar;
    QProgressBar *progressBar;
    QSpacerItem *horizontalSpacer_10;
    QSpacerItem *verticalSpacer;
    QWidget *tabCamara;
    QVBoxLayout *verticalLayout_2;
    QGraphicsView *DetecterCam;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout;
    QPushButton *pushButtonMoverIzq;
    QPushButton *pushButtonMoverArriba;
    QPushButton *pushButtonMoverAbajo;
    QPushButton *pushButtonMoverDer;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *horizontalSpacer_13;
    QSpacerItem *verticalSpacer_6;
    QSpacerItem *verticalSpacer_7;
    QSpacerItem *horizontalSpacer_14;
    QPushButton *pushButtonModo;
    QPushButton *pushButtonCamara;
    QPushButton *pushButtonMicrofono;
    QSpacerItem *horizontalSpacer_2;
    QWidget *tabConsola;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QPlainTextEdit *Consola;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->resize(566, 600);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMaximumSize(QSize(999999, 999999));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(136, 138, 133, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(204, 207, 200, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(170, 172, 166, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(68, 69, 66, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(91, 92, 89, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush7(QColor(195, 196, 194, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush7);
        QBrush brush8(QColor(255, 255, 220, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush9(QColor(0, 0, 0, 128));
        brush9.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush9);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush9);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush9);
        MainWindow->setPalette(palette);
        MainWindow->setAcceptDrops(false);
        QIcon icon;
        icon.addFile(QStringLiteral("../Icons/trackericon.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setAnimated(true);
        MainWindow->setDockNestingEnabled(true);
        MainWindow->setDockOptions(QMainWindow::AllowNestedDocks|QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks);
        MainWindow->setUnifiedTitleAndToolBarOnMac(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setTabPosition(QTabWidget::North);
        tabWidget->setTabShape(QTabWidget::Rounded);
        tabConfiguracion = new QWidget();
        tabConfiguracion->setObjectName(QStringLiteral("tabConfiguracion"));
        verticalLayout_3 = new QVBoxLayout(tabConfiguracion);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_11);

        UpdateButton = new QPushButton(tabConfiguracion);
        UpdateButton->setObjectName(QStringLiteral("UpdateButton"));
        UpdateButton->setMaximumSize(QSize(20, 20));
        UpdateButton->setIconSize(QSize(10, 10));

        horizontalLayout->addWidget(UpdateButton);


        verticalLayout_3->addLayout(horizontalLayout);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_3);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setSpacing(6);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_3, 1, 0, 1, 1);

        TypeOfDetection = new QComboBox(tabConfiguracion);
        TypeOfDetection->setObjectName(QStringLiteral("TypeOfDetection"));

        gridLayout_3->addWidget(TypeOfDetection, 1, 2, 1, 1);

        labelDetectar = new QLabel(tabConfiguracion);
        labelDetectar->setObjectName(QStringLiteral("labelDetectar"));
        QFont font;
        font.setUnderline(false);
        font.setKerning(true);
        labelDetectar->setFont(font);

        gridLayout_3->addWidget(labelDetectar, 1, 1, 1, 1);

        MicIndex = new QComboBox(tabConfiguracion);
        MicIndex->setObjectName(QStringLiteral("MicIndex"));

        gridLayout_3->addWidget(MicIndex, 4, 2, 1, 1);

        label_2 = new QLabel(tabConfiguracion);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout_3->addWidget(label_2, 4, 1, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_4, 1, 3, 1, 1);


        verticalLayout_3->addLayout(gridLayout_3);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_5);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setSpacing(0);
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        gridLayout_4->setSizeConstraint(QLayout::SetDefaultConstraint);
        gridLayout_4->setContentsMargins(0, -1, -1, -1);
        CamaraIpButton = new QRadioButton(tabConfiguracion);
        CamaraIpButton->setObjectName(QStringLiteral("CamaraIpButton"));

        gridLayout_4->addWidget(CamaraIpButton, 1, 2, 1, 1);

        label = new QLabel(tabConfiguracion);
        label->setObjectName(QStringLiteral("label"));

        gridLayout_4->addWidget(label, 0, 1, 1, 1);

        CamaraWebButton = new QRadioButton(tabConfiguracion);
        CamaraWebButton->setObjectName(QStringLiteral("CamaraWebButton"));

        gridLayout_4->addWidget(CamaraWebButton, 2, 2, 1, 1);

        CamaraWebIndex = new QComboBox(tabConfiguracion);
        CamaraWebIndex->setObjectName(QStringLiteral("CamaraWebIndex"));

        gridLayout_4->addWidget(CamaraWebIndex, 2, 3, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_5, 1, 4, 1, 1);

        CamaraIpName = new QLineEdit(tabConfiguracion);
        CamaraIpName->setObjectName(QStringLiteral("CamaraIpName"));
        CamaraIpName->setMaximumSize(QSize(200, 50));

        gridLayout_4->addWidget(CamaraIpName, 1, 3, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_6, 1, 0, 1, 1);


        verticalLayout_3->addLayout(gridLayout_4);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_4);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setSpacing(6);
        gridLayout_5->setObjectName(QStringLiteral("gridLayout_5"));
        PortLineEdit = new QLineEdit(tabConfiguracion);
        PortLineEdit->setObjectName(QStringLiteral("PortLineEdit"));
        PortLineEdit->setMaximumSize(QSize(200, 50));

        gridLayout_5->addWidget(PortLineEdit, 1, 3, 1, 1);

        label_3 = new QLabel(tabConfiguracion);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_5->addWidget(label_3, 0, 1, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_7, 0, 0, 1, 1);

        label_4 = new QLabel(tabConfiguracion);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout_5->addWidget(label_4, 1, 2, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_8, 1, 4, 1, 1);


        verticalLayout_3->addLayout(gridLayout_5);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_9, 1, 2, 1, 1);

        pushButtonCargar = new QPushButton(tabConfiguracion);
        pushButtonCargar->setObjectName(QStringLiteral("pushButtonCargar"));
        QPalette palette1;
        QBrush brush10(QColor(46, 52, 54, 255));
        brush10.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush10);
        QBrush brush11(QColor(211, 215, 207, 255));
        brush11.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush11);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush11);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush11);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush10);
        QBrush brush12(QColor(0, 0, 0, 102));
        brush12.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::Highlight, brush12);
        palette1.setBrush(QPalette::Active, QPalette::HighlightedText, brush10);
        QBrush brush13(QColor(26, 27, 27, 255));
        brush13.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush13);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        QBrush brush14(QColor(255, 255, 255, 128));
        brush14.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Active, QPalette::PlaceholderText, brush14);
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush11);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::Highlight, brush12);
        palette1.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush10);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush13);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush14);
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush11);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush10);
        palette1.setBrush(QPalette::Disabled, QPalette::Highlight, brush12);
        palette1.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush10);
        QBrush brush15(QColor(53, 54, 55, 255));
        brush15.setStyle(Qt::SolidPattern);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush15);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush14);
        pushButtonCargar->setPalette(palette1);

        gridLayout_2->addWidget(pushButtonCargar, 1, 1, 1, 1);

        progressBar = new QProgressBar(tabConfiguracion);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setAutoFillBackground(true);
        progressBar->setValue(0);

        gridLayout_2->addWidget(progressBar, 2, 1, 1, 1);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_10, 1, 0, 1, 1);


        verticalLayout_3->addLayout(gridLayout_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        tabWidget->addTab(tabConfiguracion, QString());
        tabCamara = new QWidget();
        tabCamara->setObjectName(QStringLiteral("tabCamara"));
        verticalLayout_2 = new QVBoxLayout(tabCamara);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        DetecterCam = new QGraphicsView(tabCamara);
        DetecterCam->setObjectName(QStringLiteral("DetecterCam"));
        DetecterCam->setMinimumSize(QSize(230, 320));

        verticalLayout_2->addWidget(DetecterCam);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pushButtonMoverIzq = new QPushButton(tabCamara);
        pushButtonMoverIzq->setObjectName(QStringLiteral("pushButtonMoverIzq"));
        pushButtonMoverIzq->setEnabled(true);
        sizePolicy.setHeightForWidth(pushButtonMoverIzq->sizePolicy().hasHeightForWidth());
        pushButtonMoverIzq->setSizePolicy(sizePolicy);
        pushButtonMoverIzq->setMaximumSize(QSize(40, 40));
        QIcon icon1;
        icon1.addFile(QStringLiteral("../Icons/botonizquierda.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButtonMoverIzq->setIcon(icon1);
        pushButtonMoverIzq->setIconSize(QSize(20, 20));

        gridLayout->addWidget(pushButtonMoverIzq, 1, 1, 1, 1);

        pushButtonMoverArriba = new QPushButton(tabCamara);
        pushButtonMoverArriba->setObjectName(QStringLiteral("pushButtonMoverArriba"));
        pushButtonMoverArriba->setEnabled(true);
        sizePolicy.setHeightForWidth(pushButtonMoverArriba->sizePolicy().hasHeightForWidth());
        pushButtonMoverArriba->setSizePolicy(sizePolicy);
        pushButtonMoverArriba->setMaximumSize(QSize(40, 40));
        pushButtonMoverArriba->setText(QStringLiteral(""));
        QIcon icon2;
        icon2.addFile(QStringLiteral("../Icons/flechaarriba.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButtonMoverArriba->setIcon(icon2);
        pushButtonMoverArriba->setIconSize(QSize(20, 20));

        gridLayout->addWidget(pushButtonMoverArriba, 0, 2, 1, 1);

        pushButtonMoverAbajo = new QPushButton(tabCamara);
        pushButtonMoverAbajo->setObjectName(QStringLiteral("pushButtonMoverAbajo"));
        pushButtonMoverAbajo->setEnabled(true);
        sizePolicy.setHeightForWidth(pushButtonMoverAbajo->sizePolicy().hasHeightForWidth());
        pushButtonMoverAbajo->setSizePolicy(sizePolicy);
        pushButtonMoverAbajo->setMaximumSize(QSize(40, 40));
        QIcon icon3;
        icon3.addFile(QStringLiteral("../Icons/botonabajo.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButtonMoverAbajo->setIcon(icon3);
        pushButtonMoverAbajo->setIconSize(QSize(20, 20));

        gridLayout->addWidget(pushButtonMoverAbajo, 2, 2, 1, 1);

        pushButtonMoverDer = new QPushButton(tabCamara);
        pushButtonMoverDer->setObjectName(QStringLiteral("pushButtonMoverDer"));
        pushButtonMoverDer->setEnabled(true);
        sizePolicy.setHeightForWidth(pushButtonMoverDer->sizePolicy().hasHeightForWidth());
        pushButtonMoverDer->setSizePolicy(sizePolicy);
        pushButtonMoverDer->setMaximumSize(QSize(40, 40));
        QIcon icon4;
        icon4.addFile(QStringLiteral("../Icons/botonderecha.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButtonMoverDer->setIcon(icon4);
        pushButtonMoverDer->setIconSize(QSize(20, 20));

        gridLayout->addWidget(pushButtonMoverDer, 1, 3, 1, 1);

        horizontalSpacer_12 = new QSpacerItem(40, 40, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_12, 0, 3, 1, 1);

        horizontalSpacer_13 = new QSpacerItem(40, 40, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_13, 2, 1, 1, 1);

        verticalSpacer_6 = new QSpacerItem(40, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_6, 2, 3, 1, 1);

        verticalSpacer_7 = new QSpacerItem(40, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_7, 0, 1, 1, 1);

        horizontalSpacer_14 = new QSpacerItem(40, 40, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_14, 1, 2, 1, 1);


        horizontalLayout_3->addLayout(gridLayout);

        pushButtonModo = new QPushButton(tabCamara);
        pushButtonModo->setObjectName(QStringLiteral("pushButtonModo"));
        pushButtonModo->setEnabled(true);
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(pushButtonModo->sizePolicy().hasHeightForWidth());
        pushButtonModo->setSizePolicy(sizePolicy1);
        pushButtonModo->setMinimumSize(QSize(100, 100));
        pushButtonModo->setMaximumSize(QSize(100, 100));

        horizontalLayout_3->addWidget(pushButtonModo);

        pushButtonCamara = new QPushButton(tabCamara);
        pushButtonCamara->setObjectName(QStringLiteral("pushButtonCamara"));
        pushButtonCamara->setEnabled(true);
        sizePolicy1.setHeightForWidth(pushButtonCamara->sizePolicy().hasHeightForWidth());
        pushButtonCamara->setSizePolicy(sizePolicy1);
        pushButtonCamara->setMinimumSize(QSize(100, 100));
        pushButtonCamara->setMaximumSize(QSize(100, 100));
        QIcon icon5;
        icon5.addFile(QStringLiteral("../Icons/webcam.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButtonCamara->setIcon(icon5);
        pushButtonCamara->setIconSize(QSize(20, 20));
        pushButtonCamara->setCheckable(false);
        pushButtonCamara->setChecked(false);

        horizontalLayout_3->addWidget(pushButtonCamara);

        pushButtonMicrofono = new QPushButton(tabCamara);
        pushButtonMicrofono->setObjectName(QStringLiteral("pushButtonMicrofono"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pushButtonMicrofono->sizePolicy().hasHeightForWidth());
        pushButtonMicrofono->setSizePolicy(sizePolicy2);
        pushButtonMicrofono->setMinimumSize(QSize(100, 100));
        pushButtonMicrofono->setMaximumSize(QSize(100, 100));
        QIcon icon6;
        icon6.addFile(QStringLiteral("../Icons/microfono.png"), QSize(), QIcon::Normal, QIcon::Off);
        pushButtonMicrofono->setIcon(icon6);
        pushButtonMicrofono->setIconSize(QSize(20, 20));

        horizontalLayout_3->addWidget(pushButtonMicrofono);

        horizontalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_3);

        tabWidget->addTab(tabCamara, QString());
        tabConsola = new QWidget();
        tabConsola->setObjectName(QStringLiteral("tabConsola"));
        verticalLayout_5 = new QVBoxLayout(tabConsola);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        Consola = new QPlainTextEdit(tabConsola);
        Consola->setObjectName(QStringLiteral("Consola"));

        verticalLayout_4->addWidget(Consola);


        verticalLayout_5->addLayout(verticalLayout_4);

        tabWidget->addTab(tabConsola, QString());

        verticalLayout->addWidget(tabWidget);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Tracker", 0));
        UpdateButton->setText(QApplication::translate("MainWindow", "\342\237\263", 0));
        TypeOfDetection->clear();
        TypeOfDetection->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "Personas", 0)
        );
        labelDetectar->setText(QApplication::translate("MainWindow", "Objeto a detectar", 0));
        label_2->setText(QApplication::translate("MainWindow", "Micr\303\263fono", 0));
        CamaraIpButton->setText(QApplication::translate("MainWindow", "C\303\241mara IP", 0));
        label->setText(QApplication::translate("MainWindow", "C\303\241mara:", 0));
        CamaraWebButton->setText(QApplication::translate("MainWindow", "C\303\241mara Web", 0));
        label_3->setText(QApplication::translate("MainWindow", "Servidor", 0));
        label_4->setText(QApplication::translate("MainWindow", "Puerto:", 0));
        pushButtonCargar->setText(QApplication::translate("MainWindow", "Cargar configuraci\303\263n", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabConfiguracion), QApplication::translate("MainWindow", "Configuraci\303\263n", 0));
        pushButtonMoverIzq->setText(QString());
        pushButtonMoverAbajo->setText(QString());
        pushButtonMoverDer->setText(QString());
        pushButtonModo->setText(QApplication::translate("MainWindow", "Manual", 0));
        pushButtonCamara->setText(QString());
        pushButtonMicrofono->setText(QString());
        tabWidget->setTabText(tabWidget->indexOf(tabCamara), QApplication::translate("MainWindow", "C\303\241mara", 0));
        tabWidget->setTabText(tabWidget->indexOf(tabConsola), QApplication::translate("MainWindow", "Consola", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
