#include "configuracion.h"

Configuracion::Configuracion(QObject *parent) : QObject(parent)
{

}
void Configuracion::SetDetectType(int tipo)
{
    this->TipoDeDeteccion=tipo;
}

int Configuracion::GetDetectType(void)const
{
    return this->TipoDeDeteccion;
}

void Configuracion::SetCameraName(QString NewName)
{
    CameraName = NewName;
    return;
}

QString Configuracion::GetCameraName(void)
{
    return CameraName;
}
