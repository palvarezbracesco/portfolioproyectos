#include "camara.h"
#include <string>
#include <QTimer>

#define CAMERA_TITLE_MSG "Error en camera"
#define CAMERA_DISCONNECTED_MSG "Se ha desconectado la cámara"
#define CAMERA_MISSING_MSG  "No hay cámara seleccionada"
#define CAMERA_CANT_OPEN "No se puede abrir la cámara"

Camara::Camara()
{
    video.set(cv::CAP_PROP_BUFFERSIZE, 1);
}

Camara::~Camara(void)
{
}

void Camara::Close(void)
{
    if(video.isOpened())
    {
        video.release();
    }
    timer->stop();
    timer->deleteLater();
}

bool Camara::Open(QString name)
{
    if (name.isEmpty())
    {
        emit Camera_Error(CAMERA_TITLE_MSG,CAMERA_MISSING_MSG);
        return false;
    }

    if(video.isOpened())
    {
        video.release();
    }

    if(!video.open(name.toStdString()))
    {
        emit Camera_Error(CAMERA_TITLE_MSG, CAMERA_CANT_OPEN);
        return false;
    }

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(FrameRequest()));
    timer->start();

    return true;
}

void Camara::FrameRequest()
{
    cv::Mat frame;

    if(!video.isOpened())
    {
        timer->stop();
        timer->deleteLater();
        emit Camera_Error(CAMERA_TITLE_MSG, CAMERA_DISCONNECTED_MSG);
        return;
    }

    if(video.read(frame))
    {
        QImage image( frame.data,
                      frame.cols,
                      frame.rows,
                      static_cast<int>(frame.step),
                      QImage::Format_RGB888);

        emit Frame(image);
    }

}
