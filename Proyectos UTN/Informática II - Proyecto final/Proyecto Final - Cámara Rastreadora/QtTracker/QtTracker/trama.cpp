#include <stdbool.h>
#include <stdint.h>
#include <string>
#include "trama.h"

Trama::Trama()
{

}


void Trama::Analizar_Elemento(char elemento)
{
    static int i;
    if(Trama_Vacia == true)
    {
        if(elemento == BYTE_INICIO)
        {
            trama[INICIO] = BYTE_INICIO;
            Trama_Vacia = false;
            i=FUNCION;
        }
    }

    else
    {
        trama[i] = elemento;
        i++;

        if(i == MAX_ELEMENTOS)
        {
            Procesar_Trama();
            Trama_Vacia =true;
        }

    }

    return;

}

bool Trama::Procesar_Trama(void)
{

    if(trama[INICIO] != BYTE_INICIO || trama[FIN]!= BYTE_FIN )
    {
        return false;
    }

    switch(trama[FUNCION])
    {
    case KEYBOARD:
        emit(KeybardChange(trama[PARAMETRO]));
        break;

    default:
        break;
    }

    return false;
}

bool Trama::Procesar_Trama(QString nuevo)
{

    if(nuevo[INICIO] != BYTE_INICIO || nuevo[FIN]!= BYTE_FIN )
    {
        return false;
    }

    QString aux = QString(nuevo[FUNCION]);
    int select = aux.toInt();

    switch(select)
    {
        case KEYBOARD:
        {
            std::string s = nuevo.toStdString();
            emit(KeybardChange(static_cast<unsigned char>(s[PARAMETRO])));
            break;
        }
        default:
            break;
    }

    return false;
}
