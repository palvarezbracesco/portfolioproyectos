#include "detector.h"

#include "opencv2/opencv.hpp"

#include <QImage>
#include <QTimer>

#include <math.h>

#define OFF false
#define ON true
#define MAX_PIXEL_X 640
#define MAX_PIXEL_Y 480

#define MECANICAL_DELAY 100 // En milisegundos
#define CAM_DELAY 1000 //En milisegundos
#define CAM_INTERVAL 50

Detector::Detector(QObject *parent) : QObject(parent)
{
    Distance = 5;
    detector_state = OFF;
    cascade.load("../haarcascades/haarcascade_frontalface_alt.xml");

    timer = new QTimer(this);
    timer->setSingleShot(ON);
    timer->setInterval(CAM_INTERVAL);

    pidx = new PID( CAM_INTERVAL, 50, -50, 0.1, 0.1, 0); //Inicializo los pid coloco 0 en la integral

    pidy = new PID( CAM_INTERVAL, 50, -50, 0.1, 0.1, 0); //Inicializo los pid coloco 0 en la integral

    connect(this,SIGNAL(DetectionReady()) ,this, SLOT(Draw()));
    connect(this,SIGNAL(DistanciaLista()),this, SLOT(MoverCamara()));
}

/**
 * @brief Detector::ProcesarImagen Funcion que procesa la imagen
 * @param im Imagen recibida
 */
void Detector::ProcesarImagen(QImage im)
{
    cv::Mat view(im.height(),im.width(),CV_8UC3, (void *)(im.constBits()), static_cast<size_t>(im.bytesPerLine()));
    cv::cvtColor(view, frame, cv::COLOR_RGB2BGR);


    if (detector_state == ON) //Esta variable es ON cuandod el modo está en automatico
    {
        if(!timer->isActive())
        {
            timer->start();
            CascadeDetect(); //Hace la deteccion

            if(!detections.empty()) //Si detecto algo
            {
                emit(ImprimirDistancias(detections[0].x,detections[0].y)); //Para imprimir en cosola
                MoverCamara();
            }

        }
    }

    emit DetectionReady();
}

void Detector::CascadeDetect(void)
{
    using namespace cv;

    cvtColor( frame, gray, COLOR_BGR2GRAY); // Convierte en blanco y negro

    equalizeHist( gray, gray );

    cascade.detectMultiScale( gray,
                              detections,
                              1.1,
                              3,
                              0|CASCADE_SCALE_IMAGE,
                              Size(30, 30));

    return;
}

void Detector::Draw(void)
{
    using namespace cv;

    if(detector_state == ON)
    {
        if(!detections.empty())
        {
            Scalar color = Scalar(255, 0, 0); //Color del rectangulo

            rectangle(  frame,
                        cvPoint(detections[0].x, detections[0].y),
                        cvPoint(cvRound((detections[0].x + detections[0].width)),
                                cvRound((detections[0].y + detections[0].height-1))),
                        color,
                        3,
                        8,
                        0); //Funcion que dibuja un rectangulo
        }
    }

    QImage im(  frame.data,
                frame.cols,
                frame.rows,
                static_cast<int>(frame.step),
                QImage::Format_RGB888); //Convierto el frame en QImage

    emit DetectedImage(im); //Imagen lista para imprimir en la GUI

    return;
}

void Detector::MoverCamara(void)
{
    int x_proportional;
    int y_proportional;

    x_proportional = int(pidx->calculate(detections[0].x + detections[0].width/2 ,
                        frame.cols/2)); //Controlador pid de la posicion en x

    y_proportional = int(pidy->calculate(detections[0].y + detections[0].height/2,
                        frame.rows/2)); //Controlador pid de la posicion en y

    if (x_proportional > 0)
    {
        emit(MoverMotor( PAN, uint8_t(x_proportional), DERECHA));
    }

    if (x_proportional < 0)
    {
        x_proportional = abs(x_proportional);

        emit(MoverMotor( PAN, uint8_t(x_proportional), IZQUIERDA));
    }

    if (y_proportional > 0)
    {
        emit(MoverMotor( TILT, uint8_t(y_proportional), ABAJO));
    }

    if (y_proportional < 0)
    {
        y_proportional = abs(y_proportional);

        emit(MoverMotor( TILT, uint8_t(y_proportional), ARRIBA));
    }
}

void Detector::SetDetectorState(bool State)
{
    detector_state = State;
}
