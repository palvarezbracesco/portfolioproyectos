#ifndef CAMARA_H
#define CAMARA_H

#include <QImage>
#include "opencv2/opencv.hpp"

class Camara : public QObject
{
    Q_OBJECT
public:
    Camara(void);
    ~Camara(void);

public:
    void Close(void);
    bool Open(QString);

private:
    cv::VideoCapture video;
    QTimer *timer;

public slots:
    void FrameRequest();

signals:
    void Camera_Error(QString,QString);
    void Frame(QImage);
};

#endif // CAMARA_H
