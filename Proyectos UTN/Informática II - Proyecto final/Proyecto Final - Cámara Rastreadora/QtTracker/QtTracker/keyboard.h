#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <QObject>

typedef enum {
    KEY_UP,
    KEY_DOWN,
    KEY_RIGHT,
    KEY_LEFT,
    KEY_CAMERA,
    KEY_MIC,
    KEY_MODE,
    KEY_UNDEFINED,
    MAX_TECLAS,
    NO_KEY = -1
} Key_t;

typedef char keyboard_t;

class Keyboard : public QObject
{
    Q_OBJECT

public:
    Keyboard();


private:
    keyboard_t keyboard;
    keyboard_t external_keyboard;
    keyboard_t internal_keyboard;
public slots:

    void NewExternalKeyboard(keyboard_t );
    void Keyboard_Key_SetOn(Key_t);
    void Keyboard_Key_SetOff(Key_t);
    void Keyboard_Mode_SetOff(void);

signals:

    void Keyboard_Changed(keyboard_t);
};

#endif // KEYBOARD_H
