#include <QString>
#include <QtCore>
#include <QThread>
#include <QMessageBox>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "keyboard.h"

#define OFF 0
#define ON 1

#define manual 0
#define automatico 1

static bool flag_microfono=OFF;
static bool flag_camara=OFF;
static bool flag_modo=manual;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    server = nullptr;
    socket = nullptr;

    connect(&camara,
            SIGNAL(Frame(QImage)),
            &detector,
            SLOT(ProcesarImagen(QImage)));// Conecto el nuevo frame de la camara con el detector

    connect(&detector,
            SIGNAL(DetectedImage(QImage)),
            this,
            SLOT(ImprimirImagen(QImage))); //Cuando se procesó la imagen imprimo la imagen

    connect(&detector,
            SIGNAL(ImprimirDistancias(int,int)),
            this,
            SLOT(ImprimirConsola(int,int))); //Solo para mostrar algo en consola

    connect(&trama,
            SIGNAL(NuevaTrama(QString)),
            this,
            SLOT(ImprimirConsola(QString))); //Solo para mostrar que se recibió trama

    connect(&keyboard,
            SIGNAL(Keyboard_Changed(keyboard_t)),
            this,
            SLOT(EnviarKeyboard(keyboard_t))); //Cuando tocas alguna tecla de pantalla envía al stick

    connect(&detector,
            SIGNAL(MoverMotor(byte_t, uint8_t, byte_t)),
            this,
            SLOT(EnviarAngulo(byte_t, uint8_t, byte_t)));  //Envia trama

    //**********************************MENSAJES DE ERROR***************************************/

    connect(&camara,
            SIGNAL(Camera_Error(QString,QString)),
            this,
            SLOT(error_message(QString,QString))); //Imprime errores

    //******************************************************************************************/

    ui->DetecterCam->setScene(new QGraphicsScene(this));
    ui->DetecterCam->scene()->addItem(&pixmap);
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newConnection(void)
{
    if(!socket)
    {
        socket = new QTcpSocket;
        socket = server->nextPendingConnection();
        connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
        connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    }
}

void MainWindow::disconnected()
{
    socket->close();
    delete socket;
    socket = nullptr;

    error_message("Desconectado", "El módulo wifi se ha desconectado");
}

void MainWindow::readyRead(void)
{
    QByteArray buff;

    buff = socket->read(socket->bytesAvailable());

    QString str = QString(buff);

    ImprimirConsola("Trama recibida:" + str);

    for (int i = 0; i < buff.size();i++)
    {
        trama.Analizar_Elemento(buff[i]);
    }
}

int MainWindow:: getOpcionDeteccion (void)
{
    int tipo_deteccion=EMPTY;

    tipo_deteccion = ui->TypeOfDetection->currentIndex();

    return tipo_deteccion;
}

void MainWindow::on_pushButtonCargar_clicked(void)
{
    //Cargo el nombre de la cámara a la configuracion
    CargarBarra();

    if(ui->CamaraIpButton->isChecked())
    {
        configuracionActual.SetCameraName(ui->CamaraIpName->text());
    }
    else if(ui->CamaraWebButton->isChecked())
    {
        QList<QCameraInfo> cameras = QCameraInfo::availableCameras();

        if(ui->CamaraWebIndex->currentIndex() != -1)
        {
            configuracionActual.SetCameraName(cameras[ui->CamaraWebIndex->currentIndex()].deviceName());
            //std::cout << (cameras[ui->CamaraWebIndex->currentIndex()].deviceName()).toStdString();
        }
    }

    quint16 port = static_cast < quint16 > ( ui->PortLineEdit->text().toInt() ) ;

    if( !server )
    {
        server = new QTcpServer(this);
        connect(server, SIGNAL(newConnection()),this, SLOT(newConnection()) );

        server->listen(QHostAddress::Any, port);

    }
}

void MainWindow::CargarBarra(void)
{
    static int i = 0;

    if(i != 100)
    {
        i++;
        ui->progressBar->setValue(i);
        QTimer::singleShot(10,this,SLOT(CargarBarra()));
    }
    else
    {
        i=0;
    }
}

void MainWindow::on_pushButtonModo_pressed()
{
    if(flag_modo==manual)
    {
        ui->pushButtonModo->setText("Automático");
        keyboard.Keyboard_Key_SetOn(KEY_MODE);
        QTimer::singleShot(100,&keyboard, SLOT(Keyboard_Mode_SetOff()));
        detector.SetDetectorState(true);
        ui->pushButtonMoverArriba->setEnabled(false);
        ui->pushButtonMoverIzq->setEnabled(false);
        ui->pushButtonMoverDer->setEnabled(false);
        ui->pushButtonMoverAbajo->setEnabled(false);

        flag_modo = automatico;
    }
    else
    {
        ui->pushButtonModo->setText("Manual");
        keyboard.Keyboard_Key_SetOn(KEY_MODE);
        QTimer::singleShot(100,&keyboard, SLOT(Keyboard_Mode_SetOff()));
        detector.SetDetectorState(false);
        ui->pushButtonMoverArriba->setEnabled(true);
        ui->pushButtonMoverIzq->setEnabled(true);
        ui->pushButtonMoverDer->setEnabled(true);
        ui->pushButtonMoverAbajo->setEnabled(true);

        flag_modo = manual;
    }
}

void MainWindow::on_pushButtonCamara_pressed()
{
    if(flag_camara==OFF)
    {
        if(camara.Open(configuracionActual.GetCameraName()) == true)
        {
            keyboard.Keyboard_Key_SetOn(KEY_CAMERA);
            ui->pushButtonCamara->setCheckable(true);
            flag_camara=ON;
        }
    }
    else
    {
        keyboard.Keyboard_Key_SetOff(KEY_CAMERA);
        camara.Close();
        ui->pushButtonCamara->setCheckable(false);
        flag_camara=OFF;
    }
}

void MainWindow::on_pushButtonMicrofono_pressed()
{
    if(flag_microfono==OFF)
    {
        keyboard.Keyboard_Key_SetOn(KEY_MIC);
        ui->pushButtonMicrofono->setCheckable(false);
        flag_microfono=ON;
    }
    else
    {
        keyboard.Keyboard_Key_SetOff(KEY_MIC);
        ui->pushButtonMicrofono->setCheckable(true);
        flag_microfono=OFF;
    }
}

void MainWindow::ImprimirImagen(QImage image)
{
    cv::Mat frame;

    pixmap.setPixmap( QPixmap::fromImage(image));
    ui->DetecterCam->fitInView(&pixmap, Qt::KeepAspectRatio);

}

void MainWindow::ImprimirConsola(int x,int y)
{
   // static const int distance = 55;

    QString message;

    message = "Xpos: ";
    message += QString::number(x);
    message += "\t Ypos: ";
    message += QString::number(y);

    ui->Consola->appendPlainText(message);
    return;
}
void MainWindow::ImprimirConsola(QString string)
{
    ui->Consola->appendPlainText(string);
    return;
}

void MainWindow::on_UpdateButton_pressed()
{
    ui->CamaraWebIndex->clear();

    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();

    ui->CamaraWebIndex->addItem(cameras[0].description());

    QList<QAudioDeviceInfo> microfonos = QAudioDeviceInfo::availableDevices(QAudio::AudioInput);

    ui->MicIndex->clear();

    ui->MicIndex->addItem(microfonos[0].deviceName());
}

void MainWindow::error_message(QString title, QString description)
{
    QMessageBox::information(this,title,description );
}

void MainWindow::EnviarKeyboard(keyboard_t keyboard)
{
    if(socket)
    {
        QByteArray array;
        array.append(BYTE_INICIO);
        array.append(KEYBOARD);
        array.append(static_cast<char>(keyboard));
        array.append(BYTE_FIN);
        socket->write(array);
    }
}

void MainWindow::EnviarAngulo(byte_t motor, uint8_t angle, byte_t direc)
{
    if(socket)
    {
        QByteArray array;
        switch (motor)
        {
        case PAN:

            array.append(BYTE_INICIO);
            array.append(PAN);
            array.append(static_cast<char>(angle));
            array.append(direc);
            array.append(BYTE_FIN);

            break;

        case TILT:

            array.append(BYTE_INICIO);
            array.append(TILT);
            array.append(static_cast<char>(angle));
            array.append(direc);
            array.append(BYTE_FIN);

            break;

        default:
            break;
        }


        socket->write(array);
    }
}

void MainWindow::on_pushButtonMoverIzq_pressed()
{
    keyboard.Keyboard_Key_SetOn(KEY_LEFT);
}

void MainWindow::on_pushButtonMoverIzq_released()
{
    keyboard.Keyboard_Key_SetOff(KEY_LEFT);
}

void MainWindow::on_pushButtonMoverArriba_pressed()
{
    keyboard.Keyboard_Key_SetOn(KEY_UP);
}

void MainWindow::on_pushButtonMoverArriba_released()
{
    keyboard.Keyboard_Key_SetOff(KEY_UP);
}

void MainWindow::on_pushButtonMoverDer_pressed()
{
    keyboard.Keyboard_Key_SetOn(KEY_RIGHT);
}

void MainWindow::on_pushButtonMoverDer_released()
{
    keyboard.Keyboard_Key_SetOff(KEY_RIGHT);
}

void MainWindow::on_pushButtonMoverAbajo_pressed()
{
    keyboard.Keyboard_Key_SetOn(KEY_DOWN);
}

void MainWindow::on_pushButtonMoverAbajo_released()
{
    keyboard.Keyboard_Key_SetOff(KEY_DOWN);
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(flag_modo == manual)
    {
        if(event->key() == Qt::Key_A)
        {
            keyboard.Keyboard_Key_SetOn(KEY_LEFT);
        }
        if(event->key() == Qt::Key_S)
        {
            keyboard.Keyboard_Key_SetOn(KEY_DOWN);
        }
        if(event->key() == Qt::Key_D)
        {
            keyboard.Keyboard_Key_SetOn(KEY_RIGHT);
        }
        if(event->key() == Qt::Key_W)
        {
            keyboard.Keyboard_Key_SetOn(KEY_UP);
        }
    }
}

void MainWindow::keyReleaseEvent(QKeyEvent *event)
{
    if(flag_modo == manual)
    {
        if(event->key() == Qt::Key_A)
        {
            keyboard.Keyboard_Key_SetOff(KEY_LEFT);
        }
        if(event->key() == Qt::Key_S)
        {
            keyboard.Keyboard_Key_SetOff(KEY_DOWN);
        }
        if(event->key() == Qt::Key_D)
        {
            keyboard.Keyboard_Key_SetOff(KEY_RIGHT);
        }
        if(event->key() == Qt::Key_W)
        {
            keyboard.Keyboard_Key_SetOff(KEY_UP);
        }
    }
}
