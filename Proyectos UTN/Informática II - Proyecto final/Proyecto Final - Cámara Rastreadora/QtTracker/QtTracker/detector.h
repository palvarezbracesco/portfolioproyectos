#ifndef DETECTOR_H
#define DETECTOR_H

#include <QObject>
#include <QImage>

#include "opencv2/opencv.hpp"

#include "trama.h"
#include "pid.h"

typedef int angle_t;

class Detector : public QObject
{
    Q_OBJECT
public:
    explicit Detector(QObject *parent = nullptr);
    void CascadeDetect(void);
    void SetDetectorState(bool);

private:
    cv::Mat frame;
    cv::Mat gray;
    cv::CascadeClassifier cascade;
    std::vector<cv::Rect> detections;
    cv::Rect past_detection;
    bool detector_state;
    bool flag_period;
    int Distance;
    angle_t pan;
    angle_t tilt;
    QTimer *timer;
    PID *pidx;
    PID *pidy;

signals:
    void DistanciaLista();
    void DetectionReady();
    void DetectedImage(QImage);
    void ImprimirDistancias(int, int);
    void MoverMotor(byte_t motor, uint8_t angle, byte_t direc);

public slots:
    void MoverCamara(void);
    void ProcesarImagen(QImage);
    void Draw();
};

#endif // DETECTOR_H
