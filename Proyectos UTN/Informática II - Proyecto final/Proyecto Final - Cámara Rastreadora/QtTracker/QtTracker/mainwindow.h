#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QTcpServer>
#include <QGraphicsPixmapItem>
#include <QMenu>
#include <QDir>
#include <QMessageBox>
#include <QCamera>
#include <QString>
#include <QCameraInfo>
#include <QAudioDeviceInfo>
#include <QKeyEvent>

#include "opencv2/opencv.hpp"

#include "configuracion.h"
#include "camara.h"
#include "detector.h"
#include "keyboard.h"
#include "trama.h"

#define PUERTO 9000

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int getOpcionDeteccion (void);

protected:

private slots:

    void on_pushButtonModo_pressed(void);
    void on_pushButtonCamara_pressed(void);
    void on_pushButtonMicrofono_pressed(void);
    void ImprimirImagen(QImage);
    void ImprimirConsola(int, int);
    void ImprimirConsola(QString string);
    void on_UpdateButton_pressed();
    void error_message(QString title, QString description);
    void on_pushButtonCargar_clicked(void);
    void newConnection(void);
    void disconnected(void);
    void readyRead(void);
    void EnviarAngulo(byte_t motor, uint8_t angle, byte_t direc);
    void EnviarKeyboard(keyboard_t);
    void on_pushButtonMoverIzq_pressed();
    void on_pushButtonMoverIzq_released();
    void on_pushButtonMoverArriba_pressed();
    void on_pushButtonMoverArriba_released();
    void on_pushButtonMoverDer_pressed();
    void on_pushButtonMoverDer_released();
    void on_pushButtonMoverAbajo_pressed();
    void on_pushButtonMoverAbajo_released();
    void keyPressEvent(QKeyEvent *);
    void keyReleaseEvent(QKeyEvent *);
    void CargarBarra(void);

signals:
    void CamaraNewFrame(void);

private:
    Ui::MainWindow *ui;
    QTcpServer * server;
    QTcpSocket * socket;
    QGraphicsPixmapItem pixmap;
    Configuracion configuracionActual;
    Camara camara;
    Detector detector;
    Keyboard keyboard;
    Trama trama;
};

#endif // MAINWINDOW_H
