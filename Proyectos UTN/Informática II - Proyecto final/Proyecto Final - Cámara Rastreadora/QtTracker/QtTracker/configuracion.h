#ifndef CONFIGURACION_H
#define CONFIGURACION_H

#include <QObject>
#define NOT_OK 0
#define OK 1
#define EMPTY -1
#define OVALADO 0
#define RECTANGULAR 1
#define PERSONAS 2

typedef int DetectType;

class Configuracion:public QObject
{
    Q_OBJECT
public:

    explicit Configuracion(QObject *parent = nullptr);
    void SetDetectType(int tipo);
    int GetDetectType(void)const;
    QString GetCameraName(void);
    void SetCameraName(QString);

private:
    QString CameraName;
    QString MicName;
    DetectType TipoDeDeteccion;
};

#endif // CONFIGURACION_H
