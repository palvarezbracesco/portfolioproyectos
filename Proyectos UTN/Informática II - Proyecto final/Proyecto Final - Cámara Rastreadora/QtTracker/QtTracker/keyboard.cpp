#include "keyboard.h"

Keyboard::Keyboard()
{
    keyboard = 0;
    internal_keyboard = 0;
    external_keyboard = 0;
}

void Keyboard::Keyboard_Key_SetOn(Key_t key)
{
    keyboard |= (1 << key);
    emit (Keyboard_Changed(keyboard));
}
void Keyboard::Keyboard_Key_SetOff(Key_t key)
{
    keyboard &= ~(1 << key);
    emit (Keyboard_Changed(keyboard));
}

void Keyboard::Keyboard_Mode_SetOff(void)
{
    Keyboard_Key_SetOff(KEY_MODE);
}


void Keyboard::NewExternalKeyboard(keyboard_t new_k)
{
    external_keyboard = new_k;
    return;
}
