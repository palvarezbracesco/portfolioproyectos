#ifndef TRAMA_H
#define TRAMA_H

#include <stdint.h>
#include <QObject>

typedef enum{
    INICIO,
    FUNCION,
    PARAMETRO,
    FIN,
    MAX_ELEMENTOS
}trama_t;

typedef enum{
    BYTE_INICIO = '#',
    BYTE_FIN = '$',
    PAN = 'P',
    TILT = 'T',
    KEYBOARD = 'K',
    IZQUIERDA = 'L',
    DERECHA = 'R',
    ARRIBA = 'U',
    ABAJO = 'D'
}byte_t;

class Trama: public QObject
{
    Q_OBJECT
public:
    Trama();
    void Analizar_Elemento(char);
    bool Procesar_Trama(void);
    bool Procesar_Trama(QString nuevo);

private:
    bool Trama_Vacia;
    char trama [MAX_ELEMENTOS];

signals:
    void Ready(void);
    void KeybardChange(char);
    void NuevaTrama(QString);
};

#endif // TRAMA_H
