/*
===============================================================================
 Name        : Wifi.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "Wifi.h"
#include "Uart.h"
#include "Trama.h"

/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS
 **********************************************************************************************************************************/

typedef enum
{
	START,            //!< START
	NOT_ECO,          //!< NOT_ECO
	CONFIG_CLIENT,    //!< CONFIG_CLIENT
	CONFIG_LENGTH,    //!< CONFIG_LENGTH
	CONNECTING_NET,   //!< CONNECTING_NET
	CONNECTING_SERVER,//!< CONNECTING_SERVER
	CONNECTED,        //!< CONNECTED
	DATA_LENGHT_SEND, //!< DATA_LENGHT_SEND
	DATA_SEND,        //!< DATA_SEND
	RESET,            //!< RESET
}wifi_state_t;

typedef struct
{
	char data[WIFI_BUFFER_MAX_LENGTH];
	size_t size;

}wifi_buffer_t;


/***********************************************************************************************************************************
 *** VARIABLES GLOBALES LOCALES AL MODULO
 **********************************************************************************************************************************/

static wifi_state_t state = START;

static bool ok_flag=false;
static bool error_flag=false;
static bool ready_to_send_data=false;
static bool ready_flag=false;
static bool fail_flag=false;
static wifi_buffer_t wifi_buffer_read;
static wifi_buffer_t wifi_buffer_write;
static char str_aux [300];

/***********************************************************************************************************************************
 *** PROTOTIPOS PRIVADOS
 **********************************************************************************************************************************/

bool Ok_Received(void);
bool Error_Received(void);
void Procesar_Wifi(void);
bool Ready_To_Send(void);
bool Ready_Received(void);
bool Fail_Received(void);

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/

/**
 * @brief 	Función para inicializar el módulo wifi.
 * 			Es parametrizable por medio de MACROS
 */
void Wifi_Init(void)
{
	ULCR_t ULCR_data;

	ULCR_data.wordLength = WIFI_UART_LENGTH; //Largo de los datos a comunicar
	ULCR_data.stopBit = WIFI_UART_STOP_BIT; //Bit de stop
	ULCR_data.parityEnable = WIFI_UART_PARITY_ENABLE; //Paridad (habilita/deshabilita)
	ULCR_data.paritySelect = WIFI_UART_PARITY_SELECTED; //Tipo de paridad
	ULCR_data.breakControl = WIFY_UART_BREAK_CONTROL; //Break control

	UART_Init(WIFI_UART_NUMBER,ULCR_data,WIFI_UART_CLK);
}

void MdE_Wifi(void)
{
	switch (state)
	{
	case START:
		//Comando para apagar el ECO (sino nos responde con el comando enviado)
		Uart_SendString(WIFI_UART_NUMBER,"ATE0\r\n");
		state = NOT_ECO;

		break;

	case NOT_ECO:
		//Comando para configurar el módulo como cliente
		if(Ok_Received())
		{
			Uart_SendString(WIFI_UART_NUMBER,"AT+CWMODE_DEF=1\r\n");

			state = CONFIG_CLIENT;
		}
		else if(Error_Received())
		{
			state =START;
		}
		break;

	case CONFIG_CLIENT:
		//Comando que configura si realizará una o multiples sesiones. Le asignamos el'0' ya que es para una única sesión
		if(Ok_Received())
		{
			sprintf(str_aux,"AT+CIPMUX=0\r\n");
			Uart_SendString(WIFI_UART_NUMBER,str_aux);

			state = CONFIG_LENGTH;
		}
		else if(Error_Received())
		{
			state = START;
		}
		break;

	case CONFIG_LENGTH:
		//Comando para configurar y conectarse a la red LAN (debe coincidir SSID de red a la cual la PC este conectada)
		if(Ok_Received())
		{
			sprintf(str_aux, "AT+CWJAP_DEF=\"%s\",\"%s\"\r\n",WIFI_SSID,WIFI_PASSWORD);
			Uart_SendString(WIFI_UART_NUMBER,str_aux);

			state = CONNECTING_NET;
		}
		else if(Error_Received())
		{
			state = START;
		}
		break;

	case CONNECTING_NET:
		//Comando para conectarse al servidor de Qt
		if(Ok_Received())
		{
			sprintf(str_aux,"AT+CIPSTART=\"TCP\",\"%s\",%s\r\n",SERVER_IP ,SERVER_PORT);
			Uart_SendString(WIFI_UART_NUMBER,str_aux);

			state = CONNECTING_SERVER;
		}
		if(Fail_Received())
		{	//Comando para resetear el módulo wifi
			Uart_SendString(WIFI_UART_NUMBER,"AT+RST\r\n");

			state = RESET;
		}
		else if(Error_Received())
		{
			state = START;
		}
		break;

	case CONNECTING_SERVER:

		if(Ok_Received())
		{
			state = CONNECTED;
		}
		else if(Error_Received())
		{
			state = START;
		}
		break;

	case CONNECTED:
		if(Ok_Received())
		{
			state = CONNECTED;
		}
		if(Error_Received())
		{
			state = START;
		}
		break;

	case DATA_LENGHT_SEND:

		if(Ok_Received())
		{
			Uart_SendString(WIFI_UART_NUMBER,wifi_buffer_write.data);

			state = DATA_SEND;
		}
		else if(Error_Received())
		{
			Uart_SendString(WIFI_UART_NUMBER,"AT+RST\r\n");
			state = RESET;
		}
		break;

	case DATA_SEND:

		if(Ok_Received())
		{
			state = CONNECTED;
		}
		else if(Error_Received())
		{
			Uart_SendString(WIFI_UART_NUMBER,"AT+RST\r\n");
			state = RESET;
		}
		break;

	case RESET:

		if(Ready_Received())
		{
			state = START;
		}

		if(Ok_Received())
		{
			state = CONNECTED;
		}

		break;

	}
}

bool Wifi_Send(char const * const str)
{
	if(state == CONNECTED)
	{

		for(size_t i =0; i < WIFI_BUFFER_MAX_LENGTH; i++)
		{
			wifi_buffer_write.size = i;

			wifi_buffer_write.data[i] = str[i];

			if(wifi_buffer_write.data[i] == '\0')
			{
				break;
			}

		}

		state = DATA_LENGHT_SEND;
		//Comando para enviar N bytes al ESP8266
		sprintf(str_aux,"AT+CIPSEND=%zu\r\n", wifi_buffer_write.size);
		Uart_SendString(WIFI_UART_NUMBER,str_aux);

		return true;
	}
	return false;
}

void Wifi_Update(void)
{
	if(Uart_IsByteAvailable(WIFI_UART_NUMBER))
	{
		wifi_buffer_read.data[wifi_buffer_read.size] = Uart_GetByte(WIFI_UART_NUMBER);

		wifi_buffer_read.size++;

		if(wifi_buffer_read.data[wifi_buffer_read.size-1] == WIFI_END_BYTE)
		{
			wifi_buffer_read.data[wifi_buffer_read.size] = '\0';
			Procesar_Wifi();
			wifi_buffer_read.size = 0;
		}
		else
		{

			if(wifi_buffer_read.size ==WIFI_BUFFER_MAX_LENGTH-1)
			{
				wifi_buffer_read.size = 0; // limpio buffer
			}
		}

	}
}

//Analiza la respuesta del ESP8266 a los comandos enviados,
//en base a la respuesta activa flags para avanzar con la inicializacion del modulo
void Procesar_Wifi(void)
{
	if(wifi_buffer_read.size != 2) //COMPRUEBO TRAMA VACIA
	{
		if(strstr(wifi_buffer_read.data,"OK"))
		{
			ok_flag = true;
		}

		else if(strstr(wifi_buffer_read.data,"READY"))
		{
			ready_flag = true;
		}
		else if(strstr(wifi_buffer_read.data,"ERROR"))
		{
			error_flag = true;
		}
		else if(strstr(wifi_buffer_read.data,"CLOSE"))
		{
			state = START;
		}
		else if(strstr(wifi_buffer_read.data,">"))
		{
			ready_to_send_data = true;
		}
		else if(strstr(wifi_buffer_read.data,"FAIL"))
		{
			fail_flag = true;
		}
		else if(strstr(wifi_buffer_read.data,"+IPD"))
		{
			for(size_t i = 0; i < wifi_buffer_read.size - 2 ; i++)
			{
				Analizar_Elemento(wifi_buffer_read.data[i]);
			}
		}
	}

	return;
}

//Interface para verificar la respuesta del ESP8266 a comandos
// que el modulo responde OK cuando lo procesa correctamente
bool Ok_Received(void)
{
	if(ok_flag==true)
	{
		ok_flag = false;
		return true;
	}

	return false;
}

//Interface para verificar si el modulo no pudo procesar la trama recibida
// devuelve ERROR
bool Error_Received(void)
{
	if(error_flag==true)
	{
		error_flag = false;
		return true;
	}

	return false;
}

bool Fail_Received(void)
{
	if(fail_flag==true)
	{
		fail_flag = false;
		return true;
	}

	return false;
}

bool Ready_Received(void)
{
	if(ready_flag==true)
	{
		ready_flag = false;
		return true;
	}

	return false;
}

bool Ready_To_Send(void)
{
	if(ready_to_send_data==true)
	{
		ready_to_send_data = false;
		return true;
	}

	return false;
}

void Wifi_Reset(void)
{
	Uart_SendString(WIFI_UART_NUMBER,"AT+RST\r\n");
	state = RESET;
}
