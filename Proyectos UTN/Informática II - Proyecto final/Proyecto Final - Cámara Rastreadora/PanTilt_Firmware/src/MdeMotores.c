/*
===============================================================================
 Name        : MdeMotores.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/


/***********************************************************************************************************************************
 *** MODULO MdeMotores
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include "MdeMotores.h"
#include "MdeModoFuncionamiento.h"
#include "Motor.h"


/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES LOCALES AL MODULO
 **********************************************************************************************************************************/

static estado_motores_t estado;

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/

void MdeMotores(void)
{
	switch(estado)
	{
	case MOVIENDO:
		//Si el buffer del motor esta vacío cambia de estado a REPOSO
		if(Motor_Buffer_Vacio())
		{
			estado = REPOSO;
		}
		break;

	case REPOSO:
		//Si el buffer no esta vacio cambia de estado a MOVIENDO
		if(!Motor_Buffer_Vacio())
		{
			estado = MOVIENDO;
		}
		break;

	default:
		estado = REPOSO;
		break;
	}
}

/***********************************************************************************************************************************
 *** FIN DEL MODULO MdeMotores
 **********************************************************************************************************************************/
