/*
===============================================================================
 Name        : main.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <stdio.h>
#include <stdint.h>

#include <cr_section_macros.h>
#include "system_LPC17xx.h"

#include "MdeMicrofono.h"
#include "MdeCamara.h"
#include "MdeMotores.h"
#include "MdeTeclado.h"
#include "MdeModoFuncionamiento.h"

#include "Motor.h"
#include <Teclado.h>
#include "Pin.h"
#include "Gpio.h"
#include "Systick.h"
#include "Wifi.h"

void Test_Motor(void);
void Test_Teclado(void);

int main(void) {

	SystemInit();		// Inicializa el sistema (clock, PLL), a partir de aqui el micro opera a 100 MHz.

	Inicializar_Systick(1); //Inicializa el SysTick en 1 miliseg

	Motor_Inicializar();

	Teclado_Init();

	Wifi_Init();

	while(1)
	{
		if(Tick(TICK_1))
		{
			Motor_Actualizar();
			Teclado_Update();
			Wifi_Update();

		}
		if(Tick(TICK_5))
		{
			MdE_Wifi();
			MdeModoFuncionamiento();
		}

		if(Tick(TICK_10))
		{
			//Test_Motor();

			MdeModoTeclado();
		}

		if(Tick(TICK_50))
		{
			MdeMotores();
			MdeModoTeclado();
		}
		if(Tick(TICK_100))
		{

			//Test_Teclado();
		}
		if(Tick(TICK_500))
		{

		}
	}

}

//--------------------- Funciones utilizadas para testear ---------------------

//Testea los motores moviendolos paso a paso hasta llegar a 180 grados de la posicion inicial y se reinicia

void Test_Motor(void)
{
	static uint8_t pan=0;
	static uint8_t tilt=0;

	motor_pos_s pos;

	pos.angulo_pan = pan;

	pos.angulo_tilt = tilt;

	Motor_Buffer_Push(pos);

	pan++;
	tilt++;

	if(pan == 180 || tilt == 180)
	{
		pan=0;
		tilt=0;
	}

	return;
}

// Testea el teclado
void Test_Teclado(void)
{
	static key_t tecla = NO_KEY;
	static keyboard_t teclado = 0;

	tecla = Teclado_GetKey();

	if(tecla != NO_KEY)
	{
		teclado = Teclado_Internal_Get();
		(void)teclado;
	}


	return;
}
