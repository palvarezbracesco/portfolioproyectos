/*
===============================================================================
 Name        : Systick.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/


/***********************************************************************************************************************************
 *** MODULO Systick
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include "Systick.h"
#include "LPC1769.h"
#include "system_LPC17xx.h"

/* SysTick Control / Status Register Definitions */
#define SysTick_CTRL_COUNTFLAG_Pos         16                                             /*!< SysTick CTRL: COUNTFLAG Position */
#define SysTick_CTRL_COUNTFLAG_Msk         (1UL << SysTick_CTRL_COUNTFLAG_Pos)            /*!< SysTick CTRL: COUNTFLAG Mask */

#define SysTick_CTRL_CLKSOURCE_Pos          2                                             /*!< SysTick CTRL: CLKSOURCE Position */
#define SysTick_CTRL_CLKSOURCE_Msk         (1UL << SysTick_CTRL_CLKSOURCE_Pos)            /*!< SysTick CTRL: CLKSOURCE Mask */

#define SysTick_CTRL_TICKINT_Pos            1                                             /*!< SysTick CTRL: TICKINT Position */
#define SysTick_CTRL_TICKINT_Msk           (1UL << SysTick_CTRL_TICKINT_Pos)              /*!< SysTick CTRL: TICKINT Mask */

#define SysTick_CTRL_ENABLE_Pos             0                                             /*!< SysTick CTRL: ENABLE Position */
#define SysTick_CTRL_ENABLE_Msk            (1UL << SysTick_CTRL_ENABLE_Pos)               /*!< SysTick CTRL: ENABLE Mask */

/* SysTick Reload Register Definitions */
#define SysTick_LOAD_RELOAD_Pos             0                                             /*!< SysTick LOAD: RELOAD Position */
#define SysTick_LOAD_RELOAD_Msk            (0xFFFFFFUL << SysTick_LOAD_RELOAD_Pos)        /*!< SysTick LOAD: RELOAD Mask */

/* SysTick Current Register Definitions */
#define SysTick_VAL_CURRENT_Pos             0                                             /*!< SysTick VAL: CURRENT Position */
#define SysTick_VAL_CURRENT_Msk            (0xFFFFFFUL << SysTick_VAL_CURRENT_Pos)        /*!< SysTick VAL: CURRENT Mask */

/* SysTick Calibration Register Definitions */
#define SysTick_CALIB_NOREF_Pos            31                                             /*!< SysTick CALIB: NOREF Position */
#define SysTick_CALIB_NOREF_Msk            (1UL << SysTick_CALIB_NOREF_Pos)               /*!< SysTick CALIB: NOREF Mask */

#define SysTick_CALIB_SKEW_Pos             30                                             /*!< SysTick CALIB: SKEW Position */
#define SysTick_CALIB_SKEW_Msk             (1UL << SysTick_CALIB_SKEW_Pos)                /*!< SysTick CALIB: SKEW Mask */

#define SysTick_CALIB_TENMS_Pos             0                                             /*!< SysTick CALIB: TENMS Position */
#define SysTick_CALIB_TENMS_Msk            (0xFFFFFFUL << SysTick_VAL_CURRENT_Pos)        /*!< SysTick CALIB: TENMS Mask */

/**
 * VECTOR DE LOS CONTADORES
 */
size_t ticks [MAX_TICK];

/**
 * ESTRUCTURA PARA EL CONTROL DEL SYSTICK
 */
typedef struct
{
  __IO uint32_t CTRL;                    /*!< Offset: 0x000 (R/W)  SysTick Control and Status Register */
  __IO uint32_t LOAD;                    /*!< Offset: 0x004 (R/W)  SysTick Reload Value Register       */
  __IO uint32_t VAL;                     /*!< Offset: 0x008 (R/W)  SysTick Current Value Register      */
  __I  uint32_t CALIB;                   /*!< Offset: 0x00C (R/ )  SysTick Calibration Register        */
} SysTick_Type;

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/

/**
 * @brief Función para inicializar el modulo de systick
 * @param time_ms
 */
void Inicializar_Systick(uint32_t time_ms)
{
	SysTick->LOAD = (SystemCoreClock / 1000) * time_ms - 1;	// Carga la cuenta que define el tiempo entre interrupciones
	SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;			// Selecciona la fuente de clock
	SysTick->VAL = 0;										// Resetea la cuenta actual (Pone a cero el contador)
	SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;				// Habilita la interrupción
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;				// Habilita el inicio de conteo

	for(int i =0 ; i <MAX_TICK ; i++)
	{
		Tick_Reload(i);
	}
}

/**
 * @brief Se utiliza un contador descendente
 */
void SysTick_Handler(void)
{
	for(int i =0 ; i <MAX_TICK ; i++)
	{
		if(ticks[i])
			ticks[i]--;
	}
}

/**
 * @brief Consulta si el contador terminó y si es así lo recarga
 * @param tick_id Id del tick a consultar
 * @return true si el contador terminó, false si aún no terminó
 */
bool Tick(tick_t tick_id)
{
	if(!ticks[tick_id])
	{
		Tick_Reload(tick_id);
		return true;
	}

	return false;
}

/**
 * @brief Esta función recarga los tick para reiniciar los contadores
 * @param id : Id del tick a reiniciar
 */
void Tick_Reload(tick_t id)
{
	switch (id)
	{
	case TICK_1:
		ticks[TICK_1] = 1;
		break;

	case TICK_5:
		ticks[TICK_5] = 5;
		break;


	case TICK_10:
		ticks[TICK_10] = 10;
		break;

	case TICK_50:
		ticks[TICK_50] = 50;
		break;

	case TICK_100:
		ticks[TICK_100] = 100;
		break;

	case TICK_500:
		ticks[TICK_500] = 500;
		break;

	default:
		break;
	}

}
/***********************************************************************************************************************************
 *** FIN DEL MODULO Systick
 **********************************************************************************************************************************/
