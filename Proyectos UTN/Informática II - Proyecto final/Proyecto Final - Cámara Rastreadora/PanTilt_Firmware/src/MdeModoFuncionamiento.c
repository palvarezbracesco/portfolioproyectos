/*
===============================================================================
 Name        : MdeModoFuncionamiento.c
 Author      : Grupo 1 (ALVAREZ-COSTA-PEZZELATTO)
===============================================================================
*/


/***********************************************************************************************************************************
 *** MODULO Mde_Modo_Movimiento
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include <Teclado.h>
#include "Motor.h"
#include "MdeModoFuncionamiento.h"

/***********************************************************************************************************************************
 *** PROTOTIPOS
 **********************************************************************************************************************************/

void CargarBufferDesdeTeclado(void);


/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

static modo_funcionamiento_t modo = MODO_MANUAL;
static modo_funcionamiento_t nuevo_modo = MODO_MANUAL;

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/
static uint8_t angulo_giro = 1;

/**
 * @brief Maquina de estados de el modo de funcionamiento de los motores
 */
void MdeModoFuncionamiento()
{
	switch (modo)
	{
	case MODO_MANUAL:

		CargarBufferDesdeTeclado();

		if( nuevo_modo != MODO_MANUAL)
		{
			modo = nuevo_modo;
		}

		break;

	case MODO_AUTOMATICO:

		if( nuevo_modo != MODO_AUTOMATICO)
		{
			modo = nuevo_modo;
		}

		break;

	default:
		modo = nuevo_modo;

		break;

	}
}


void CargarBufferDesdeTeclado(void)
{
	motor_pos_s pos;

	pos = Motor_Posicion();

	if(Teclado_GetKeyState(KEY_UP))
	{
		pos.angulo_tilt = (uint8_t)(pos.angulo_tilt - angulo_giro);
	}

	if(Teclado_GetKeyState(KEY_DOWN))
	{
		pos.angulo_tilt = (uint8_t)(pos.angulo_tilt + angulo_giro);
	}

	if(Teclado_GetKeyState(KEY_LEFT))
	{
		pos.angulo_pan = (uint8_t)(pos.angulo_pan + angulo_giro);
	}
	if(Teclado_GetKeyState(KEY_RIGHT))
	{
		pos.angulo_pan = (uint8_t)(pos.angulo_pan - angulo_giro);
	}

	Motor_Buffer_Push(pos);

	return;
}

/**
 * @brief 	Esta funcion brinda información sobre el modo de funcionamiento
 * @return	Puede devolve MODO_MANUAL en caso de que los motores se controlen
 * 			por el teclado o MODO_UART si se controlan mediante información
 * 			de la uart
 */
modo_funcionamiento_t GetModoFuncionamiento(void)
{
	return modo;
}

/**
 * @brief Configura el modo de funcionamiento del control de los motores.
 * @param nuevo_modo modo a configurar el dispositivo, este puede ser:
 * 				MODO_MANUAL: Modo en el cual se controla el dispositivo con el teclado
 * 				MODO_UART: Modo en el cual se controla el dispositivo por
 * 								detección de objetos(con datos ingresados mediante la uart)
 */
void SetModoFuncionamiento(modo_funcionamiento_t modo_funcionamiento)
{
	nuevo_modo = modo_funcionamiento;
}

void TabModoFuncionamiento(void)
{

	nuevo_modo++;

	nuevo_modo %= MAX_MODOS;

	return;
}

/***********************************************************************************************************************************
 *** FIN DEL MODULO Mde_Modo_Movimiento
 **********************************************************************************************************************************/
