/*
===============================================================================
 @file       : Wifi.c
 @author     : Juan Costa Suárez
 @date       : 2 dic. 2019
 @brief      :
===============================================================================
*/

#ifndef WIFI_H_
#define WIFI_H_

#include "LPC1769.h"
#include "Uart.h"
#include <stdbool.h>
#include <stdint.h>

/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

#define WIFI_END_BYTE	'\n'

#define WIFI_SSID 		"Costa2.4"//"afOf88"
#define WIFI_PASSWORD	"costasuarez"//"235056651"
#define SERVER_PORT		"9000"
#define SERVER_IP		"192.168.1.18"

/**
 * MACROS DE PARAMETRIZACION DE LA INICIALIZACION DEL WIFI
 */
#define WIFI_UART_NUMBER UART_3
#define	WIFI_UART_LENGTH Length_8Bit
#define	WIFI_UART_STOP_BIT OFF
#define	WIFI_UART_PARITY_ENABLE OFF
#define	WIFI_UART_PARITY_SELECTED OFF_paritySelect
#define WIFY_UART_BREAK_CONTROL OFF
#define WIFI_UART_CLK CCLK_break_special

/**
 * MÁXIMO LARGO DEL BUFFER WIFI
 */
#define WIFI_BUFFER_MAX_LENGTH 256

/***********************************************************************************************************************************
 *** IMPLEMENTACIONES
 **********************************************************************************************************************************/

void Wifi_Init(void);

void MdE_Wifi(void);

void Wifi_Update(void);

bool Wifi_Send(char const * const str);

void Wifi_Reset(void);

#endif /* WIFI_H_ */
