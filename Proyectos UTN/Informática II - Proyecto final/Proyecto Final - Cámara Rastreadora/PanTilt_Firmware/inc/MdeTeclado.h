/*
===============================================================================
 @file       : MdeTeclado.h
 @author     : Juan Costa Suárez
 @date       : 19 nov. 2019
 @brief      : 
===============================================================================
*/


/***********************************************************************************************************************************
 *** MODULO MdeTeclado
 **********************************************************************************************************************************/

#ifndef MDETECLADO_H_
#define MDETECLADO_H_

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/


/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPOS
 **********************************************************************************************************************************/

void MdeModoTeclado(void);

#endif /* MDETECLADO_H_ */

/***********************************************************************************************************************************
 *** FIN DEL MODULO
 **********************************************************************************************************************************/
