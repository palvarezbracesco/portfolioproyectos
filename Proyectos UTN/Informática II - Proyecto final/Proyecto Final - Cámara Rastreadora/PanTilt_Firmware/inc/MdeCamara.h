/*
===============================================================================
 @file       : MdeCamara.h
 @author     : Juan Costa Suárez
 @date       : 28 oct. 2019
 @brief      : 
===============================================================================
*/


/***********************************************************************************************************************************
 *** MODULO MdeCámara
 **********************************************************************************************************************************/

#ifndef MDECAMARA_H_
#define MDECAMARA_H_

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/

#include <stdbool.h>


/***********************************************************************************************************************************
 *** DEFINES
 **********************************************************************************************************************************/

void MdeCamara(void);

bool IsCamaraOn(void);

void CamaraTurnOn(void);

void CamaraTurnOff(void);

void CamaraTab(void);

#endif /* MDECAMARA_H_ */

/***********************************************************************************************************************************
 *** FIN DEL MODULO
 **********************************************************************************************************************************/
